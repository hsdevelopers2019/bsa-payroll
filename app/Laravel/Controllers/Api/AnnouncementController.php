<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader,Carbon;
use App\Laravel\Models\Announcement;
use Illuminate\Http\Request;
use App\Laravel\Requests\Api\ArticleRequest;
use App\Laravel\Transformers\AnnouncementTransformer;
use App\Laravel\Transformers\TransformerManager;

use App\Laravel\Jobs\Article\NotifyUsers;
use App\Laravel\Notifications\Self\Article\NewArticleNotification as SelfNewArticleNotification;


class AnnouncementController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function index(Request $request, $format = '') {

		$per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        $user = $request->user();
        $keyword = $request->get('keyword');

        $sort_by = Str::lower($request->get('sort_by','created_at'));
        $sort_order = Str::lower($request->get('sort_order','desc'));
    
        $this->response['msg'] = "List of Announcements";
        $this->response['status_code'] = "ANNOUNCEMENT_LIST";

        switch($sort_order){ 
            case 'desc'  : $sort_order = 'desc'; break;
            default: $sort_order = 'asc';
        }

        switch($sort_by){ 
            case 'category'  : $sort_by = 'category_id'; break;
            default: $sort_by = 'created_at';
        }

        $date_today = Carbon::now()->format("Y-m-d");
        $announcements = Announcement::keyword($keyword)
                        ->whereRaw("DATE(created_at) = '{$date_today}'")
                        ->orderBy($sort_by,$sort_order)->paginate($per_page);

        if($request->has('keyword')){
            $this->response['msg'] = "Search result: '{$keyword}'";
            $this->response['status_code'] = "SEARCH_RESULT";
        }

        $this->response['status'] = TRUE;
        $this->response['has_morepages'] = $announcements->hasMorePages();
        $this->response['data'] = $this->transformer->transform($announcements, new AnnouncementTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}