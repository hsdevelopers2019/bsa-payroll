<?php

namespace App\Laravel\Controllers\Api\Auth;

use App\Laravel\Controllers\Controller;
use App\Laravel\Events\UserAction;
use App\Laravel\Models\User;
use App\Laravel\Models\UserSocial;
use App\Laravel\Models\EmailVerification;
use App\Laravel\Requests\Api\FacebookLoginRequest;
use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\UserTransformer;
use App\Laravel\Notifications\SendEmailVerificationToken;

use App\Laravel\Notifications\Self\Greetings\WelcomeNotification;

use Auth, JWTAuth, Helper, Str, Socialite,Carbon;
use GuzzleHttp\Exception\RequestException;

class FacebookLoginController extends Controller {

	protected $response = array();

	public function __construct() {
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function authenticate(FacebookLoginRequest $request, $format = '') {

		try {
			$facebook_account = Socialite::driver('facebook')->userFromToken($request->get('access_token'));
		} catch (RequestException $e) {
			$facebook_account = NULL;
		}


		if(!$facebook_account) {
			$this->response['msg'] = "Login failed";
			$this->response['status_code'] = "FACEBOOK_LOGIN_FAILED";
			$this->response_code = 409;
			goto callback;
		}

		$user_social = UserSocial::where('provider', "facebook")
						->where('provider_user_id', $facebook_account->getId())
						->first();

		if($user_social) {

			$user = $user_social->user;

			$this->response['msg'] = "Hello {$user->name}";
			$this->response['status'] = TRUE;
			$this->response['status_code'] = "FACEBOOK_LOGIN_SUCCESS";
			$this->response['token'] = JWTAuth::fromUser($user, ['did' => $request->get('device_id')]);
			$this->response['first_login'] = FALSE;
			$this->response['data'] = $this->transformer->transform($user, new UserTransformer,'item');
			$this->response_code = 200;

			event( new UserAction($user, ['login']) );
			goto callback;
		}

		$email = $facebook_account->getEmail() ? : ( $facebook_account->getId() . "@facebook.com" );
		$existing_email = User::where('email', $email)->where('type', 'user')->first();

		if($existing_email) {
			
			$existing_email->fb_id = $request->get('fb_id');
			$existing_email->save();

			UserSocial::firstOrCreate([
				'provider' => "facebook", 
				'provider_user_id' => $facebook_account->getId(), 
				'user_id' => $existing_email->id,
			]);

			$this->response['msg'] = "Login successfully.";
			$this->response['status'] = TRUE;
			$this->response['status_code'] = "FACEBOOK_CONNECT_SUCCESS";
			$this->response['token'] = JWTAuth::fromUser($existing_email, ['did' => $request->get('device_id')]);
			$this->response['first_login'] = FALSE;
			$this->response['data'] = $this->transformer->transform($existing_email, new UserTransformer,'item');
			$this->response_code = 200;

			event( new UserAction($existing_email, ['login']) );

		} else {

			$facebook_autofill = [
				'email' => $email,
				'name' => $facebook_account->getName(),
			];

			$user = new User;
			$user->fill($facebook_autofill);
			// $user->email = $request->get('email') ?: $request->get('fb_id')."@facebook.com";

			$username = substr(Str::slug($user->name, ""), 0, 20);
			$user->fb_id = $facebook_account->getId();
			$user->username = $user->fb_id;
			$user->password = NULL;
			// $user->my_privacy = "month_day";
			$user->save();
			$token = Str::upper($this->_generateEmailVerificationToken());

			$user->notify(new SendEmailVerificationToken($token, ['source' => "api", 'name' => $user->name, 'email' => $user->email]) );
			$user->notify(new WelcomeNotification());


			UserSocial::firstOrCreate([
				'provider' => "facebook", 
				'provider_user_id' => $facebook_account->getId(), 
				'user_id' => $user->id,
			]);

			// $user->notify(new WelcomeNotification());


			$this->response['msg'] = "Login successfully.";
			$this->response['status'] = TRUE;
			$this->response['status_code'] = "FACEBOOK_REGISTER_SUCCESS";
			$this->response['token'] = JWTAuth::fromUser($user, ['did' => $request->get('device_id')]);
			$this->response['first_login'] = TRUE;
			$this->response['data'] = $this->transformer->transform($user, new UserTransformer,'item');
			$this->response_code = 200;

			event( new UserAction($user, ['register']) );
		}

		callback:
		switch(Str::lower($format)){
			case 'json' :
				return response()->json($this->response, $this->response_code);
			break;
			case 'xml' :
				return response()->xml($this->response, $this->response_code);
			break;
		}
	}

	private function _generateEmailVerificationToken() {
		$key = config('app.key');
        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }
        // return hash_hmac('sha256', Str::random(40), $key);
		return Str::random(6);
	}

	private function _saveEmailVerificationToken(User $user, $token) {
		EmailVerification::where('email', $user->email)->delete();

		$email_verification = new EmailVerification;
		$email_verification->email = $user->email;
		$email_verification->token = $token;
		$email_verification->created_at = Carbon::now();
		$email_verification->save();
	}


}