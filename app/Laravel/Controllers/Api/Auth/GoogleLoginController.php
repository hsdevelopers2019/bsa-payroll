<?php

namespace App\Laravel\Controllers\Api\Auth;

use App\Laravel\Controllers\Controller;
use App\Laravel\Events\UserAction;
use App\Laravel\Models\User;
use App\Laravel\Models\UserSocial;
use App\Laravel\Models\EmailVerification;

use App\Laravel\Requests\Api\GoogleLoginRequest;
use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\UserTransformer;
use App\Laravel\Notifications\Self\Greetings\WelcomeNotification;
use App\Laravel\Notifications\SendEmailVerificationToken;


use Auth, JWTAuth, Helper, Str, Socialite,Carbon;
use GuzzleHttp\Exception\RequestException;
use Ixudra\Curl\Facades\Curl;


class GoogleLoginController extends Controller {

	protected $response = array();

	public function __construct() {
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function authenticate(GoogleLoginRequest $request, $format = '') {

		// try {
		// 	$google_account = Socialite::driver('google')->userFromToken($request->get('access_token'));
		// } catch (RequestException $e) {
		// 	$google_account = NULL;
		// }

		// Initialize cURL
		$ch = curl_init();   
		// The URL to load 
		curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='.$request->get('access_token')); 
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); 
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array( "Authorization: Bearer ".$request->get('access_token'), "Accept: application/json" ));
		curl_setopt($ch, CURLOPT_BUFFERSIZE, 64000);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
			
		$google_account = @curl_exec($ch); 
		$http_status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
		@curl_close($ch); 

		if($http_status_code != "200") {
			$this->response['msg'] = "Login failed";
			$this->response['status_code'] = "GOOGLE_LOGIN_FAILED";
			$this->response_code = 409;
			goto callback;
		}

		$google_account  = json_decode($google_account);

		$user_social = UserSocial::where('provider', "google")
						->where('provider_user_id', $google_account->sub)
						->first();

		if($user_social) {

			$user = $user_social->user;

			$this->response['msg'] = "Hello {$user->name}";
			$this->response['status'] = TRUE;
			$this->response['status_code'] = "GOOGLE_LOGIN_SUCCESS";
			$this->response['token'] = JWTAuth::fromUser($user, ['did' => $request->get('device_id')]);
			$this->response['first_login'] = FALSE;
			$this->response['data'] = $this->transformer->transform($user, new UserTransformer,'item');
			$this->response_code = 200;

			event( new UserAction($user, ['login']) );
			goto callback;
		}

		$email = $google_account->email ? : ( $google_account->sub . "@gmail.com" );
		$existing_email = User::where('email', $email)->where('type', 'user')->first();
		if($existing_email) {
			
			$existing_email->google_id = $request->get('google_id');
			$existing_email->save();

			UserSocial::firstOrCreate([
				'provider' => "google", 
				'provider_user_id' => $google_account->sub, 
				'user_id' => $existing_email->id,
			]);

			$this->response['msg'] = "Login successfully.";
			$this->response['status'] = TRUE;
			$this->response['status_code'] = "GOOGLE_CONNECT_SUCCESS";
			$this->response['token'] = JWTAuth::fromUser($existing_email, ['did' => $request->get('device_id')]);
			$this->response['first_login'] = FALSE;
			$this->response['data'] = $this->transformer->transform($existing_email, new UserTransformer,'item');
			$this->response_code = 200;

			event( new UserAction($existing_email, ['login']) );

		} else {

			$google_autofill = [
				'email' => $email,
				'name' => $google_account->name,
			];

			$user = new User;
			$user->fill($google_autofill);
			// $user->email = $request->get('email') ?: $request->get('google_id')."@gmail.com";

			$username = substr(Str::slug($user->name, ""), 0, 20);
			$user->password = NULL;
			$user->google_id = $google_account->sub;
			$user->username = Carbon::now()->format("Ymdhis");
			
			// $user->my_privacy = "month_day";
			$user->save();

			$token = Str::upper($this->_generateEmailVerificationToken());


			$user->notify(new SendEmailVerificationToken($token, ['source' => "api", 'name' => $user->name, 'email' => $user->email]) );
			$user->notify(new WelcomeNotification());


			UserSocial::firstOrCreate([
				'provider' => "google", 
				'provider_user_id' => $google_account->sub, 
				'user_id' => $user->id,
			]);

			$this->response['msg'] = "Login successfully.";
			$this->response['status'] = TRUE;
			$this->response['status_code'] = "GOOGLE_REGISTER_SUCCESS";
			$this->response['token'] = JWTAuth::fromUser($user, ['did' => $request->get('device_id')]);
			$this->response['first_login'] = TRUE;
			$this->response['data'] = $this->transformer->transform($user, new UserTransformer,'item');
			$this->response_code = 200;

			event( new UserAction($user, ['register']) );
		}

		callback:
		switch(Str::lower($format)){
			case 'json' :
				return response()->json($this->response, $this->response_code);
			break;
			case 'xml' :
				return response()->xml($this->response, $this->response_code);
			break;
		}
	}

	private function _generateEmailVerificationToken() {
		$key = config('app.key');
        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }
        // return hash_hmac('sha256', Str::random(40), $key);
		return Str::random(6);
	}

	private function _saveEmailVerificationToken(User $user, $token) {
		EmailVerification::where('email', $user->email)->delete();

		$email_verification = new EmailVerification;
		$email_verification->email = $user->email;
		$email_verification->token = $token;
		$email_verification->created_at = Carbon::now();
		$email_verification->save();
	}


}