<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader,Carbon;
use App\Laravel\Models\User;
use App\Laravel\Models\Chat;
use App\Laravel\Models\Views\Chat as ChatView;
use App\Laravel\Models\ChatParticipant;
use App\Laravel\Models\ChatConversation;
use Illuminate\Http\Request;
use App\Laravel\Requests\Api\ChatRequest;
use App\Laravel\Requests\Api\ChatNameRequest;
use App\Laravel\Requests\Api\ChatIconRequest;

use App\Laravel\Transformers\ChatTransformer;
use App\Laravel\Transformers\Views\ChatTransformer as ChatViewTransformer;
use App\Laravel\Transformers\ArticleTransformer;
use App\Laravel\Transformers\TransformerManager;


use App\Laravel\Notifications\Self\Chat\NewChatNotification as SelfNewChatNotification;
use App\Laravel\Notifications\Chat\NewChatNotification;
use App\Laravel\Notifications\Self\Chat\PublicJoinChatNotification;

use App\Laravel\Jobs\Chat\NotifyParticipants;
use App\Laravel\Jobs\Chat\NotifyUsers;

class ChatController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function index(Request $request, $format = '') {

		$per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        $user = $request->user();
        $keyword = $request->get('keyword');

        $sort_by = Str::lower($request->get('sort_by','date_posted'));
        $sort_order = Str::lower($request->get('sort_order','desc'));
    
        $this->response['msg'] = "List of Group Chat";
        $this->response['status_code'] = "GROUPCHAT_LIST";

        switch($sort_order){ 
            case 'desc'  : $sort_order = 'desc'; break;
            default: $sort_order = 'asc';
        }

        switch($sort_by){ 
            case 'category'  : $sort_by = 'category_id'; break;
            default: $sort_by = 'latest_message_created_at';
        }
        $chat_ids = ChatParticipant::where('user_id',$user->id)->pluck('chat_id')->toArray();
        if(!$chat_ids){ $chat_ids = "0";}
        else{$chat_ids = implode(",", $chat_ids);}

        $chats = ChatView::keyword($keyword)
                ->where(function($query) use($chat_ids){
                    $date_today = Carbon::now()->format("Y-m-d");
                    return $query->whereRaw("id IN ($chat_ids)")
                                ->orWhereRaw("DATE(created_at) = '{$date_today}'");  
                })
                ->orderBy($sort_by,$sort_order)->paginate($per_page);

        if($request->has('keyword')){
            $this->response['msg'] = "Search result: '{$keyword}'";
            $this->response['status_code'] = "SEARCH_RESULT";
        }

        $this->response['status'] = TRUE;
        $this->response['has_morepages'] = $chats->hasMorePages();
        $this->response['data'] = $this->transformer->transform($chats, new ChatViewTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function owned(Request $request, $format = '') {

        $per_page = $request->get('per_page', 5);
        $page = $request->get('page', 1);
        $user = $request->user();
        $owner = $request->get('user_data');

        $keyword = $request->get('keyword');

        $sort_by = Str::lower($request->get('sort_by','date_posted'));
        $sort_order = Str::lower($request->get('sort_order','desc'));
    
        $this->response['msg'] = "List of Group Chat";
        $this->response['status_code'] = "GROUPCHAT_LIST";

        switch($sort_order){ 
            case 'desc'  : $sort_order = 'desc'; break;
            default: $sort_order = 'asc';
        }

        switch($sort_by){ 
            case 'category'  : $sort_by = 'category_id'; break;
            default: $sort_by = 'latest_message_created_at';
        }
        $chat_ids = ChatParticipant::where('user_id',$owner->id)->pluck('chat_id')->toArray();
        if(!$chat_ids){ $chat_ids = "0";}
        else{$chat_ids = implode(",", $chat_ids);}

        $chats = ChatView::keyword($keyword)
                ->where(function($query) use($chat_ids){
                    $date_today = Carbon::now()->format("Y-m-d");
                    return $query->whereRaw("id IN ($chat_ids)")
                                ->orWhereRaw("DATE(created_at) = '{$date_today}'");  
                })
                ->orderBy($sort_by,$sort_order)->paginate($per_page);

        if($request->has('keyword')){
            $this->response['msg'] = "Search result: '{$keyword}'";
            $this->response['status_code'] = "SEARCH_RESULT";
        }

        $this->response['status'] = TRUE;
        $this->response['has_morepages'] = $chats->hasMorePages();
        $this->response['data'] = $this->transformer->transform($chats, new ChatViewTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function store(ChatRequest $request, $format = '') {

        $user = $request->user();

        $chat = new Chat;
        $chat->owner_user_id = $user->id;
        $chat->title = $request->get('title');

        // $article->fill($request->only('title','video_url','content','category_id'));

        // if($request->hasFile('file')) {
        //     $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/articles");
        //     $article->path = $image['path'];
        //     $article->directory = $image['directory'];
        //     $article->filename = $image['filename'];
        // }

        $chat->save();

        $user->notify(new SelfNewChatNotification($chat));

        $particpant = new ChatParticipant;
        $particpant->user_id = 1;
        $particpant->chat_id = $chat->id;
        $particpant->role = "moderator";
        // $particpant->nickname = "Administrator";
        $particpant->save();

        $particpant = new ChatParticipant;
        $particpant->user_id = $user->id;
        $particpant->chat_id = $chat->id;
        $particpant->role = "moderator";
        $particpant->save();

        $participants = explode(",", $request->get('participants'));

        if(count($participants) > 0 ){
            $users = User::types(['mentee','mentor'])->whereIn('id',$participants)->get();

            foreach($users as $index => $member){
                if(!in_array($member->id, ['1',$user->id])){
                    $particpant = new ChatParticipant;
                    $particpant->user_id = $member->id;
                    $particpant->chat_id = $chat->id;
                    $particpant->role = "member";
                    $particpant->save();
                }
            }
        }

        dispatch(new NotifyParticipants($user, $chat,"NEW_CHAT",[$user->id]));

        $exclude_users = [$user->id];
        if(count($participants )> 0){$exclude_users += $participants;}
        dispatch(new NotifyUsers($user, $chat,"NEW_CHAT",$exclude_users));

        $message = new ChatConversation;
        $message->chat_id = $chat->id;
        $message->type = "announcement";
        $message->sender_user_id = 1;
        $message->content = "Group chat created.";
        $message->save();

        $this->response['msg'] = "New group has been created.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "GROUPCHAT_CREATED";
        $this->response['data'] = $this->transformer->transform($chat, new ChatTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function show(Request $request, $format = '') {

        $chat = $request->get('chat_data');
        $user = $request->user();

        $participant = ChatParticipant::where('user_id',$user->id)->where('chat_id',$chat->id)->first();

        $this->response['msg'] = "Group chat detail.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CHAT_DETAIL";
        $this->response['role'] = $participant?$participant->role:"n/a";
        $this->response['data'] = $this->transformer->transform($chat, new ChatTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function join(Request $request,$format = ''){
        $chat = $request->get('chat_data');
        $auth = $request->user();
        $user = User::find($chat->owner_user_id);

        $already_participant = ChatParticipant::where('chat_id',$chat->id)
                                    ->where('user_id',$auth->id)
                                    ->first();
        if($already_participant){
            $this->response['msg'] = "You are already a participant";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "UNACCEPTABLE";
            $this->response_code = 406;
            goto callback;
        }

        dispatch(new NotifyParticipants($user, $chat,"NEW_JOIN",[$user->id,$auth->id]));
        $auth->notify(new PublicJoinChatNotification($chat));

        $member = new ChatParticipant;
        $member->user_id = $auth->id;
        $member->chat_id = $chat->id;
        $member->role = "member";
        $member->save();

        $message = new ChatConversation;
        $message->chat_id = $chat->id;
        $message->type = "announcement";
        $message->sender_user_id = 1;
        $message->content = "{$auth->name} join the group.";
        $message->save();

        $this->response['msg'] = "Successfully joined to the group.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "JOINED_GROUP";
        // $this->response['data'] = $this->transformer->transform($chat, new ChatTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update_name(ChatNameRequest $request, $format = '' ){
        $chat = $request->get('chat_data');

        $user = $request->user();

        $chat->title = $request->get('title');
        $chat->save();

        dispatch(new NotifyParticipants($user, $chat,"UPDATE_NAME",[$user->id]));


        $this->response['msg'] = "Group name has been modified.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "GROUPNAME_MODIFIED";
        $this->response['data'] = $this->transformer->transform($chat, new ChatTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update_icon(ChatIconRequest $request, $format = '' ){
        $chat = $request->get('chat_data');

        if($request->hasFile('file')) {
            $image = ImageUploader::upload($request->file('file'), "uploads/images/chat/{$chat->id}");
            $chat->path = $image['path'];
            $chat->directory = $image['directory'];
            $chat->filename = $image['filename'];
            $chat->source = $image['source'];
        }

        $chat->save();


        $this->response['msg'] = "Group name has been modified.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "GROUPNAME_MODIFIED";
        $this->response['data'] = $this->transformer->transform($chat, new ChatTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update(ChatRequest $request, $format = '') {

        $article = $request->get('article_data');
        $user = $request->user();

        $article->fill($request->only('title','video_url','content','category_id'));

        if($request->hasFile('file')) {
            $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/articles");
            $article->path = $image['path'];
            $article->directory = $image['directory'];
            $article->filename = $image['filename'];
        }

        $article->save();

        $this->response['msg'] = "Article has been updated.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_MODIFIED";
        $this->response['data'] = $this->transformer->transform($article, new ArticleTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function destroy(Request $request, $format = '') {

        $article = $request->get('article_data');

        $article->delete();

        $this->response['msg'] = "Article has been removed.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_DELETED";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}