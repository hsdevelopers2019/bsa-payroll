<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader,Carbon, PDF, QrCode, Auth, stdClass;

use App\Laravel\Models\User;
use App\Laravel\Models\Page;
use App\Laravel\Models\Rating;
use Illuminate\Support\Collection;

use App\Laravel\Requests\Api\RatingRequest;

use Illuminate\Http\Request;

class EventController extends Controller{

	protected $response = array();
    protected $programs = '';
    protected $panelists = '';
    protected $partners = '';
    protected $exhibitors = '';

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;

        
        $this->programs = new Collection([
            ['id' => 1, 
                'time' => '07:00 - 09:00 AM', 
                'segment' => 'Registration<br/>Operation Salvo', 
                'is_rate' => false,
                'resource' => new Collection([
                    ['name' => 'Urban Crew', 'position' => '', 'is_rate' => false]
                ]),
                'moderators' => new Collection([])
            ],
            ['id' => 2, 
                'time' => '09:00 - 09:15 AM', 
                'segment' => 'Welcome Message', 
                'is_rate' => false,
                'resource' => new Collection([
                    ['name' => 'Mr. Jose Ma. Concepcion III', 'position' => 'Presidential Adviser for Entrepreneurship  Chair, ASEAN Business Advisory Council Philippines Founder, Go Negosyo', 'is_rate' => false]
                ]),
                'moderators' => new Collection([])
            ],
            ['id' => 3, 
                'time' => '09:10 - 09:30 AM', 
                'segment' => 'Message to the Youth and Small Entrepreneurs', 
                'is_rate' => false,
                'resource' => new Collection([
                    ['name' => 'Sec. Ramon Lopez', 'position' => 'Secretary, Department of Trade and Industry, Philippines' , 'is_rate' => false]
                ]),
                'moderators' => new Collection([])
            ],
            ['id' => 4, 
                'time' => '09:30 - 09:45 AM', 
                'segment' => 'Special Message', 
                'is_rate' => false,
                'resource' => new Collection([
                    ['name' => 'H.E. Ambassador Sung Kim', 'position' => 'Ambassador in the Philippines, Embassy of the United States of  America', 'is_rate' => false]
                ]),
                'moderators' => new Collection([])
            ],
            ['id' => 5, 
                'time' => '09:45 - 10:15 AM', 
                'segment' => 'Plenary Talk', 
                'is_rate' => false,
                'resource' => new Collection([
                    ['name' => 'Mr. Anthony Tan', 'position' => 'CEO and Co-founder of Grab', 'is_rate' => true]
                ]),
                'moderators' => new Collection([])
            ],
            ['id' => 6, 
                'time' => '10:15 - 11:00 AM', 
                'segment' => 'Forum 1: <b>Innovation through Collaboration</b>', 
                'is_rate' => true,
                'resource' => new Collection([
                    ['name' => 'Mr. Robbie Antonio', 'position' => 'CEO and Founder of Revolution Precrafted', 'is_rate' => true],
                    ['name' => 'Mr. Ricky Kapur', 'position' => 'Regional Manager of Pacific, Microsoft', 'is_rate' => true],
                    ['name' => 'Mr. Jeffri Cheong', 'position' => 'Managing Director of Gawin.PH', 'is_rate' => true],
                    ['name' => 'Mr. Patrick Ngan', 'position' => 'CEO and Co-founder of QFPay International', 'is_rate' => true],
                ]), 
                'moderators' => new Collection([
                    ['name' => 'Mr. Perry Lam', 'position' => 'Founder and CEO of Lam Institute', 'is_rate' => true]
                ])
            ],
            ['id' => 7, 
                'time' => '11:00 - 12:30 PM', 
                'segment' => 'Forum 2: <b>Digital Platforms as Enablers for Inclusive Growth</b>', 
                'is_rate' => true,
                'resource' => new Collection([
                     ['name' => 'Mr. Henry Motte-Munoz', 'position' => 'CEO and Co-founder of Edukasyon.PH', 'is_rate' => true],
                     ['name' => 'Mr. Kenneth Lingan', 'position' => 'Country Head for Philippines, Google', 'is_rate' => true],
                     ['name' => 'Mr. Lito Villanueva', 'position' => 'Managing Director of FintQ', 'is_rate' => true],
                     ['name' => 'Mr. Paolo Azzola', 'position' => 'COO and Managing Director of Paymaya', 'is_rate' => true],
                     ['name' => 'Mr. Anthony Thomas', 'position' => 'CEO, Mynt', 'is_rate' => true],
                     ['name' => 'Mr. Ray Alimurung', 'position' => 'CEO of Lazada Philippines', 'is_rate' => true],
                     ['name' => 'Mr. Martin Yu', 'position' => 'Associate Director of Shopee', 'is_rate' => true],
                     ['name' => 'Mr. Brian Cu', 'position' => 'President of Grab Philippines', 'is_rate' => true],
                     ['name' => 'Mr. Dennis Ng', 'position' => 'Founder and CEO Mober', 'is_rate' => true],
                     ['name' => 'Ms. Angeline Tham', 'position' => 'CEO of Angkas', 'is_rate' => true],
                     ['name' => 'Ms. Crystal Lee-Gonzalez', 'position' => 'Managaing Director of Honestbee', 'is_rate' => true]
                ]),
                'moderators' => new Collection([
                    ['name' => 'Ms. Issa Litton', 'position' => 'Corporate and Lifestyle Events Host', 'is_rate' => true]
                ])
            ],
            ['id' => 8, 
                'time' => '12:30 - 01:30 PM', 
                'segment' => 'Forum 3: <b>Emerging Enterprises Solving Real-World Problems</b>', 
                'is_rate' => true,
                'resource' => new Collection([
                    ['name' => 'Mr. Mario Jordan "Magellan" Fetalino III', 'position' => 'CEO of Acuden', 'is_rate' => true],
                    ['name' => 'Mr. Andrew Cua', 'position' => 'CEO of Tralulu (Travel)', 'is_rate' => true],
                    ['name' => 'Mr. Andrew Cua', 'position' => 'CEO of Tralulu (Travel)', 'is_rate' => true],
                    ['name' => 'Mr. Paolo Bugayong', 'position' => 'Co-founder and CEO of Aide (Health)', 'is_rate' => true],
                    ['name' => 'Mr. David Foote', 'position' => 'Founder and CEO, Zennya', 'is_rate' => true],
                    ['name' => 'Ms. Audrey Tanco Uy', 'position' => 'Founder and CEO of ServeHappy Jobs (Services/Hospitality)', 'is_rate' => true],
                    ['name' => 'Mr. Leeroy Levias Shoko', 'position' => 'CEO of DibzTech Inc.', 'is_rate' => true],
                    ['name' => 'Mr. Dennis Layug', 'position' => 'Co-founder and Director of Apptitude (Edutech)', 'is_rate' => true],
                    ['name' => 'Mr. Ellard Capiral', 'position' => 'CEO of AdMov', 'is_rate' => true],
                    ['name' => 'Mr. Keb Cuevas', 'position' => 'CEO of Tagapani.ph (Agritech)', 'is_rate' => true],
                ]),
                'moderators' => new Collection([
                    ['name' => 'Gretchen Ho', 'position' => 'TV HOST', 'is_rate' => true]
                ]),
            ],
            ['id' => 9, 
                'time' => '01:30 - 01:55 PM', 
                'segment' => 'Infotainment Paydro', 
                'is_rate' => true,
                'resource' => new Collection([
                    ['name' => 'Mr. Sam Y.G.', 'position' => 'Radio Personality', 'is_rate' => false],
                ]),
                'moderators' => new Collection([
                ])
            ],
            ['id' => 10, 
                'time' => '01:55 - 02:45 PM', 
                'segment' => 'Forum 4: <b>Design Thinking: Translating Creative Mindset to New Business Models</b>', 
                'is_rate' => true,
                'resource' => new Collection([
                    ['name' => 'Ms. Isabel Sieh', 'position' => 'Founder of Girl will Code', 'is_rate' => true],
                    ['name' => 'Ms. Shaira Luna', 'position' => 'Photographer (Photography)', 'is_rate' => true],
                    ['name' => 'Mr.  Gian Javelona', 'position' => 'CEO of Oxygen Ventures (Gaming/App Development)', 'is_rate' => true],
                    ['name' => 'Mr. Dan Matutina', 'position' => 'Graphic Designer & Illustrator and Founding Partner of Plus63 Design Co. (Graphics)', 'is_rate' => true],
                    ['name' => 'Mr. Erwan Heussaff' , 'position' => 'Founder, The Fat Kid Inside, Inc. ( Digital Video Production)', 'is_rate' => true],
                    ['name' => 'Mr. Brian Tenorio' , 'position' => 'Chair, Philippine LGBT Chamber of Commerce', 'is_rate' => true],
                    ['name' => 'Mr. Niel Dagondon' , 'position' => 'President and Co-founder of Edusuite Inc.; Chairman, CIIT College of Arts of Technology (Game Development)', 'is_rate' => true]

                ]),
                'moderators' => new Collection([
                    ['name' => 'Mr. Sam Y.G.', 'Radio Personality', 'is_rate' => true]
                ])
            ],
            ['id' => 11, 
                'time' => '02:45 - 03:45 PM', 
                'segment' => 'Forum 5: <b>Building a Country of Enterprising Filipinos</b>', 
                'is_rate' => true,
                'resource' => new Collection([
                    ['name' => 'Sec. Leonor Briones', 'position' => 'Secretary, Department of Education (DepEd)', 'is_rate' => true],
                    ['name' => 'Mr. Prospero de Vera III', 'position' => 'Chairperson, Commission on Higher Education (CHED)', 'is_rate' => true],
                    ['name' => 'Mr. Neelan Bhatia', 'position' => 'Center Director,  Singapore Polytechnic Innovation Office', 'is_rate' => true]
                ]),
                'moderators' => new Collection([
                    ['name' => 'Ms.Josephine Romero', 'Senior Adviser, ASEAN Business Advisory Council Philippines; and CEO of Apptitude', 'is_rate' => true]
                ])
            ],
            ['id' => 12, 
                'time' => '03:45 - 04:45 PM', 
                'segment' => '<b>Forum 6: YES! Angat Lahat sa Digital!</b> Para sa kabataan at Maliliit na Negosyante<br/><br/>(Launch of the Angat Lahat sa Digital and Mentor Me E-Learning powered by Go Negosyo and Singapore Polytechnic)', 
                'is_rate' => true,
                'resource' => new Collection([
                    ['name' => 'Sen. Grace Poe', 'position' => 'Senate of the Philippines', 'is_rate' => true],
                    ['name' => 'Sen. Cynthia Villar', 'position' => 'Senate of the Philippines', 'is_rate' => true],
                    ['name' => 'Mr. Jose Ma. Concepcion III', 'position' => 'Presidential Adviser Entrepreneurship; Chair, ASEAN Business Advisory Council Philippines; Founder, Go Negosyo', 'is_rate' => true]
                ]),
                'moderators' => new Collection([
                    ['name' => 'Vice Ganda', 'position' => 'TV Presenter, Actor, Singer, and CEO of Vice Cosmetics', 'is_rate' => true]
                ])
            ],
            ['id' => 13, 
                'time' => '04:45 - 05:00 PM', 
                'segment' => '<p>Raffle</p><p>End of Conference Proper</p>', 
                'is_rate' => false,
                'resource' => new Collection([
                ]),
                'moderators' => new Collection([
                ])
            ],
            ['id' => 14, 
                'time' => '05:00 PM', 
                'segment' => '<p>End of Summit</p><p>Concert/Entertainment</p>',
                'is_rate' => false,
                'resource' => new Collection([
                    ['name' => 'AUTOTELIC', 'position' => 'Local Band Performer', 'is_rate' => false]
                ]), 
                'moderators' => new Collection([])
            ]
        ]);

        $this->panelists = new Collection([
             ['id' => 1, 'name' => 'Mr. Jose Ma. Concepcion III', 'position' => 'Presidential Adviser for  Entrepreneurship; Chair, ASEAN Business Advisory  Council Philippines; Founder, Go Negosyo', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/rzddowxlq91206.jpg'],
             ['id' => 2, 'name' => 'Mr. Robbie Antonio', 'position' => 'CEO and Founder of Revolution Precrafted', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/mihvoy7pba1211.jpg'],
             ['id' => 3, 'name' => 'Mr. Ricky Kapur', 'position' => 'Regional Manager of Pacific, Microsoft', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/e44nr1rmy71211.jpg'],
             ['id' => 4, 'name' => 'Mr. Jeffri Cheong', 'position' => 'Managing Director of Gawin.PH', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/g7lpkzyulr1206.jpg'],
             ['id' => 5, 'name' => 'Mr. Patrick Ngan', 'position' => 'CEO and Co-founder of QFPay International', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/v3onhfr6iw1210.jpg'],
             ['id' => 6, 'name' => 'Mr. Henry Motte-Munoz', 'position' => 'CEO and Co-founder of Edukasyon.PH', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/agrkty4idx1205.jpg'],
             ['id' => 7, 'name' => 'Mr. Kenneth Lingan', 'position' => 'Country Head for Philippines, Google', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/q4gb1lflkh1207.jpg'],
             ['id' => 8, 'name' => 'Mr. Lito Villanueva', 'position' => 'Managing Director of FintQ', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/sozi3z4yqe1208.jpg'],
             ['id' => 9, 'name' => 'Mr. Paolo Azzola', 'position' => 'COO and Managing Director of Paymaya', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/whxmyc6ojx1209.jpg'],
             ['id' => 10, 'name' => 'Mr. Anthony Thomas', 'position' => 'CEO, Mynt', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/sfkuxwqpyf1200.jpg'],
             ['id' => 11, 'name' => 'Mr. Ray Alimurung', 'position' => 'CEO of Lazada Philippines', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/gxuggoem0l1211.jpg'],
             ['id' => 12, 'name' => 'Mr. Martin Yu', 'position' => 'Associate Director of Shopee', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/mcnyjpfgi81208.jpg'],
             ['id' => 13, 'name' => 'Mr. Brian Cu', 'position' => 'President of Grab Philippines', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/d3vupsw2k91201.jpg'],
             ['id' => 14, 'name' => 'Mr. Dennis Ng', 'position' => 'Founder and CEO Mober', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/njgqnake051203.jpg'],
             ['id' => 15, 'name' => 'Ms. Angeline Tham', 'position' => 'CEO of Angkas', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/gibuk2y2sz1200.jpeg'],
             ['id' => 16, 'name' => 'Ms. Crystal Lee-Gonzalez', 'position' => 'Managaing Director of Honestbee', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ztvohnyw511201.jpg'],
             ['id' => 17, 'name' => 'Ms. Dannah Majarocon', 'position' => 'Managing Director, Lalamove Philippines', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/f70ibquhtf1202.jpg'],
             ['id' => 18, 'name' => 'Mr. Mario Jordan "Magellan" Fetalino III', 'position' => 'CEO of Acuden', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/46zshwxg6u1208.jpeg'],
             ['id' => 19, 'name' => 'Mr. Andrew Cua', 'position' => 'CEO of Tralulu (Travel)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/nlguocr19m1159.jpg'],
             ['id' => 20, 'name' => 'Mr. Paolo Bugayong', 'position' => 'Co-founder and CEO of Aide (Health)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/a8lnmxiprr1210.jpg'],
             ['id' => 21, 'name' => 'Mr. David Foote', 'position' => 'Founder and CEO, Zennya', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/yke6fuburi1203.jpg'],
             ['id' => 22, 'name' => 'Ms. Audrey Tanco Uy', 'position' => 'Founder and CEO of ServeHappy Jobs (Services/Hospitality)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/bubstj56o41200.jpg'],
             ['id' => 23, 'name' => 'Mr. Leeroy Levias Shoko', 'position' => 'CEO of DibzTech Inc.', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/jwpnjmzbxp1207.jpg'],
             ['id' => 24, 'name' => 'Mr. Dennis Layug', 'position' => 'Co-founder and Director of Apptitude (Edutech)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/atj85qznj01203.jpg'],
             ['id' => 25, 'name' => 'Mr. Ellard Capiral', 'position' => 'CEO of AdMov', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/5bjvalmmrb1204.jpg'],
             ['id' => 26, 'name' => 'Mr. Keb Cuevas', 'position' => 'CEO of Tagapani.ph (Agritech)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/njjayojr1p1207.jpg'],
             ['id' => 27, 'name' => 'Ms. Isabel Sieh', 'position' => 'Founder of Girls will Code  (Coding/programming)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/7ulq1fmzah1205.jpeg'],
             ['id' => 28, 'name' => 'Ms. Shaira Luna​', 'position' => 'Photographer (Photography)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ehlqdc4whv1212.JPG'],
             ['id' => 29, 'name' => 'Mr. Gian Javelona', 'position' => 'CEO of Oxygen Ventures (Gaming/App Development)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/nonvbz2faf1204.jpg'],
             ['id' => 30, 'name' => 'Mr. Dan Matutina', 'position' => 'Grpahic Designer & Illustrator and Founding Partner of Plus63 Design Co. (Graphics)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/fgb4nalbxb1202.jpg'],
             ['id' => 31, 'name' => 'Mr. Erwan Heussaff', 'position' => 'Founder, The Fat Kid Inside, Inc. (Digital Video Production)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/k73kqapayp1204.jpg'],
             ['id' => 32, 'name' => 'Mr. Pepe Diokno', 'position' => 'Film Director, Producer, and Writer', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/vsqmweyckd1210.jpg'],
             ['id' => 33, 'name' => 'Mr. Brian Tenorio', 'position' => 'Chair, Philippine LGBT Chamber of Commerce', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ym0ebyequl1201.jpg'],
             ['id' => 34, 'name' => 'Mr. Niel Dagondon', 'position' => 'President and Co-founder of Edusuite Inc.; Chairman, CIY College of Arts and Technology (Game Development)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/mvsvx6wgw21209.jpg'],
             ['id' => 35, 'name' => 'Sen. Leonor Briones', 'position' => 'Secretary, Department of Education (DepEd)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ulsfb2mzxd1208.jpg'],
             ['id' => 36, 'name' => 'Mr. Prospero de Vera III', 'position' => 'Chairperson, Commission on  Higher Education (CHED)', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/izeq4wivdo1211.jpeg'],
             ['id' => 37, 'name' => 'Mr. Neelesh Bhatia', 'position' => 'Center Director, Singapore Polytechnic Innovation Office', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ta6c7w3ur21209.jpg'],
             ['id' => 38, 'name' => 'Sen. Grace Poe', 'position' => 'Senate of the Philippines', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/7casdebubl1205.jpg'],
             ['id' => 39, 'name' => 'Sen. Cynthia Villar', 'position' => 'Senate of the Philippines', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/7bphg7vytq1201.jpg']
        ]);

        $this->exhibitors = new Collection([
             ['id' => 1, 'name' => 'Voyager - PLDT', 'info' => 'SMART has built an organization with a mindset biased for action, innovation & constant change. Our goal is to come out with market shaping services that are relevant to the needs of our customers. The products we sell, how we do things, the way we are structured, the networks, systems and platforms that support us are all changing and evolving. We need to evolve to an organization that is better positioned for the future new business streams & evolving business models.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/o9ldmj8px41941.png'],
             ['id' => 2, 'name' => 'DragonPay', 'info' => 'Dragonpay provides innovative payment solutions to help merchants accept or disburse payments online.  We focus on alternative payment channels such as banks, mobile payments, ATM’s, and brick-and-mortar payment centers.  By doing so, we provide our merchants convenience, better protection against fraud, and low transaction fees.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/2gnftk5bhi1904.png'],
             ['id' => 3, 'name' => 'First Circle', 'info' => 'First Circle is a Philippine-based financial technology lender that provides strong businesses with supply chain financing to meet larger client demands and improve bargaining position with trading partners. Since our public launch in 2016, we have loaned millions of dollars and served thousands of SMEs from various industries, enabling them to grow their business without worrying about lack of working capital.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/hmflxl7x0l1842.png'],
             ['id' => 4, 'name' => 'GCash Mynt', 'info' => 'Mynt is a fintech startup wholly-owned by Globe Telecom that provides services in payments, remittance, loans, business solutions, and platforms. Mynt also has subsidiaries; GCash, which provides various mobile money solutions, and Fuse, which provides loan products.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/p1art7zuoa1942.png'],
             ['id' => 5, 'name' => 'Angkas', 'info' => 'Angkas is a mobile motorcycle hailing platform designed to provide access to fast, safe and affordable motorbike transport.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/1jh5kvz4of1943.png'],
             ['id' => 6, 'name' => 'Gawin.ph', 'info' => 'Gawin is transforming small and medium sized businesses like never before. They receive thousands of requests for their services on their smartphones, tablets and computers everyday, allowing them to make an instant connection with new clients at an unprecedented rate.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/zpiw1kjqsb1943.png'],
             ['id' => 7, 'name' => 'Honestbee', 'info' => 'Honestbee is an online grocery and food delivery service as its core business, a concierge service, and also a parcel delivery service for its B2B clients. The company provides personal shoppers that pick products for its clients.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/de1o4eyooe1945.png'],
             ['id' => 8, 'name' => 'Lalamove', 'info' => 'Originally known as EasyVan, Lalamove was launched to satisfy a specific logistical need - van hiring. Started in Hong Kong in December 2013, Lalamove now operates in 100+ cities across China and Southeast Asia. Today, Lalamove unceasingly matches 15,000,000 registered customers with a pool of 2,000,000 drivers of vans, trucks and motorcycles. The Lalamove way has revolutionised old van hiring call centres to being so streamlined, customers and drivers match with each other within 12 seconds. Local deliveries are fulfilled at a breakneck 55 minutes, door-to-door. Providing reliable and quick deliveries for customers, Lalamove also optimises drivers fleet and route to maximise their earning potential.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/iq5hru9eei2000.png'],
             ['id' => 9, 'name' => 'Lazada', 'info' => 'LAZADA, the leading e-commerce player in the country, is creating a colossal shift in Filipino buying habits. It continues to evolve and adapt to the needs of its customers by continuously streamlining website and mobile app design, constantly growing the inventory of items, and offering convenient payment options. All these result in a best-in-class online store that delivers effortless shopping experience to every Filipino.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/udufvvqy3k2001.png'],
             ['id' => 10, 'name' => 'Edukasyon', 'info' => 'Edukasyon.ph is the #1 online platform for senior high school and college students looking for schools, courses, scholarships, and student-centric content.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/4woddz5mbl2003.png'],
             ['id' => 11, 'name' => 'Apptitude', 'info' => 'Apptitude is a software development firm born in New Orleans, Louisiana. We focus on creating well-designed, high-performance smartphone apps and websites for businesses, entrepreneurs, and nonprofits.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/g1mp2elghf2027.png'],
             ['id' => 12, 'name' => 'Shopinas', 'info' => 'We are the coolest and happiest online marketplace for awesome and unique local finds. We have everything from food, health, fashion, beauty, accessories, toys, baby products and everything you can buy and shop to fill your happy place. We have a mix of everything Pinoy from the old to the new.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/nxigfgreee2028.png'],
             ['id' => 13, 'name' => 'Shopee', 'info' => 'Shopee is an e-commerce platform headquartered in Singapore under Sea Group, which was founded in 2009 by Forrest Li. Shopee first launched in Singapore in 2015, and since expanded its reach to Malaysia, Thailand, Taiwan, Indonesia, Vietnam and the Philippines.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/4bybmnossv2002.png'],
             ['id' => 14, 'name' => 'Girls Will Code', 'info' => 'Girls Who Code is a nonprofit organization which aims to support and increase the number of women in computer science. The organization is working to close the gender employment difference in technology and change the image of what a programmer looks like.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/lgjfxfkeoc2029.png'],
             ['id' => 15, 'name' => 'Highly Succeed Inc.', 'info' => 'Highly Succeed is a Philippines-based IT servicing and product company that specializes on a wide range of web based services. We are composed of professionals in graphic design, web development, custom application creation, and mobile application.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190306/resized/iwiziatzan2030.png'],
             ['id' => 16, 'name' => 'Mentor Me Mobile Application', 'info' => '', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/vdxlqclts61544.png']
        ]);

        $this->partners = new Collection([
            ['id' => 1, 'type' => "Platinum", 'partners' => new Collection([
                ['id' => 1, 'type_id' => 1, 'name' => 'International Container Terminal Services, Inc.', 'info' => 'International Container Terminal Services, Inc. (ICT) was incorporated on December 24, 1987 to operate, manage and develop the Manila International Container Terminal (MICT), which handles international container cargo at the Port of Manila. The principal business of ICT is the operation, management, development and acquisition of container terminals. The primary mechanism for the operation of these terminals is long-term concession agreements with local port authorities and governments through ICT and its subsidiaries. ICT and its subsidiaries also provide ancillary services such as storage, container stripping and stuffing, inspection, weighing and services for refrigerated containers or reefers, as well as roll-on/roll-off and anchorage services to non-containerized cargoes or general cargoes on a limited basis.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ss3nuweptz1220.png'],
                ['id' => 2, 'type_id' => 1, 'name' => 'PLDT SME Nation', 'info' => 'Since its founding in 2008, PLDT SME Nation has been providing entrepreneurs with technologies suited for their needs – from business-enabling voice, broadband and mobility solutions to more sophisticated enterprise-level ICT and Cloud services that help gear towards the future – all riding on our robust nationwide infrastructure. The company is committed to serving Filipino entrepreneurs by giving them access to mobility, connectivity, support, and hardware – all tailored to meet the needs of enterprises of all sizes. With a wide array of technology tools and solutions dedicated to take MSMEs to the next level, business owners are given the opportunity to take their ventures to even the global e-commerce market and make it big in their respective industries.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/gh58q00ecl1224.png']
             ])
            ],
            ['id' => 2,'type' => "Gold", 'partners' => new Collection([
                ['id' => 1, 'type_id' => 2, 'name' => 'Sta. Elena Construction and Development', 'info' => 'The company is considered as one of the leading construction firms in Foundation Works, specializing in Pre-stress Piling, Bored Piling, Static Piling and Concrete/Metal Sheet Piling, as well as the construction of Ports, Harbors, Marine/Offshore Engineering and Infrastructures such as Bridges, Highways, Horizontal structures and the building of Industrial Plants and Power Plants. With its extensive experience in construction engineering and first rate standard equipment, the company has earned the trust and satisfaction from local and international clients.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/xg6myb0jwn1226.png'],
                ['id' => 2, 'type_id' => 2, 'name' => 'Ayala Corporation', 'info' => 'Ayala Corporation (AC) was founded in 1834, incorporated on January 23, 1968, and was listed on the Philippine Stock Exchange in 1976. AC is the holding company of the Ayala Group of Companies, with principal business interests in real estate and hotels; financial services and insurance; telecommunications; water infrastructure; electronics manufacturing; industrial technologies; automotive; power generation; transport infrastructure; international real estate; healthcare; education; and technology ventures. The significant subsidiaries of AC as of December 31, 2017 are Ayala Land, Inc.; Integrated Micro-Electronics, Inc.; Manila Water Company, Inc.; AC Energy Holdings, Inc.; and AC Industrial Technology Holdings Inc. ', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/dzo7kwttty1201.png'],
                ['id' => 3, 'type_id' => 2, 'name' => 'LT Group, Inc.', 'info' => "LT Group, Inc. (LTG), formerly Tanduay Holdings, Inc. (THI), was incorporated on May 25, 1937 under the name The Manila Wine Merchants, Inc. to engage in the trading business. The Securities and Exchange Commission (SEC) approved the change in corporate name to Asian Pacific Equity Corporation on September 22, 1995 and the change of its primary purpose to that of a holding company. On November 10, 1999, the corporate name was again changed to THI. On September 28, 2012, the SEC approved the change in the corporate name to the present one. After a series of restructuring activities in 2012 and 2013, LTG expanded and diversified its investments to include the beverages; tobacco; property development; and banking businesses. The Company's subsidiaries and associates are Tanduay Distillers, Inc.; Asia Brewery, Incorporated; Fortune Tobacco Corporation; Philippine National Bank; Saturn Holdings, Inc.; Paramount Landequities, Inc.; and Eton Properties Philippines, Inc.", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/sxzsksizba1221.png'],
                ['id' => 4, 'type_id' => 2, 'name' => 'Wilcon Depot', 'info' => 'The Company operates its business and offers its products via two retail formats - the depot store format and the home essentials store format. WLCON has a wide array of product offerings which include local and international brands of tiles and flooring, plumbing and sanitary wares, electrical and lighting products, hardware and tools, furniture, furnishings and houseware, paints, and building materials, among others.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/fz4winbxjq1228.jpg'],
                ['id' => 5, 'type_id' => 2, 'name' => 'Yazaki-Torres Manufacturing Inc.', 'info' => 'Yazaki-Torres Manufacturing Incorporated is the largest wiring harness manufacturer in the Philippines, and likewise the Top Exporter of Automotive Parts. Established in 1974, Yazaki-Torres is a joint venture of the Yazaki group of Japan and the Torres family of the Philippines. The company produces different kinds/variants of wiring harnesses and exports 97% of its production to overseas markets, primarily the United States, with customers such as Ford, Toyota, Chrysler, Mitsubishi, Fuso, and Nissan.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/pdxha8ppxt1228.png'],
                ['id' => 6, 'type_id' => 2, 'name' => 'RFM Corporation', 'info' => 'RFM Corporation (RFM) was incorporated on August 16, 1957 as Republic Flour Mills, Inc. to manufacture flour in the Philippines. From its original business of flour milling, the Company diversified into poultry and livestock production and areas of food manufacturing that includes flour-based products, margarine, milk & juices, canned and processed meat, ice cream, and bottled mineral water. RFM also operates non-food businesses, which include barging services and leasing of commercial/office spaces that serve the internal requirement of the various operating divisions. RFM operates two business segments namely, institutional segment, which primarily manufactures and sells flour, bakery and other bakery products to institutional customers; and consumer segment, which manufactures and sells ice cream, milk and juices, pasta products and flour- and rice-based mixes. The Company is known for the following brands: “White King All-Purpose Flour”; “Fiesta Spaghetti”, “Selecta Moo”; “Sunkist Orange Pulp”; “Vitwater”; “Selecta Magnum”; “Selecta Cornetto”; “Selecta Paddle Pop”; and “Royal Spaghetti”. ', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/aesmnlcicy1225.png']
             ])
            ],
            ['id' => 3, 'type' => "Silver", 'partners' => new Collection([
                ['id' => 1, 'type_id' => 3, 'name' => 'BDO Unibank Inc.', 'info' => 'BDO is a full-service universal bank in the Philippines. It provides a complete array of industry-leading products and services including Lending (corporate and consumer), Deposit-taking, Foreign Exchange, Brokering, Trust and Investments, Credit Cards, Corporate Cash Management and Remittances in the Philippines. Through its local subsidiaries, the Bank offers Investment Banking, Private Banking, Leasing and Finance, Rural Banking, Life Insurance, Insurance Brokerage and Stock Brokerage services.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/lutbbpnjso1214.png'],
                ['id' => 2, 'type_id' => 3, 'name' => 'Cebu Pacific', 'info' => 'Cebu Pacific is the largest carrier in the Philippine air transportation industry, offering low-cost services to more destinations and routes with higher flight frequency within the Philippines than any other airline. CEB currently offers flights to 37 Philippine and 26 international destinations, spanning Asia, Australia, the Middle East, and USA.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/p76pwsyfir1216.png'],
                ['id' => 3, 'type_id' => 3, 'name' => 'First Philippine Holdings Corporation',
                 'info' => "First Philippine Holdings Corporation (FPH) was incorporated and registered with the Securities and Exchange Commission on June 30, 1961. The Company's principal activities consist of investments in real and personal properties including, but not limited to, shares of stocks, notes, securities and entities in the power generation, real estate development, manufacturing and construction, financing and other service industries. FPH's operating businesses are organized and managed separately according to the nature of the products and services, with each segment representing a strategic business unit that offers different products and serves different markets. The Company's major business segments are in power generation, real estate development, manufacturing, and construction and other services. FPH's other activities consist of pipeline service, construction, drilling and sale of merchandise.", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ycv1vx1jol1219.png'],
                ['id' => 4, 'type_id' => 3, 'name' => 'GT Capital Holdings Incorporated', 'info' => 'GT Capital is a listed major Philippine conglomerate with interests in market-leading businesses across banking, property development, infrastructure and utilities, automotive assembly, importation, wholesaling, dealership, and financing, and life and non-life insurance. GT Capital is the primary vehicle for the holding and management of the diversified business interests of the family of Dr. George S.K. Ty in the Philippines.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ywluxick3w1219.png'],
                ['id' => 5, 'type_id' => 3, 'name' => 'San Miguel Corporation', 'info' => 'San Miguel Corporation (SMC) is one of the Philippines’ largest and most diversified conglomerates, generating about 5.2%* of the country’s GDP through its highly integrated operations in food and beverages, packaging, fuel and oil, power, and infrastructure.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/bljo2il98i1226.png'],
                ['id' => 6, 'type_id' => 3, 'name' => 'Uratex', 'info' => 'The Cheng family inaugurated the RGC Group of companies to cater to the growing demands of other industries. This paved way into offering specialized foam for electronics and automotive seating. It also embarked in supplying textile, food packaging, and automotive original equipment manufacturer (OEM) products. Today, Uratex continues to give Filipinos comfortable and affordable mattresses with 21 plants nationwide. It also serves as a key supplier to country’s prominent companies from the hotel and furniture industry.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/lfqq2mqlcj1227.png'],
                ['id' => 7, 'type_id' => 3, 'name' => 'Megaworld', 'info' => 'Megaworld Corporation was founded by Andrew Tan and incorporated under Philippine law on August 24, 1989 to engage in the development, leasing and marketing of real estate. The company initially established a reputation for building high-end residential condominiums and commercial properties located in convenient urban locations with easy access to offices as well as leisure and entertainment amenities in Metro Manila.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/nwrxxvjtrt1222.png'],
                ['id' => 8, 'type_id' => 3, 'name' => 'LBC Business Solutions', 'info' => 'LBC is the Philippines’ market leader in payments and money transfer, documents and mail, parcels and boxes, and cargo and logistics. With a growing network of over 6,400 locations, partners, and agents in over 30 countries, LBC is committed to moving lives, businesses, and communities and delivering smiles around the world. Founded in 1945 as a brokerage and air cargo agent, LBC pioneered express delivery and cargo shipping and 24-hour door-to-door delivery in the Philippines. Today, it is the most trusted courier, cargo and logistics, and remittance service provider of the Global Filipino.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/z1ngjk94u11221.png'],
                ['id' => 9, 'type_id' => 3, 'name' => 'Bounty Fresh Food Inc.', 'info' => 'Bounty Fresh Food, Inc. is the first company in the Philippines to put up a single-stage commercial broiler hatchery. This state-of-the art technology ensures better hatchery sanitation with controlled ventilation that will in turn produce better chick quality. Its main processing plant is one of the few large plants in the country utilizing the latest technology which allows the company to supply its customers with safe quality products, along with the flexibility to offer products to meet customer specifications. For its packaging, Bounty is the pioneer in using the latest tamper-evident packaging materials for packing whole chicken products as well as cut-ups and marinated products.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/fcx26wzyov1214.png'],
                ['id' => 10, 'type_id' => 3, 'name' => 'SL Agritech Corporation', 'info' => 'SL Agritech Corporation (SLAC) is a company engaged in the research, development, production and distribution of hybrid rice seed and premium quality rice. Apart from it being an ISO-certified rice company, it is also the largest local hybrid rice seed company in the Philippines and tropical Asia, and a market leader in premium quality rice. SLAC is the first local company to export high quality milled rice in the country.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/mcfme8dpb11226.png'],
                ['id' => 11, 'type_id' => 3, 'name' => 'Penshoppe', 'info' => 'Established in 1986, Penshoppe is a pioneer in the fashion retail industry and is the multi-awarded flagship brand of Golden ABC. Over the years, Penshoppe has evolved into a lifestyle brand offering a wide variety of merchandise. Today, it is a casual wear brand that is admired for making on-trend apparel & accessories accessible to a broad market with affordable price points. Penshoppe is known for its fresh take on casual wear. The brand delivers apparel that’s stylish yet affordable. As one of the Philippines’ leading fashion retail brand, Penshoppe is recognized for its international appeal and is endorsed by some of the world’s biggest names in fashion, music, movies, and television.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/nrveuylgyc1223.png']
             ])
            ],
            ['id' => 4, 'type' => "Bronze", 'partners' => new Collection([
                ['id' => 1, 'type_id' => 4, 'name' => 'DHL', 'info' => "DHL is a division of the German logistics company Deutsche Post DHL providing international courier, parcel, and express mail services. Deutsche Post DHL is the world's largest logistics company operating around the world, particularly in sea and air mail.", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/vguonjgtka1218.png'],
                ['id' => 2, 'type_id' => 4, 'name' => 'The PTC Group', 'info' => 'In 1979, Ambassador Carlos Salinas founded Philippine Transmarine Carriers (PTC) for Filipinos aspiring for a future in the merchant maritime field. Today, PTC has emerged beyond crew management to offer an integrated value chain of services which include Shipping, Real Estate, Energy, Hospitality & Leisure, Aviation and International Professional Placement. With all hands on deck, the PTC Group is passionately committed to provide world-class PTC-branded professionals, resources and services that develop communities, help build nations and move the world.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/h4rymey8rv1224.png'],
                ['id' => 3, 'type_id' => 4, 'name' => 'All Asian Countertrade Inc.', 'info' => "All Asian Countertrade Inc. was founded in 1994 by Michael L. Escaler with the partnership of Louis Dreyfus and Nissho-Iwai. We began operations with four people and the intention of creating a world-class Filipino trading company that could compete with international trading houses.Our first major transaction was to supply one of the world's largest non-alcoholic beverage manufacturers with six months of sugar at a time of rising prices. Prior to our entry into the market, the company had been unable to secure a steady source of sugar as traders refused to sell them more than a week's supply. Our arrival gave the company access to half of their sugar requirements at a fixed price, a first in the Philippine sugar industry. It was not long before other industrial sugar users came to seek All Asian's exceptional, bespoke  sugar service solutions.", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/19fmhbueey1200.png'],
                ['id' => 4, 'type_id' => 4, 'name' => 'Brother International Philippines Corporation', 'info' => 'Brother International Philippines Corporation which was incorporated last 2000 is one of the sales subsidiary of Brother International Singapore Limited. The whole Brother group is under the umbrella of Brother Industries Limited who originated from Japan with head office located in Nagoya. The Brother Group started by providing repair services for sewing machines in 1908. The history began under the motto of “superior quality and diligent service” while core technologies have been accumulated based on the “spirit of independent development” through this history. And we will continue to provide superior value keeping the spirit of ‘At your side’ to place our customers first everywhere and everytime. Brother International Philippines Inc, carries the following products : Fax Machines, Laser Printers, Color Inkjet Multi Function Center, Mono Laser Multi Function Center, Color LED Multi Function Center, Mono Laser Multi Function Center, Home Sewing Machine, Home Embroidery Machines.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/xddmdrqoyp1216.png'],
                ['id' => 5, 'type_id' => 4, 'name' => 'Buhler Group', 'info' => 'Billions of people come into contact with Bühler technologies to cover their basic needs for food and mobility every day. Two billion people each day enjoy foods produced on Bühler equipment; and one billion people travel in vehicles manufactured with parts produced with our machinery. Countless people wear eye glasses, use smart phones, and read newspapers and magazines, all of which depend on Bühler process technologies and solutions. Having this global relevance, we are in a unique position to turn today’s global challenges into sustainable business. We want every human being to have access to healthy food. We want to do our part to protect the climate with energy-efficient cars, buildings, and machinery. Our motto is creating “innovations for a better world.”', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/dpbjwxno7t1216.png'],
                ['id' => 6, 'type_id' => 4, 'name' => 'CDO Foodsphere Inc.', 'info' => 'Foodsphere’s remarkable growth from an aspiring small-scale industry to a competitive mega-enterprise was powered by the vision of dietitian and enterprising housewife Corazon Dayro-Ong: “To be able to provide the most number of people with the highest quality products at the most affordable prices.” She set out to fulfill this dream when she started selling homemade meat products by knocking on doors of neighbors and friends. Soon, the succulence of her tocino and longaniza gained popularity through word of mouth and orders started to pour in faster than she could prepare them.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/lzkflzykqi1241.png'],
                ['id' => 7, 'type_id' => 4, 'name' => 'Cebuana Lhuillier', 'info' => 'Cebuana Lhuillier Pawnshop commonly known as Cebuana, is a Philippine-based pawnshop, and the flagship brand of PJ Lhuillier Group of Companies. It is also the Philippines’ leading and largest non-bank financial services provider specializing in pawning, remittance, and microinsurance', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/v9utg4k3av1501.png'],
                ['id' => 8, 'type_id' => 4, 'name' => 'Hyundai', 'info' => 'In August 2001, Hyundai Motor Company (HMC) of South Korea appointed Hyundai Asia Resources, Inc. (HARI) as the official distributor of Hyundai vehicles in the Philippines. Driven by a young and dynamic workforce and a fast-growing nationwide fortress of 42 dealerships, they have earned their rightful spot as the third top player in the Philippine automotive industry, and have been consistently recognized by HMC, the Philippine government, and private motoring and consumer bodies for best practices in corporate governance.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/hj1qgq9krb1220.png'],
                ['id' => 9, 'type_id' => 4, 'name' => "Jollibee Foods Corporation",
                 'info' => "Jollibee Foods Corporation (JFC), founded in 1978, is the No. 1 Quick Service Restaurant in the Philippines and the largest Asian food service company. JFC operates the largest food service network in the Philippines. At the end of 2018, we operate 3,126 restaurant outlets in the country: Jollibee, Chowking, Greenwich, Red Ribbon, Mang Inasal, Burger King, PHO24. Abroad, we operate 1,395 stores: Yonghe King (China), Hong Zhuang Yuan (China), Dunkin Donuts (China), Jollibee (Vietnam, Brunei, Hong Kong, Singapore, Macau, United States, Canada, Saudi Arabia, UAE, Qatar, Kuwait, Bahrain, Oman, Italy, United Kingdom), Red Ribbon (United States), Chowking (United States, UAE, Qatar, Oman, Kuwait, Saudi Arabia), Highlands Coffee (Vietnam, Philippines), PHO24 (Vietnam, Indonesia), Hard Rock Café (Vietnam, Hong Kong and Macau) and Smashburger (United States). The JFC Group's worldwise store network reached 4,521 stores.", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/jettdz6tzk1221.png'],
                ['id' => 10, 'type_id' => 4, 'name' => 'Kettle Korn', 'info' => 'Kettle Foods Corporation is the manufacturer of the growing brand of Kettle Korn, established on September 12, 2005. It envisions itself to be the leader in the Snack Foods Category by consistently providing the market with innovative products and services; and maintaining strict standards on quality and value to deliver maximum customer satisfaction.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/swgfqebcy41221.png'],
                ['id' => 11, 'type_id' => 4, 'name' => 'Seaoil', 'info' => "SEAOIL Philippines, Inc. is proudly a FILIPINO company that shares and understands every Filipino's hopes for the future: a better economy, the youth achieving their dreams, a healthy environment. As an organization, SEAOIL gives conscious effort to do its part in the nation's path towards success. Through our innovative products, services, and programs, we remain true to our promise of “Fueling a Better Future.” Now the leading and largest independent fuel company in the Philippines, SEAOIL continues to expand its network to provide quality and affordable products to more Filipinos. We are committed to each and every one of our customers, because it is for them and through them that we are in this business.", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/m9z9tq9lhq1225.png'],
                ['id' => 12, 'type_id' => 4, 'name' => 'Oishi', 'info' => 'From its humble beginnings of “gawgaw” and coffee 70 years ago, Liwayway Marketing Corporation has taken on bigger and more exciting adventures. In 1974,  Oishi was brought to life and has become the much celebrated snack brand, with iconic products like Oishi Prawn Crackers and Kirei. The adventure continued with a dash of creativity and innovation from products like Marty’s Cracklin’ Vegetarian Chicharon, to surprise bursts of chocolate from Pillows. Today, Oishi’s World of O, Wow! Continues to expand, leaving trails of fun and flavorful treats across Asia: China, Vietnam, Myanmar, Thailand, Indonesia, Cambodia, and India.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/gils5wplgq1223.png'],
                ['id' => 13, 'type_id' => 4, 'name' => 'Voyager', 'info' => 'SMART has built an organization with a mindset biased for action, innovation & constant change. Our goal is to come out with market shaping services that are relevant to the needs of our customers. The products we sell, how we do things, the way we are structured, the networks, systems and platforms that support us are all changing and evolving. We need to evolve to an organization that is better positioned for the future new business streams & evolving business models.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/obxeb6ky3h1227.png'],
                ['id' => 14, 'type_id' => 4, 'name' => 'Century Pacific Food Inc.', 'info' => 'Century Pacific Food, Inc. (PSE: CNPF) is the Philippines’ largest canned food company, engaged in the development, marketing, and distribution of processed fish, meat, and dairy products. Its brands, which include Century Tuna, Argentina Corned Beef, 555 Sardines, and Birch Tree, have established market-leading positions and are well-recognized by Filipinos both locally and abroad. CNPF also produces private label tuna products for export to major overseas markets. The Company continues to be the leading Philippine exporter of tuna products according to data from the Philippine Bureau of Customs.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/2bmncdnkm21217.png'],
                ['id' => 15, 'type_id' => 4, 'name' => 'Century Properties', 'info' => 'Century Properties is a leading Philippine real еstatе development company. Our pioneering real еstatе concepts and industry firsts have resulted in some of the most exciting and truly remarkable real еstatе projects in the country.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/fwxvll9csa1217.png'],
                ['id' => 16, 'type_id' => 4, 'name' => 'Concepcion Industrial Corporation', 'info' => 'Concepcion Industrial Corporation (“CIC”) is one of the Philippines’ most established and leading providers of airconditioning solutions and refrigerators. It operates principally through its two subsidiaries, Concepcion-Carrier Airconditioning Company (“CCAC”) and Concepcion Durables, Inc. (“CDI”).', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/xzdgpsdfyb1217.png'],
                ['id' => 17, 'type_id' => 4, 'name' => 'Double Dragon Properties Corp.', 'info' => "DoubleDragon Properties Corp. is a newly listed real estate company led by its Chairman Edgar “Injap” Sia II (founder of Mang Inasal) and the Co-Chairman Tony Tan Caktiong (founder of Jollibee). Having achieved a market capitalization of over $2 Billion in less than two years from IPO, it is now one of the top 5 listed companies on the Philippine Stock Exchange in terms of market capitalization. The Company’s Vision is to accumulate 1 Million square meters of leasable space by 2020. 700,000 square meters of leasable space will be coming from the rollout of 100 community malls in the provincial areas of the Philippines under its subsidiary “CityMall” which is a partnership with SM Investments Corporation. CityMall is poised to become the largest independently branded community mall chain in the country. The balance of 300,000 square meters of leasable space will be coming from two office projects in Metro Manila, namely DD Meridian Park in the Bay Area and Jollibee Tower in Ortigas CBD. The Company's flagship project in Metro Manila is DD Meridian Park, a 4.8 hectare prime commercial block located in the growing central business district of the Bay Area along the corners of Roxas Blvd., EDSA and Macapagal Ave. The first phase, DoubleDragon Plaza, which consists of four BPO Towers on top of a retail podium is currently under construction and slated for completion by 2018. DD Meridian Park, once fully built, will contribute 280,000 square meters of leasable space to DoubleDragon’s portfolio.", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/eu1qb8io0i1218.png'],
                ['id' => 18, 'type_id' => 4, 'name' => 'Marcventures Holdings Inc.', 'info' => 'Marcventures Holdings Inc. (formerly AJO.net Holdings Inc.) (“MHI”) is a corporation duly organized and existing under the laws of the Republic of the Philippines with SEC Registration No. 12942 incorporated on 07 August 1957. It is a publicly-listed company whose shares are actively traded on the Philippine Stock Exchange (PSE) under the stock symbol “MARC”. On December 29, 2017, the Securities and Exchange Commission approved the merger of MHI with Asia Pilot Mining Philippines Corp. (APMPC) and BrightGreen Resources Holdings Inc. (“BHI”) with MHI as the surviving entity. The merger resulted to MHI’s acquisition of APMPC’s subsidiaries, namely, Alumina Mining Philippines Inc. (“AMPI”) and bauxite Resources Inc. (“BARI”) as well as BHI’s subsidiary, BrightGreen Resources Corp. (“BRC”)', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/exnk9jgzuh1222.png'],
                ['id' => 19, 'type' => 4, 'name' => 'Zuellig Pharma', 'info' => 'Zuellig Pharma is one of the largest healthcare services groups in Asia and our purpose is to make healthcare more accessible. We provide world-class distribution, digital and commercial services to support the growing healthcare needs in this region. The company was started almost a hundred years ago and has grown to become a US$10 billion business covering 13 markets with over 10,000 employees. Our people serve over 320,000 medical facilities and work with over 1,000 clients, including the top 10 pharmaceutical companies in the world.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/xco6bd7qro1228.png']

             ])
            ],
            ['id' => 5, 'type' => "Media", 'partners' => new Collection([
                ['id' => 1, 'type_id' => 5, 'name' => 'DZRH', 'info' => 'DZRH is a 24-hour commercial news/talk radio station serving Mega Manila market, which serves as the flagship radio station of the Manila Broadcasting Company in the Philippines. The station has nationwide coverage via its relay stations located within the Philippines.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/dwgyh7uimz1219.png'],
                ['id' => 2, 'type_id' => 5, 'name' => 'Manila Bulletin', 'info' => "The Manila Bulletin, is the Philippines' largest broadsheet newspaper by circulation, followed by the Philippine Daily Inquirer. It bills itself as “The Nation's Leading Newspaper”, which is its official slogan", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/x1mudcya7p1222.png'],
                ['id' => 3, 'type_id' => 5, 'name' => 'The Philippine Daily Inquirer', 'info' => "The Philippine Daily Inquirer, popularly known as the Inquirer, is an English-language newspaper in the Philippines. Founded in 1985, it is often regarded as the Philippines' newspaper of record.", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/4ux6t7ecos1639.png'],
                ['id' => 4, 'type_id' => 5, 'name' => 'The Philippine Star', 'info' => 'The Philippine Star is a print and digital newspaper in the Philippines and the flagship brand of the PhilStar Media Group. First published on 28 July 1986 by veteran journalists Betty Go-Belmonte, Max Soliven and Art Borjal, it is one of several Philippine newspapers founded after the 1986 People Power Revolution.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/g6hvx1sbmo1223.png'],
                ['id' => 5, 'type_id' => 5, 'name' => 'Radyo Natin', 'info' => 'Radyo Natin is a community radio network brand. It has more than 100 stations across the country spread from Claveria and Aparri, Cagayan in the northernmost part to Bongao, Tawi-tawi in the south.', 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/e6zqpl6mxe1224.png'],
                ['id' => 6, 'type_id' => 5, 'name' => "People's Television", 'info' => "People's Television Network, Inc. (PTNI) is a government-owned and controlled corporation created and existing by virtue of RA 7306, as amended by RA 10390. PTNI, carries the brand name PTV, which is the flagship government television network under the supervision of the President Communications Operations Office (PCOO).", 'image' => 'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/vjokxqw0ln1642.png']
             ])
            ]
        ]);

        $this->exhibitors_image = [
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/id15ycdfkc1139.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/sskauqmr5h1319.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/o4ovqvgipc1328.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/p4x4qv5fu81329.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/7thmq2nq6f1330.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/qznfm6ope91331.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/b2ol3mpayc1333.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/havbcwg5z61334.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ghnzouvetw1334.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/r2h1qkvjas1334.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/k41k797sgz1335.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/6t4fllcha41335.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/huyqeoypyy1338.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ontek1u5ov1336.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/acag2meqpq1336.png',
            'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/vdxlqclts61544.png'
        ];

        $this->partners_image = [
            [
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/zkyehytpmz1559.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/4h8svviqsl1559.png'
            ],
            [
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ukcrjrzqlp1558.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/1jtlblarg71557.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ld4bz9pkvp1557.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/liimqawrnu1600.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ng7mi2namd1602.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/yumdlbwhwe1603.png'
            ],
            [
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/qug5jciaag1604.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/2fe31o02bb1604.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/y1tbzdz1ho1605.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/qs7q8koupt1605.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/q2xsxfz4rg1606.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/yd7lnjm77t1606.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/bq0jv4z13y1607.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/tuyotrctik1607.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/oeh2iybfsm1608.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/44qoy1vh801608.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/f15cgekiyw1609.png'
            ],
            [
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/kjnvgnodt01610.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/zqkozytap21611.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/cw2zw6f6ot1613.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/jouiypits91614.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ejkgfrwjee1614.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/lzkflzykqi1241.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/h9m8gecqom1623.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/smynherbjd1624.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/amiythkg151625.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/q5hldsnkwf1626.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ej7mnylvfo1626.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/6wwbyofa3s1627.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/id15ycdfkc1139.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/ayivedxpij1634.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/q8auvuol9y1635.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/e4rfhlc1yt1635.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/v1wwoephej1636.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/iflajq0ot81636.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/wgcnxqiozi1637.png'
            ],
            [
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/4rkilicknq1638.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/3axylifwjj1639.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/leda39wccm1639.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/jvlic40dwz1640.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/mwojtomwui1641.png',
                'http://mentormetesting.azurewebsites.net/uploads/file/20190307/resized/6gns1fh5ia1642.png'
            ]
        ];
	}

	public function print(Request $request, $format = ''){
        $this->data['user'] = User::find($request->auth->id);

        // $pdf = PDF::loadView('pdf.print',$this->data);
        return view('generic.vcard',$this->data);
        // return $pdf->stream("{$this->data['user']->username}.pdf");
    }

    public function list_of_panelists(Request $request, $format = NULL){
        
        $this->response['msg'] = "List of Panelist";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PANELIST_LIST";
        $this->response['data'] = $this->panelists;
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }

    }

    public function show_panelist(Request $request, $format = NULL){

        $this->response['msg'] = "Panelist Details";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PANELIST_DETAIL";
        $this->response['data'] = $this->panelists[$request->get('panelist_id') - 1];
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function list_of_exhibitors(Request $request, $format = NULL){
        
        $this->response['msg'] = "List of Exhibitors";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "EXHIBITORS_LIST";
        $this->response['data'] = $this->exhibitors;
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }

    }

    public function show_exhibitor(Request $request, $format = NULL){

        $this->response['msg'] = "Exhibitors Details";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "EXHIBITORS_DETAIL";
        $this->response['data'] = $this->exhibitors[$request->get('exhibitor_id') - 1];
        $this->response['data']['image'] = $this->exhibitors_image[$request->get('exhibitor_id') - 1];
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function list_of_partners(Request $request, $format = NULL){
        
        $this->response['msg'] = "List of Partners";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PARTNERS_LIST";
        $this->response['data'] = $this->partners;
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }

    }

    public function show_partner(Request $request, $format = NULL){

        $this->response['msg'] = "Partner Details";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PARTNERS_DETAIL";
        $this->response['data'] = $this->partners[$request->get('type_id') - 1]['partners'][$request->get('partner_id') - 1];
        $this->response['data']['image'] = $this->partners_image[$request->get('type_id') - 1][$request->get('partner_id') - 1];
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function list_of_program($format = NULL){
        $programs = new Collection([
            ['id' => 1, 'time' => '07:00 - 09:00 AM', 'segment' => 'Registration<br/>Operation Salvo', 'has_details' => true],
            ['id' => 2, 'time' => '09:00 - 09:15 AM', 'segment' => 'Welcome Message', 'has_details' => true],
            ['id' => 3, 'time' => '09:10 - 09:30 AM', 'segment' => 'Message to the Youth and Small Entrepreneurs', 'has_details' => true],
            ['id' => 4, 'time' => '09:30 - 09:45 AM', 'segment' => 'Special Message', 'has_details' => true],
            ['id' => 6, 'time' => '10:15 - 11:00 AM', 'segment' => 'Forum 1: <b>Innovation through Collaboration</b>', 'has_details' => true],
            ['id' => 7, 'time' => '11:00 - 12:30 PM', 'segment' => 'Forum 1: <b>Digital Platforms as Enablers for Inclusive Growth</b>', 'has_details' => true],
            ['id' => 8, 'time' => '12:30 - 01:30 PM', 'segment' => 'Forum 3: <b>Emerging Enterprises Solving Real-World Problems</b>', 'has_details' => true],
            ['id' => 9, 'time' => '01:30 - 01:55 PM', 'segment' => 'Infotainment Paydro', 'has_details' => true],
            ['id' => 10, 'time' => '01:55 - 02:45 PM', 'segment' => 'Forum 4: <b>Design Thinking: Translating Creative Mindset to New Business Models</b>', 'has_details' => true],
            ['id' => 11, 'time' => '02:45 - 03:45 PM', 'segment' => 'Forum 5: <b>Building a Country of Enterprising Filipinos</b>', 'has_details' => true],
            ['id' => 12, 'time' => '03:45 - 04:45 PM', 'segment' => '<b>Forum 6: YES! Angat Lahat sa Digital!</b> Para sa kabataan at Maliliit na Negosyante<br/><br/>(Launch of the Angat Lahat sa Digital and Mentor Me E-Learning powered by Go Negosyo and Singapore Polytechnic)', 'has_details' => true],
            ['id' => 13, 'time' => '04:45 - 05:00 PM', 'segment' => 'Raffle<br/>End of Conference Proper', 'has_details' => false],
            ['id' => 14, 'time' => '05:00 PM', 'segment' => 'End of Summit<br/>Concert/Entertainment', 'has_details' => true],
        ]);

        $this->response['msg'] = "YES Program";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PROGRAM";
        $this->response['data'] = $programs;
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function show_program(Request $request, $format = NULL){

        $this->response['msg'] = "Program Details";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PROGRAM_DETAIL";
        $this->response['data'] = $this->programs[$request->get('program_id') - 1];
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function rating(RatingRequest $request, $format = NULL){
        $rating = New Rating;
        $rating->fill($request->all());
        $rating->user_id = $request->auth->id;
        $rating->save();

        $this->response['msg'] = "New Rating has been added";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "RATING";
        $this->response['data'] = $rating;
        $this->response_code = 201;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function about($format = NULL){
        $page = Page::find(4);
        if(!$page){
            $this->data['page_title'] = "Page Not Found";
            return view('generic.not_found',$this->data);
        }
        $this->data['page_title'] = $page->title;
        $this->data['page'] = $page;
        return view('generic.page',$this->data);
    }

    public function terms_of_use($format = NULL){
        $page = Page::find(3);
        if(!$page){
            $this->data['page_title'] = "Page Not Found";
            return view('generic.not_found',$this->data);
        }
        $this->data['page_title'] = $page->title;
        $this->data['page'] = $page;
        return view('generic.page',$this->data);
    }

    public function privacy_policy($format = NULL){
        $page = Page::find(1);
        if(!$page){
            $this->data['page_title'] = "Page Not Found";
            return view('generic.not_found',$this->data);
        }
        $this->data['page_title'] = $page->title;
        $this->data['page'] = $page;
        return view('generic.page',$this->data);
    }
}