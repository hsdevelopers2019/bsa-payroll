<?php 

namespace App\Laravel\Controllers\Finance;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\User_finance;
use App\Laravel\Models\Article;
use App\Laravel\Models\LeaveRequest;




/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class DashboardController extends Controller{

  /*
  *
  * @var Array $data
  */
  protected $data;

  public function __construct () {
    $this->data = [];
    parent::__construct();
    array_merge($this->data, parent::get_data());
  }

  public function index(){

    // $this->data['user'] = UserFinance::where('id',auth()->user()->id)->first();
    $this->data['page_title'] = "Dashboard";

    // $this->data['leave_request'] = LeaveRequest::where('status','pending')->count();


    // $this->data['user_count'] = UserFinance::where('id','<>',1)->count();
     return view('finance.index',$this->data);
  }
}