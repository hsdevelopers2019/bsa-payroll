<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\LeaveCategory;
use App\Laravel\Models\LeaveRequest;
use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class ApprovedController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		
	}

	public function index () {

		$this->data['page_title'] = "Approved Leave Request";

		$this->data['leaves'] = LeaveRequest::where('status','=','approve')->get();
		
		return view('system.leave.approved.index',$this->data);
	}

	public function show($id=NULL)
	{
		$this->data['page_title'] = "Approved Request list";

		$this->data['users'] = LeaveRequest::where('id',$id)->get();

		return view('system.leave.approved.show',$this->data);
	}

	

	






}