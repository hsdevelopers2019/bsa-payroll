<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\Department;
use App\Laravel\Models\Attendance;
use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;
use App\Laravel\Requests\System\AbsentRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class AttendanceCategoryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
		$this->data['departments'] = Department::all();
	}

	public function category_a(Request $request,$category=NULL,$date=NULL) {
		// $this->data['users'] = User::where('id','<>','1')->get();

		$this->data['users'] = DB::table('attendances')
								   ->rightjoin('user','attendances.employee_id','=','user.id')
								   ->where('user.id','<>','1')
								   ->where('user.payroll_category',$category)
								   ->where('user.deleted_at',NUll)
								   ->groupBy('user.id')
								   ->get();
		$this->data['page_title'] = "Attendance Management";
		$this->data['departments'] = Department::all();


		return view('system.attendance-category.category_a',$this->data);
	}

	public function category_b(Request $request) {
		$this->data['users'] = User::where('id','<>','1')->get();
		$this->data['page_title'] = "Attendance Management";
		$this->data['departments'] = Department::all();
		$this->data['user'] = User::where('id','<>','1')->where('id',$request->id)->first();

		return view('system.attendance-category.category_b',$this->data);
	}


public function search(Request $request,$department=NULL)
{
	// dd($request->all());
	$this->data['page_title'] = 'result';

$this->data['employees'] = DB::table('attendances')
								   ->rightjoin('user','attendances.employee_id','=','user.id')
								   ->where('user.id','<>','1')
								   ->where('department','=',$request->department)

								   ->groupBy('user.id')

								   ->get();

	// return $this->data['employees'] = DB::Table('user')
	// 								->leftjoin('attendances','attendances.employee_id','=','user.id')
									
	// 								->groupBy('user.id')
	// 								->get();
									
	return view('system.attendance-category.search-result',$this->data);
}

public function search_get(Request $request,$department=NULL)
{
	// dd($request->all());
	$this->data['page_title'] = 'result';

$this->data['employees'] = DB::table('attendances')
								   ->rightjoin('user','attendances.employee_id','=','user.id')
								   ->where('user.id','<>','1')
								   ->where('department','=',$department)

								   ->groupBy('user.id')

								   ->get();

	// return $this->data['employees'] = DB::Table('user')
	// 								->leftjoin('attendances','attendances.employee_id','=','user.id')
									
	// 								->groupBy('user.id')
	// 								->get();
									
	return view('system.attendance-category.search-result',$this->data);
}


	public function store (AbsentRequest $request) {
		 // dd($request->all());
			$attendance = new Attendance;
			 

			
				$check = Attendance::where('employee_id',$request->employee_id)
						    ->where('date_from',$request->date_from)
						    ->where('date_to',$request->date_to)->first();
						    
						    
			if($check)
			{
			session()->flash('notification-status','failed');
				session()->flash('notification-msg',"Attendance already Added");
				return redirect()->back();
					
			}
			else if(Attendance::where('employee_id',$request->employee_id)->where('status','=','a')->first())
			{
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',"Employee has a pending paycheck, Please create paycheck first to procceed ");
				return redirect()->back();	
					
			}
			
			$attendance->fill($request->only('employee_id','date_from','date_to','absent','attendance_status','overtime'));

		try {

			

			if($attendance->save()) {

				// dd($request->all());

				if( $request->date_from == Carbon::now()->startOfMonth()->addDays('15')->format('Y-m-d'))
				{

					Attendance::where('employee_id',$request->employee_id)
							->where('date_from',Carbon::now()->startOfMonth())	
							->update(['attendance_status'=>'aa']);

				}
				
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New account has been added.");

				 return redirect()->route('system.attendance-category.search-get',[$request->department]);
				 
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}