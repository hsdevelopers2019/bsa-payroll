<?php 

namespace App\Laravel\Controllers\System;


/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\Department;
use App\Laravel\Models\Attendance;
use App\Laravel\Models\Deduction;
use App\Laravel\Models\AuditTrail;
use Illuminate\Http\Request;


use App\Laravel\Events\AuditTrailActivity;
/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;
use App\Laravel\Requests\System\AbsentRequest;

// use App\Laravel\Events\AuditTrailActivity;
use Helper, Carbon, Session, Str, PDF, AuditRequest, Auth, Event, Input,DB,Excel;

/**
*
* Classes used for this controller
*/
// use Helper, Carbon, Session, Str;

class AttendanceController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";

		$this->data['departments'] = Department::all();
	}

	public function index (Request $request) {
		$this->data['users'] = User::where('id','<>','1')->get();
		$this->data['page_title'] = "Attendance Management";
		$this->data['departments'] = Department::all();
		$this->data['user'] = User::where('id','<>','1')->where('id',$request->id)->first();

		return view('system.attendance.index',$this->data);
	}

	public function create (Request $request) {
	}

	public function show(Request $request)
	{
		$this->data['page_title'] = "Attendance Management";

	
		$attendance = DB::Table('user')
		 							->join('attendances','attendances.employee_id','=','user.id')
		 							->where('attendances.date_from',$request->date_from)
		 							->where('attendances.date_to',$request->date_to);
		 							// ->where('status','=',NULL);
		 							// ->get();

		 if($attendance->count() == 0)
		{
			session()->flash('notification-status','error');
			session()->flash('notification-msg','No Attendance found for selected cut-off');
			return redirect()->back();
		}
		$this->data['attendance'] = $attendance->paginate('15');
	 
		return view('system.attendance.show',$this->data);
	}
	public function show_all(Request $request,$month,$year,$id=NULL)
	{
		
		$this->data['page_title'] = "Attendance Management";

		$this->data['attendance'] = DB::Table('attendances')
											->leftjoin('user','user.id' ,'=' ,'attendances.employee_id')
											->where('attendances.date_from','=',Carbon::now()->firstOfMonth()->format('Y-m-d'))
											->whereMonth('attendances.date_from',$month)
											->whereYear('attendances.date_from',$year)
											->where('user.payroll_category','=','a')
											->where('user.deleted_at','=',NULL)
											->get();
		$this->data['attendance2'] = DB::Table('attendances')
											->leftjoin('user','user.id','=','attendances.employee_id')
											->where('attendances.date_from','=',Carbon::now()->firstOfMonth()->addDays('15')->format('Y-m-d'))
											->whereMonth('attendances.date_from',$month)
											->whereYear('attendances.date_from',$year)
											->where('user.payroll_category','=','a')
											->where('user.deleted_at','=',NULL)
											->get();

		$this->data['attendance3'] = DB::Table('attendances')
											->leftjoin('user','user.id','=','attendances.employee_id')
											
											->whereMonth('attendances.date_from',$month)
											->whereYear('attendances.date_from',$year)
											->where('user.payroll_category','=','b')
											->get();	
		$this->data['payrolls'] = DB::Table('attendances')
											->leftjoin('user','user.id','=','attendances.employee_id')
											->groupBy('date_from')
											->get();										


		$this->data['employees'] = User::all()->where('id','<>','1');															
	

		return view('system.attendance.show',$this->data);
	}

	public function search(Request $request)
	{
		 // dd($request->all());
		$this->data['page_title'] = "result";
		// $this->data['employee'] = User::find($request->employee_id);

		$this->data['employee'] = DB::Table('user')
									  ->leftjoin('attendances','user.id','=','attendances.employee_id')
									  ->where('user.id','=',$request->employee_id)
									  ->first();

		$this->data['employees'] = User::all()->where('id','<>',1);

		

		return view('system.attendance.search-result',$this->data);
	}
	public function store (Request $request) {
		// return $ip = AuditRequest::header('X-Forwarded-For');
		 // dd($request->all());
			
			$attendance = new Attendance;
			 
				$check = Attendance::where('employee_id',$request->employee_id)
						    ->where('date_from',$request->date_from)
						    ->where('date_to',$request->date_to)->first();
						    
						    
			if($check)
			{
			session()->flash('notification-status','failed');
				session()->flash('notification-msg',"Attendance already Added");
				return redirect()->back();
					
			}
			else if(Attendance::where('employee_id',$request->employee_id)->where('status','=','a')->first())
			{
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',"Employee has a pending paycheck, Please create paycheck first to procceed ");
				return redirect()->back();	
					
			}


		try {

			$attendance->fill($request->only('overtime','employee_id','date_from','date_to','absent','attendance_status'));

			if($attendance->save()) {

				if( $request->date_from == Carbon::now()->startOfMonth()->addDays('15')->format('Y-m-d'))
				{

					Attendance::where('employee_id',$request->employee_id)
							->where('date_from',Carbon::now()->startOfMonth())	
							->update(['attendance_status'=>'aa']);

				}
				

				// if($request->has('parent_id')){
				// $clearance = Attendance::find($request->get('parent_id'));
				// 	if($clearance->parent_id != 0)
				// 	$new_business_clearance->parent_id = $clearance->parent_id;

				// 	else
				// 	$new_business_clearance->parent_id = $request->get('parent_id');

				// return $log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "BUSINESS_INFO_CREATED", 'remarks' => Auth::user()->name." has successfully renew {$new_business_clearance->business_name} information.",'ip' => $ip ,'transaction_id'=>"BSA-BP-".str_pad($new_business_clearance->parent_id, 6, "0", STR_PAD_LEFT)]);
				// } else {

				
				// return $log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "BUSINESS_INFO_CREATED", 'remarks' => Auth::user()->name." has successfully created  information.",'ip' => $ip,'transaction_id' =>"BSA-BP-"] );
				// }



				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New account has been added.");

				return redirect()->back();
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}



	public function edit ($id = NULL) {
		
		$this->data['page_title'] = "Edit record Attendance";

		return view('system.attendance.edit',$this->data);	
	}

	public function update (Request $request, $id = NULL) {
		try {
			

			$compensation = Compensation::where('employee_id',$request->input('employee_id'))
										->update([
												
												'honoraria' => $request->input('honoraria'),
												'tax' => $request->input('tax'),
												'sss' => $request->input('sss'),
												'absent' => $request->input('absent'),
												'death_contribution' => $request->input('death_contribution')
										       ]);
				


			if($compensation) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy (Request $request) {
		try {
			// dd($request->all());
			$attendance = DB::Table('attendances')->where('id',$request->idd)->delete();

			if (!$attendance) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			DB::Table('overtimes')->where('employee_id',$request->employee_id)
									->where('date_from',$request->date_from)
									->where('date_to',$request->date_to)
									->delete();
			session()->flash('notification-status','success');
			session()->flash('notification-msg','Data Removed successfully');
			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}

	}

	public function filter_cutoff(Request $request)
	{

		$this->data['page_title'] = " :: Result";
		 // $user = User::get();

		$this->data['avatars'] = User::where('id','<>','1')->types(['admin','user'])->first();
	
		$filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('date_from','=',$request->input('dateFrom'))
							->where('date_to','=',$request->input('dateTo'));
							// ->first();
		if (!$filter->count() > 0) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.payroll.compensation');
			}
			else
			{
		
			$this->data['users'] = $filter->get();
			return view('system.payroll.filter-result', $this->data);

			}
				
}

public function payslip(Request $request,$id=NULL,$dateTo){

	$this->data['page_title'] = "payslip";
	
	$this->data['users'] = $filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('user.id','=',$id)
							->where('compensation.date_to','=',$dateTo)
							// ->where('compensation.date_from','=',$dateFrom)
							->get()
							->first();
	return view('system.user.guest.payslip',$this->data);
}

}