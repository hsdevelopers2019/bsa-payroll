<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\Deduction;
use App\Laravel\Models\Contribution;
use Illuminate\Http\Request;
use App\Laravel\Requests\System\ContributionRequest;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class ContributionController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
	}

	public function index () {
		 $this->data['users'] = User::where('id','<>','1')->types(['admin','user'])->first();
		return view('pdf.print',$this->data);

	}

	public function create ($id = NULL) {
		$this->data['page_title'] = " :: Employee Account - Add new account";
		$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();
		
		return view('system.deduction.create',$this->data);
	}


	public function store (ContributionRequest $request) {

		$user = Contribution::where('employee_id',$request->employee_id)->first();

		$contribution = new Contribution;
		$contribution->fill($request->all());
		if(!$user)
		{
			try {

				if($contribution->save()) {
					session()->flash('notification-status','success');
					session()->flash('notification-msg',"Contribution has been added.");
					return redirect()->back();
				}
				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');

				return redirect()->back();
			} catch (Exception $e) {
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',$e->getMessage());
				return redirect()->back();
			}
		}else
		{
			$contribution::find($request->employee_id);
			$contribution->sss = $request->sss;
			$contribution->pagibig = $request->pagibig;
			$contribution->philhealth = $request->philhealth;
			$contribution->save();

			try
			{
				if($contribution->save()) {
					session()->flash('notification-status','success');
					session()->flash('notification-msg',"Contribution has been added.");
					return redirect()->back();
				}
				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');

				return redirect()->back();
			}
			catch (Exception $e) 
			{
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',$e->getMessage());
				return redirect()->back();
			}
		}

		
	}




	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: System Account - Edit record";
		$check = Compensation::where('employee_id',$id)->get()->first();

		if($check)
		{
			$user = User::where('id','<>','1')->types(['admin','user'])->first();

		// $comp = User::with(['compensation','user'])->first();

			$this->data['compensation']= DB::table('user')
			->join('compensation','compensation.employee_id','=','user.id')
			->where('user.id','=',$id)
			->get()->first();

		// if (!$user) {
		// 	session()->flash('notification-status',"failed");
		// 	session()->flash('notification-msg',"Record not found.");
		// 	return redirect()->route('system.user.index');
		// }

			$this->data['user'] = $user;
			return view('system.payroll.edit',$this->data);
		}
		else
		{
			$this->data['page_title'] = " :: Employee Account - Add new account";
			$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();
			return view('system.payroll.create',$this->data);
		}


		
	}

	public function update (Request $request, $id = NULL) {
		try {
			

			$compensation = Compensation::where('employee_id',$request->input('employee_id'))
			->update([

				'honoraria' => $request->input('honoraria'),
				'tax' => $request->input('tax'),
				'sss' => $request->input('sss'),
				'absent' => $request->input('absent'),
				'death_contribution' => $request->input('death_contribution')
			]);



			if($compensation) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = User::where('id','<>','1')->types(['admin','customer_service'])->where('id',$id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.user.index');
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}


}