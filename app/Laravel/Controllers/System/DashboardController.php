<?php 

namespace App\Laravel\Controllers\System;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Article;
use App\Laravel\Models\LeaveRequest;




/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB,Excel;

class DashboardController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index(){

		$this->data['user'] = User::where('id',auth()->user()->id)->first();
		$this->data['page_title'] = "Dashboard";

		$this->data['leave_request'] = LeaveRequest::where('status','pending')->count();


		$this->data['user_count'] = User::where('id','<>',1)->count();


		$this->data['days']=  Carbon::now()->daysInMonth;

		 




		 return view('system.index',$this->data);
	}
	public function export_excel(){

		$this->data['page_title'] = "Cash Flow Report";
		
			$header = array_merge([
				"Reference Type", "Description", "Amount", "Payment Method", "Receipt Number", "Cashier","Date" 
			]);

			Excel::create("Exported Cashflow Data", function($excel) use($header){
				$excel->setTitle("Exported Cashflow Data")
			    		->setCreator('HSI')
			          	->setCompany('Highly Suceed Inc.')
			          	->setDescription('Exported Cashflow data.');
			     
			    $excel->sheet('Cashflow', function($sheet) use($header){
			    	$sheet->freezeFirstRow();

			    	$sheet->setOrientation('landscape');

			    	  
			       	$sheet->row(1, $header);

			       	$counter = 2;


			       				$a = User::get();
			       				$b = User::get()->pluck('email');
			       				$c = User::get()->pluck('type');

			       				foreach ($a as $b) {
			       					$data = [$b->name,$b->type,$b->email,$b->password];

								$sheet->row($counter++, $data);
			       				}
					
						});
			    
			})->export('xls');
		}
}