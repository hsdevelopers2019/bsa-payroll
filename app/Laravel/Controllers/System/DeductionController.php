<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\Deduction;
use App\Laravel\Models\Contribution;
use App\Laravel\Models\Payslip;
use Illuminate\Http\Request;
use App\Laravel\Requests\System\DeductionRequest;
use App\Laravel\Requests\System\ContributionRequest;
use App\Laravel\Requests\System\AbsentRequest;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class DeductionController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
	}

	public function index () {
		$this->data['page_title'] = "Deduction";
		$this->data['users'] = User::where('id','<>','1')->get();

          $data = DB::Table('user')
					->select('name')
					->get();
						
	   $a = '[';									
		foreach ($data as $row) {
			$a .= '"'.$row->name.'",' ;
			
		}
		$this->data['employee'] =  $a .="]";

		$this->data['deductions'] = Deduction::all();

		return view('system.payroll.deduction.index',$this->data);
	}

	public function create ($id = NULL) {
		$this->data['page_title'] = " :: Employee Account - Add new account";
		$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();
		
		return view('system.deduction.create',$this->data);
	}

	// public function store (UserRequest $request) {
	public function store (DeductionRequest $request) {
		

			$this->data['payslip'] = Payslip::where('employee_id',$request->employee_id)
    									  ->where('date_from',$request->date_from)
    									  ->where('date_to',$request->date_to)
    									  ->count();

    		if ($this->data['payslip']) {
    			session()->flash('notification-status','failed');
				session()->flash('notification-msg','The Payslip is already created Please procceed to next cut-off');
				return redirect()->back();
    		}
		try {


			$compensation = new Deduction;
			$compensation->fill($request->all());
			


			if($compensation->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Deduction  has been added.");
				return redirect()->back();
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		 }
	}

	public function store_contribution (ContributionRequest $request) {



		$check = DB::Table('deductions')
					->where('employee_id',$request->employee_id)
					 ->where('date_from',$request->date_from)
					 ->where('date_to',$request->date_to)
					 ->where('description',$request->description)->count();
					
					 	
		if($check > 0)
		{
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',$request->description."&nbsp;". "Already added for this Cut-off");
				return redirect()->back();
		}

		try {


			$contribution = new Deduction;
			$contribution->fill($request->all());
			


			if($contribution->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Compensation has been added.");
				return redirect()->back();
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		 }
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: System Account - Edit record";
		$check = Compensation::where('employee_id',$id)->get()->first();

		if($check)
		{
			 $user = User::where('id','<>','1')->types(['admin','user'])->first();

		// $comp = User::with(['compensation','user'])->first();

		$this->data['compensation']= DB::table('user')
				  		->join('compensation','compensation.employee_id','=','user.id')
				  		->where('user.id','=',$id)
				  		->get()->first();

		// if (!$user) {
		// 	session()->flash('notification-status',"failed");
		// 	session()->flash('notification-msg',"Record not found.");
		// 	return redirect()->route('system.user.index');
		// }

		$this->data['user'] = $user;
		return view('system.payroll.edit',$this->data);
		}
		else
		{
			$this->data['page_title'] = " :: Employee Account - Add new account";
			$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();
			return view('system.payroll.create',$this->data);
		}


		
	}

	public function update (Request $request, $id = NULL) {
		try {
			

			$compensation = Compensation::where('employee_id',$request->input('employee_id'))
										->update([
												
												'honoraria' => $request->input('honoraria'),
												'tax' => $request->input('tax'),
												'sss' => $request->input('sss'),
												'absent' => $request->input('absent'),
												'death_contribution' => $request->input('death_contribution')
										       ]);
				


			if($compensation) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = Deduction::where('id',$id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				 return redirect()->back();
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function delete_deduction (Request $request) {
		try {
			$user = Deduction::where('id',$request->id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				 return redirect()->back();
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				 return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function filter_cutoff(Request $request)
	{

		$this->data['page_title'] = " :: Result";
		 // $user = User::get();

		$this->data['avatars'] = User::where('id','<>','1')->types(['admin','user'])->first();
	
		$filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('date_from','=',$request->input('dateFrom'))
							->where('date_to','=',$request->input('dateTo'));
							// ->first();
		if (!$filter->count() > 0) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.payroll.compensation');
			}
			else
			{
		
			$this->data['users'] = $filter->get();
			return view('system.payroll.filter-result', $this->data);

			}
				
}

public function payslip(Request $request,$id=NULL,$dateTo){

	$this->data['page_title'] = "payslip";
	
	$this->data['users'] = $filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('user.id','=',$id)
							->where('compensation.date_to','=',$dateTo)
							// ->where('compensation.date_from','=',$dateFrom)
							->get()
							->first();
	return view('system.payroll.payslip',$this->data);
}



public function absent(AbsentRequest $request)
{
	return $request->all();

	
}

}