<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\EmployeeType;
use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\EmployeeTypeRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class EmployeeTypeController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
	}

	public function index () {
		$this->data['page_title'] = " :: Employee Type - Record Data";
		$this->data['types'] = EmployeeType::get();

		$this->data['users'] = User::where('id','<>','1')->types(['admin','user'])->orderBy('created_at',"DESC")->paginate(5);

	
		return view('system.user.employeeType.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Employee Type - Create New Type";
		$this->data['users'] = User::where('id','<>','1')->types(['admin','user'])->orderBy('created_at',"DESC")->paginate(5);

	
		return view('system.user.employeeType.create',$this->data);
	}

	// public function store (UserRequest $request) {
	public function store (EmployeeTypeRequest $request) {

		try {


			$employeeType = new EmployeeType;

		
			$employeeType->description = $request->description;
			


			if($employeeType->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Employee Type has been added.");
				return redirect()->route('system.employee.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = "Edit";
		$this->data['type']= EmployeeType::where('id',$id)->get()->first();

		return view('system.user.employeeType.edit',$this->data);
		


		
	}

	public function update (EmployeeTypeRequest $request) {
		


			try {
			

				$type = EmployeeType::where('id',$request->id)
										->update(['description' => $request->description]);

			if($type) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->route('system.employee.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy (Request $request) {
		try {
			// $user = User::where('id','<>','1')->types(['admin','customer_service'])->where('id',$id)->first();
			 $employee_type = EmployeeType::where('id',$request->input('idType'));

		

			if($employee_type->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.employee.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}



}