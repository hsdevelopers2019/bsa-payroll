<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Attendance;
use App\Laravel\Models\PersonalInfo;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\AccountInfoRequest;
use App\Laravel\Requests\System\AccountPasswordRequest;
use App\Laravel\Requests\System\PersonalInfoRequest;


/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, Auth, ImageUploader;

class FormController extends Controller{

public function index () {
		$this->data['page_title'] = " :: Form - Record Data";
		$this->data['personalinfos'] = PersonalInfo::orderBy('updated_at',"DESC")->paginate(15);
		// dd('working');
		return view('system.form.index',$this->data);
	}

public function create(){
	$this->data['page_title'] = "Form";
	return view("system.form.create", $this->data);
	}
public function store (PersonalInfoRequest $request) {
		try {
			$new_personal_info = new PersonalInfo;
        	$new_personal_info->fill($request->all());
        	// dd($request->all());
        	$user = $request->user();
			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/users");
			    $new_personal_info->path = $image['path'];
			    $new_personal_info->directory = $image['directory'];
			    $new_personal_info->filename = $image['filename'];
			}
			// $new_article->status = $request->get('status');
			// $new_article->user_id = $request->user()->id;
			if($new_personal_info->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.form.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: PersonalInfo - Edit record";
		$article = PersonalInfo::find($id);

		if (!$article) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.form.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.form.index');	
		}

		$this->data['article'] = $article;
		return view('system.form.edit',$this->data);
	}

	public function update (ArticleRequest $request, $id = NULL) {
		try {
			$article = PersonalInfo::find($id);

			if (!$article) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.form.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.form.index');	
			}
			$user = $request->user();
        	$article->fill($request->only('title','video_url','content','category_id'));
        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/articles");
        	    $article->path = $image['path'];
        	    $article->directory = $image['directory'];
        	    $article->filename = $image['filename'];
        	}

			if($article->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.article.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$article = PersonalInfo::find($id);

			if (!$article) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.form.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.form.index');	
			}

			if($article->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.form.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	}
