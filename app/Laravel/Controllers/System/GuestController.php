<?php 

namespace App\Laravel\Controllers\System;
use DB,PDF;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\LeaveCategory;
use App\Laravel\Models\LeaveRequest;
use App\Laravel\Models\Payslip;
use App\Laravel\Models\Deduction;
use App\Laravel\Models\Attendance;
use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;
use App\Laravel\Requests\System\LeaveRequests;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class GuestController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
	}

	public function index () {

		$this->data['page_title'] = "Request Status";

		$this->data['leave_request'] = LeaveRequest::where('employee_id',auth()->user()->id)->get();

		return view('system.user.guest.leave.index',$this->data);
	}

	public function create_leave () {

		
		$this->data['page_title'] = "Leave Form";
		

		$this->data['leave_categories'] = LeaveCategory::all();

		return view('system.user.guest.leave.create',$this->data);
	}
	
	public function store (LeaveRequests $request) {

		 // return  $request->all();
		$data = new LeaveRequest;

	 	$data->fill($request->all());
		
		if($data->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Request successfully sent.");
				return redirect()->back();
			}



	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: System Account - Edit record";
		$check = Compensation::where('employee_id',$id)->get()->first();

		if($check)
		{
			 $user = User::where('id','<>','1')->types(['admin','user'])->first();

		// $comp = User::with(['compensation','user'])->first();

		$this->data['compensation']= DB::table('user')
				  		->join('compensation','compensation.employee_id','=','user.id')
				  		->where('user.id','=',$id)
				  		->get()->first();

		// if (!$user) {
		// 	session()->flash('notification-status',"failed");
		// 	session()->flash('notification-msg',"Record not found.");
		// 	return redirect()->route('system.user.index');
		// }

		$this->data['user'] = $user;
		return view('system.payroll.edit',$this->data);
		}
		else
		{
			$this->data['page_title'] = " :: Employee Account - Add new account";
			$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();
			return view('system.payroll.create',$this->data);
		}


		
	}

	public function update (Request $request, $id = NULL) {
		try {
			

			$compensation = Compensation::where('employee_id',$request->input('employee_id'))
										->update([
												
												'honoraria' => $request->input('honoraria'),
												'tax' => $request->input('tax'),
												'sss' => $request->input('sss'),
												'absent' => $request->input('absent'),
												'death_contribution' => $request->input('death_contribution')
										       ]);
				


			if($compensation) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = User::where('id','<>','1')->types(['admin','customer_service'])->where('id',$id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.user.index');
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function filter_cutoff(Request $request)
	{

		$this->data['page_title'] = " :: Result";
		 // $user = User::get();

		$this->data['avatars'] = User::where('id','<>','1')->types(['admin','user'])->first();
	
		$filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('date_from','=',$request->input('dateFrom'))
							->where('date_to','=',$request->input('dateTo'));
							// ->first();
		if (!$filter->count() > 0) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.payroll.compensation');
			}
			else
			{
		
			$this->data['users'] = $filter->get();
			return view('system.payroll.filter-result', $this->data);

			}
				
}

// payslip

public function index_payslip(){

	 $id = auth()->user()->id;
	$this->data['page_title'] = "payslip";
	
	$this->data['users'] = Payslip::where('employee_id','=',$id)->get();


							
							
	return view('system.user.guest.payslip.index',$this->data);
}

public function show_payslip(Request $request,$id=NULL,$dateTo,$dateFrom){
      

		$this->data['users'] = User::where('id','<>','1')->where('id',$id)->first();
		$this->data['date'] = $dateTo;
		$this->data['dateFrom'] = $dateFrom;

		$this->data['payslip'] = Deduction::where('employee_id',$id)											
											->where('date_from',$dateFrom)
											->where('date_to',$dateTo)
											->get();

		$this->data['total_net'] = Payslip::where('employee_id',$id)											
											->where('date_from',$dateFrom)
											->where('date_to',$dateTo)
											->first();
		$this->data['absent'] = Attendance::where('employee_id',$id)											
											->where('date_from',$dateFrom)
											->where('date_to',$dateTo)
											->first();								

						

        $pdf = PDF::loadView('pdf.GuestPayslip', $this->data)->setPaper('letter', 'portrait');



        // $log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "BUSINESS_CLEARANCE_PRINT", 'remarks' => Auth::user()->name." has successfully view/print {$this->data['clearance']->business_name} business clearance.",'ip' => $ip]);
        // Event::fire('log-activity', $log_data);

        return $pdf->stream('GuestPayslip.pdf');
    
    }


// public function show_payslip($id=NULL,$dateFrom=NULL,$dateTo=NULL){

	 
// 	$this->data['page_title'] = "payslip";
	
// 	$this->data['users'] = Payslip::where('id',$id)
// 									->where('date_from',$dateFrom)
// 									->where('date_to',$dateTo)
// 									->get();
							
							
// 	return view('system.user.guest.payslip.show',$this->data);
// }

public function payslip(Request $request,$id=NULL,$dateTo){

	$this->data['page_title'] = "payslip";
	
	$this->data['users'] = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('user.id','=',$id)
							->where('compensation.date_to','=',$dateTo)
							// ->where('compensation.date_from','=',$dateFrom)
							->get()
							->first();
	return view('system.user.guest.payslip',$this->data);
}

}