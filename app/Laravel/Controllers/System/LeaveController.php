<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\LeaveCategory;
use App\Laravel\Models\LeaveRequest;
use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class LeaveController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		
	}

	public function index () {

		$this->data['page_title'] = "Leave Category";

		$this->data['categories'] = LeaveCategory::get();
		
		return view('system.leave.index',$this->data);
	}

	public function index_request () {

		$this->data['page_title'] = "Leave Request";

		$this->data['leave_request'] = DB::Table('user')
											->join('leave_requests','leave_requests.employee_id','=','user.id')
											 ->where('leave_requests.status','!=','approve')
											->get();

		return view('system.leave.request.index',$this->data);
	}

	


	public function show_request ($id=NULL) {

		$this->data['page_title'] = "Leave Request";

		$this->data['users'] = DB::Table('user')
								  ->join('leave_requests','leave_requests.employee_id','=','user.id')
								  ->where('leave_requests.id','=',$id)
								  ->get();

		return view('system.leave.request.show',$this->data);
	}




	public function create () {
		
		$this->data['page_title'] = "My Payslip";
		return view('system.guest.leave.create',$this->data);
	}

	// public function store (UserRequest $request) {
	public function store (Request $request) {

			$data = new LeaveCategory;

			// return	$request->all();
			$data->fill($request->all());
			if($data->save())
			{
				return redirect()->back();
			}

	}

	public function edit ($id = NULL) {
		
			$this->data['page_title'] = "Edit ";

			$this->data['category'] = LeaveCategory::where('id',$id)->first();

			return view('system.leave.edit',$this->data);
	}

	public function update (Request $request) {
	

		$data = LeaveCategory::where('id',$request->id)
								->update(['description'=> $request->description]);

		if($data)
		{
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Account has been modified successfully.");
			return redirect()->route('system.leave.index');
		}
	}

	public function destroy ($id = NULL) {
		
	try {
			
			 $employee_type = LeaveCategory::where('id',$id);

	
			if($employee_type->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}

	}

	public function store_approval_request(Request $request)
	{

				try {
			

				$recomendation = LeaveRequest::where('id',$request->id)
										->update(['status' => $request->recomendation]);

			if($recomendation) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Request has been Approved.");
				return redirect()->route('system.request.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
				
	}
	






}