<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\Department;
use App\Laravel\Models\Attendance;
use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;
use App\Laravel\Requests\System\AbsentRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class MandatoryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
	}

	public function index (Request $request) {

		$this->data['page_title'] = "Mandatories Collection";
		

		if( strtoupper( auth()->user()->department != 'FINANCE' ) || auth()->user()->type != 'admin')
		{
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','You are not Allowed to access the page');
			return redirect()->back();
		}
		
		return view('system.collection.index',$this->data);
		
	}

	public function create (Request $request) {

		$this->data['page_title'] = "Create Collections";
		
		return view('system.collection.create',$this->data);
	
	}

	public function show(Request $request)
	{
		$this->data['page_title'] = "Attendance Management";
		 $this->data['attendance'] = DB::Table('user')
		 							->join('attendances','attendances.employee_id','=','user.id')
		 							->where('attendances.date_from',$request->date_from)
		 							->where('attendances.date_to',$request->date_to)
		 							->get();
		
	 
		return view('system.attendance.show',$this->data);
	}

	

	public function store (AbsentRequest $request) {
		
			$attendance = new Attendance;

			$attendance->fill($request->all());
				$check = Attendance::where('employee_id',$request->employee_id)
						    ->where('date_from',$request->date_from)
						    ->where('date_to',$request->date_to)->first();
			if($check)
			{
			session()->flash('notification-status','failed');
				session()->flash('notification-msg',"Attendance already Added");
				return redirect()->back();	
			}


		try {

			if($attendance->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New account has been added.");
				return redirect()->back();
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = "Edit record Attendance";

			return view('system.attendance.edit',$this->data);	
	}

	public function update (Request $request, $id = NULL) {
		try {
			

			$compensation = Compensation::where('employee_id',$request->input('employee_id'))
										->update([
												
												'honoraria' => $request->input('honoraria'),
												'tax' => $request->input('tax'),
												'sss' => $request->input('sss'),
												'absent' => $request->input('absent'),
												'death_contribution' => $request->input('death_contribution')
										       ]);
				


			if($compensation) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy (Request $request) {
		try {
			$attendance = DB::Table('attendances')->where('id',$request->idd)->delete();

			if (!$attendance) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			if($attendance) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function filter_cutoff(Request $request)
	{

		$this->data['page_title'] = " :: Result";
		 // $user = User::get();

		$this->data['avatars'] = User::where('id','<>','1')->types(['admin','user'])->first();
	
		$filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('date_from','=',$request->input('dateFrom'))
							->where('date_to','=',$request->input('dateTo'));
							// ->first();
		if (!$filter->count() > 0) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.payroll.compensation');
			}
			else
			{
		
			$this->data['users'] = $filter->get();
			return view('system.payroll.filter-result', $this->data);

			}
				
}

public function payslip(Request $request,$id=NULL,$dateTo){

	$this->data['page_title'] = "payslip";
	
	$this->data['users'] = $filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('user.id','=',$id)
							->where('compensation.date_to','=',$dateTo)
							// ->where('compensation.date_from','=',$dateFrom)
							->get()
							->first();
	return view('system.user.guest.payslip',$this->data);
}

}