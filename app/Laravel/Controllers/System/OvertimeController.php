<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Overtime;

use Illuminate\Http\Request;
use App\Laravel\Requests\System\DeductionRequest;
use App\Laravel\Requests\System\ContributionRequest;
use App\Laravel\Requests\System\AbsentRequest;
use App\Laravel\Requests\System\OvertimeRequest;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class OvertimeController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
	}

	public function index () {
		$this->data['page_title'] = 'Overtime';

		$this->data['users'] = User::where('id','<>','1')->get();

		return view('system.payroll.overtime.index',$this->data);
	}

	public function create ($id = NULL) {
		$this->data['page_title'] = "Over Time";
		$this->data['users'] = User::where('id','<>','1')->get();
		
		return view('system.payroll.overtime.create',$this->data);
	}


	public function store (OvertimeRequest $request) {
		
		try {


			$overtime = new Overtime;
			$overtime->fill($request->all());
			


			if($overtime->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Overtime  has been added.");
				return redirect()->back();
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		 }
	}

		public function result(Request $request)
	{
		
		$result = DB::Table('user')
						->join('overtimes','user.id','=','Overtimes.employee_id')
						->where('overtimes.date_from',$request->date_from)
						->where('overtimes.date_to',$request->date_to)
						->get();
		return response()->json($result);
	}



	public function edit ($id = NULL) {

		$this->data['page_title'] = " :: System Account - Edit record";
		// $check = Compensation::where('employee_id',$id)->get()->first();

		// if($check)
		// {
		// 	 $user = User::where('id','<>','1')->types(['admin','user'])->first();

		// // $comp = User::with(['compensation','user'])->first();

		// $this->data['compensation']= DB::table('user')
		// 		  		->join('compensation','compensation.employee_id','=','user.id')
		// 		  		->where('user.id','=',$id)
		// 		  		->get()->first();

		// if (!$user) {
		// 	session()->flash('notification-status',"failed");
		// 	session()->flash('notification-msg',"Record not found.");
		// 	return redirect()->route('system.user.index');
		// }

		// $this->data['user'] = $user;
		 $this->data['users'] = Overtime::where('id',$id)->first();
	
		return view('system.payroll.overtime.edit',$this->data);
		// }
		// else
		// {
		// 	$this->data['page_title'] = " :: Employee Account - Add new account";
		// 	$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();
		// 	return view('system.payroll.create',$this->data);
		// }


		
	}

	public function update (Request $request) {
		try {
			// dd($request->id);
			$id = $request->id;

			$user = Overtime::find($request->id);


			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			$user->fill($request->only('date_from','date_to','hours'));
			
			if($user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = Deduction::where('id',$id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				 return redirect()->back();
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function delete_deduction (Request $request) {
		try {
			$user = Deduction::where('id',$request->id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				 return redirect()->back();
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				 return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function filter_cutoff(Request $request)
	{

		$this->data['page_title'] = " :: Result";
		 // $user = User::get();

		$this->data['avatars'] = User::where('id','<>','1')->types(['admin','user'])->first();
	
		$filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('date_from','=',$request->input('dateFrom'))
							->where('date_to','=',$request->input('dateTo'));
							// ->first();
		if (!$filter->count() > 0) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.payroll.compensation');
			}
			else
			{
		
			$this->data['users'] = $filter->get();
			return view('system.payroll.filter-result', $this->data);

			}
				
}

public function payslip(Request $request,$id=NULL,$dateTo){

	$this->data['page_title'] = "payslip";
	
	$this->data['users'] = $filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('user.id','=',$id)
							->where('compensation.date_to','=',$dateTo)
							// ->where('compensation.date_from','=',$dateFrom)
							->get()
							->first();
	return view('system.payroll.payslip',$this->data);
}



public function absent(AbsentRequest $request)
{
	return $request->all();

	
}

}