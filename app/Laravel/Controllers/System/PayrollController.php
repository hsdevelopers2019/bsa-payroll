<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\Deduction;
use App\Laravel\Models\Attendance;
use App\Laravel\Models\Payslip;
use App\Laravel\Models\Overtime;

use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;
use App\Laravel\Requests\System\CreatePayrollRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,PDF;

class PayrollController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		
		$emp = array(''=>'Choose Employee');
		$employees = User::where('id','<>','1')
							->get();

		foreach ($employees as $employee) {
			array_push($emp, $employee->name);
		}



		
		$this->data['employee'] = $emp;


		$this->data['heading'] = "System Account";
	}

	public function index () {



		$this->data['page_title'] = "Employee Account - Payroll";
		
		 $this->data['users'] = User::where('id','<>','1')->get();
		 $this->data['payrolls'] = Attendance::get();

		return view('system.payroll.index',$this->data);
	}

	public function create ($id = NULL) {
		$this->data['page_title'] = " :: Employee Account - Add new account";
		$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();
		
		return view('system.payroll.create',$this->data);
	}

	public function show (Request $request){
		$this->data['page_title'] = "Result";
		$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$request->employee_id)->first();
		$this->data['date'] = $request->date;

		$data =  Deduction::where('payrun',$request->date)
							->where('employee_id',$request->employee_id)
							->get();
		

		if(!$data->count() > 0)
		{
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',"no record found");
				return redirect()->back();
		}

		$this->data['users'] = $data;
		return view('system.payroll.show',$this->data);
	
	}

	// public function store (UserRequest $request) {
	public function store (CompensationRequest $request) {

		try {


			$compensation = new Compensation;
			$compensation->fill($request->all());
			


			if($compensation->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Compensation has been added.");
				return redirect()->route('system.attendance.show_all',[Carbon::now()->format('m'),Carbon::now()->format('Y')]);
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = "System Account - Edit record";
		$check = Compensation::where('employee_id',$id)->get()->first();

		if($check)
		{
			 $user = User::where('id','<>','1')->types(['admin','user'])->first();

		// $comp = User::with(['compensation','user'])->first();

		$this->data['compensation']= DB::table('user')
				  		->join('compensation','compensation.employee_id','=','user.id')
				  		->where('user.id','=',$id)
				  		->get()->first();

		// if (!$user) {
		// 	session()->flash('notification-status',"failed");
		// 	session()->flash('notification-msg',"Record not found.");
		// 	return redirect()->route('system.user.index');
		// }

		$this->data['user'] = $user;
		return view('system.payroll.edit',$this->data);
		}
		else
		{
			$this->data['page_title'] = " :: Employee Account - Add new account";
			$this->data['user'] = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();
			return view('system.payroll.create',$this->data);
		}


		
	}

	public function update (Request $request, $id = NULL) {
		try {
			

			$compensation = Compensation::where('employee_id',$request->input('employee_id'))
										->update([
												
												'honoraria' => $request->input('honoraria'),
												'tax' => $request->input('tax'),
												'sss' => $request->input('sss'),
												'absent' => $request->input('absent'),
												'death_contribution' => $request->input('death_contribution')
										       ]);
				


			if($compensation) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = User::where('id','<>','1')->types(['admin','customer_service'])->where('id',$id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.user.index');
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function filter_cutoff(Request $request)
	{

		$this->data['page_title'] = "Result";
		 // $user = User::get();

		$this->data['avatars'] = User::where('id','<>','1')->types(['admin','user'])->first();
	
		$filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('date_from','=',$request->input('dateFrom'))
							->where('date_to','=',$request->input('dateTo'));
							// ->first();
		if (!$filter->count() > 0) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.payroll.compensation');
			}
			else
			{
		
			$this->data['users'] = $filter->get();
			return view('system.payroll.filter-result', $this->data);

			}
				
}


public function payslip(Request $request,$id=NULL,$dateTo,$dateFrom){
        // $ip = AuditRequest::header('X-Forwarded-For');
        // if(!$ip) $ip = AuditRequest::getClientIp();

		
		$this->data['users'] = User::where('id','<>','1')->where('id',$id)->first();
		$this->data['date'] = $dateTo;
		$this->data['dateFrom'] = $dateFrom;

		$this->data['payslip'] = Deduction::where('employee_id',$id)											
											->where('date_from',$dateFrom)
											->where('date_to',$dateTo)
											->get();

		$this->data['total_net'] = Payslip::where('employee_id',$id)											
											->where('date_from',$dateFrom)
											->where('date_to',$dateTo)
											->first();
		$this->data['absent'] = Attendance::where('employee_id',$id)											
											->where('date_from',$dateFrom)
											->where('date_to',$dateTo)
											->first();								

						

        $pdf = PDF::loadView('pdf.payslip', $this->data)->setPaper('letter', 'portrait');



        // $log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "BUSINESS_CLEARANCE_PRINT", 'remarks' => Auth::user()->name." has successfully view/print {$this->data['clearance']->business_name} business clearance.",'ip' => $ip]);
        // Event::fire('log-activity', $log_data);

        return $pdf->stream('payslip.pdf');
    
    }


    // public function create_payroll(CreatePayrollRequest $request)
    public function create_payroll($id=NUll,$dateFrom=NUll,$dateTo)
    {
    	$this->data['page_title'] = "Payroll";
     	$this->data['user'] = User::where('id','<>','1')->where('id',$id)->first();
     	$basic = User::where('id',$id)->where('basic_pay',NULL)->first();
		

		if($basic)
		{
			session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Please set The basic Pay first to create payroll.");
				return redirect()->back();	
		}
		$attendance = Attendance::where('employee_id',$id)
								  ->where('date_from',$dateFrom)
								  ->where('date_to',$dateTo)
								  ->first();

		if (!$attendance) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Please set The attendance first to create payroll.");
				return redirect()->back();
			}

    	$this->data['payroll'] = Deduction::where('employee_id',$id)
    							->where('date_from',$dateFrom)
    							->where('date_to',$dateTo)
    							->get();


    	$this->data['absent'] = Attendance::where('employee_id',$id)->sum('absent');					

    	$this->data['attendance'] = Attendance::where('employee_id',$id)
    											->where('date_from',$dateFrom)
    											->where('date_to',$dateTo)
    											->get();

    	$this->data['overtime'] = Attendance::where('employee_id',$id)->sum('overtime');	
    	$this->data['payslip'] = Payslip::where('employee_id',$id)
    									  ->where('date_from',$dateFrom)
    									  ->where('date_to',$dateTo)
    									  ->count();
    	


    	$this->data['date_from'] = 	$dateFrom;			
    	$this->data['date_to'] = 	$dateTo;	
    	$this->data['employee_id'] = 	$id;	


    		
    	
    	return view('system.payroll.payroll',$this->data);


    }

    public function store_payroll(Request $request )
    {
    	// dd($request->all());
    	$data = new Payslip;
    	
    	$data->fill($request->all());

    	if ($data->save()) {

    		Attendance::where('employee_id',$request->employee_id)->update(['status'=> 'created']);
    			session()->flash('notification-status','success');
				session()->flash('notification-msg',"Payslip has been created.");
				return redirect()->route('system.attendance.show_all',[Carbon::now()->format('m'),Carbon::now()->format('Y')]);
    	}
    
    	

    }
    public function payroll_created($id =NULL)
    {
    	$this->data['page_title'] = "Payroll List";

    	return view('system.payroll-created.index',$this->data);
    }
public function payslip_view(Request $request,$id=NULL,$dateFrom,$dateTo){
       
		// $this->data['users'] = User::where('id','<>','1')->where('id',$id)->first();
		

		 $this->data['details'] = Payslip::where('employee_id',$id)->where('date_from',$dateFrom)->where('date_to',$dateTo)->first();
		 $this->data['user'] = User::find($id);
		 $this->data['deductions'] = Deduction::where('employee_id',$id)->where('date_from',$dateFrom)->where('date_to',$dateTo)->get();
		 // $this->data['absent'] = Absent::where('employee_id',$id)->where('date_from',$dateFrom)->where('date_to',$dateTo)->first();

		$data = $this->data['total_deductioins'] = Deduction::find($id);
		 if(!$data)
		 {
		 	$this->data['total_deduction'] = '0';
		 }
		 else
		 {
		 	$this->data['total_deduction'] = $data->sum('amount');
		 }

		 
        

        $pdf = PDF::loadView('pdf.payroll-created', $this->data)->setPaper('letter', 'portrait');


        return $pdf->stream('payslip.pdf');
    
    }


}