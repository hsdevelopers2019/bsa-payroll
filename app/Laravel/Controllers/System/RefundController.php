<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\Department;
use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;
use App\Laravel\Requests\System\DepartmentRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class RefundController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
	}

	public function index () {

		$this->data['page_title'] = "Department List";

		$this->data['departments'] = Department::get();
		return view('system.department.index',$this->data);
	}

	public function create (DepartmentRequest $request) {
			
			
	try {

			$department = new Department;
			$department->fill($request->all());
			
	$department->save();

			if($department->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Department has been added.");
				return redirect()->back();
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}

	}

	// public function store (UserRequest $request) {
	public function store (CompensationRequest $request) {

		try {

			$compensation = new Compensation;
			$compensation->fill($request->all());
			


			if($compensation->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Compensation has been added.");
				return redirect()->route('system.payroll.compensation');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		
		$this->data['page_title'] = "Edit Department";

		$this->data['department'] = Department::where('id',$id)->first();

		return view('system.department.edit',$this->data);
	}

	public function update (Request $request) {

		try {
			

				$dapartment = Department::where('id',$request->id)
										->update(['department_description' => $request->department]);

			if($dapartment) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->route('system.department.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = Department::find($id);

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.user.index');
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function filter_cutoff(Request $request)
	{

		$this->data['page_title'] = " :: Result";
		 // $user = User::get();

		$this->data['avatars'] = User::where('id','<>','1')->types(['admin','user'])->first();
	
		$filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('date_from','=',$request->input('dateFrom'))
							->where('date_to','=',$request->input('dateTo'));
							// ->first();
		if (!$filter->count() > 0) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.payroll.compensation');
			}
			else
			{
		
			$this->data['users'] = $filter->get();
			return view('system.payroll.filter-result', $this->data);

			}
				
}

public function payslip(Request $request,$id=NULL,$dateTo){

	$this->data['page_title'] = "payslip";
	
	$this->data['users'] = $filter = DB::Table('compensation')
							->join('user','user.id','=','compensation.employee_id')
							->where('user.id','=',$id)
							->where('compensation.date_to','=',$dateTo)
							// ->where('compensation.date_from','=',$dateFrom)
							->get()
							->first();
	return view('system.user.guest.payslip',$this->data);
}

}