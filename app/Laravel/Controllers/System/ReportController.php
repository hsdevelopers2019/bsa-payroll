<?php 

namespace App\Laravel\Controllers\System;
use DB;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Compensation;
use App\Laravel\Models\Department;
use App\Laravel\Models\Deduction;
use App\Laravel\Models\Attendance;
// use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CompensationRequest;
use App\Laravel\Requests\System\AbsentRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,PDF,Excel,Request;

class ReportController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		

		$this->data['user_types'] = ['' => "Choose Account Type",'admin' => "Administrator",'account_officer' => "Account Officer"];
		$this->data['heading'] = "System Account";
		$this->data['departments'] = Department::all();
	}

	public function index(Request $request,$dateFrom=NULL,$dateTo) {
		// return $dateFrom;
		 // dd($request->all());
		// return $dateFrom . $dateTo . $department;
		$this->data['page_title'] = "Generate Report";


		// $department = $request->department;
		// $date = date('Y-m',strtotime($request->month));

		$sql = 'SELECT p.*,t.*,
					(SELECT GROUP_CONCAT(concat_ws("",s.description,"&nbsp;",s.amount) SEPARATOR "\r\n")FROM deductions s WHERE s.employee_id = p.employee_id and s.date_from = "'.$dateFrom.'" and s.date_to  = "'.$dateTo.'"   ) AS combinedDeduction
     				FROM  payslips p ,user t  WHERE  p.employee_id = t.id AND p.date_from = "'.$dateFrom.'" and p.date_to = "'.$dateTo.'"  GROUP BY p.employee_id Order by p.basic_pay Desc';

    	$reports = DB::select(DB::raw($sql));			

		// if ($reports->) {

		$this->data['reports'] = $reports;

		$pdf = PDF::loadView('pdf.report', $this->data)->setPaper('letter', 'landscape');


        return $pdf->stream('report.pdf');
		
	}

	public function export_excel(Request $request,$dateFrom=NULL,$dateTo) {
		
		
		
		$this->data['page_title'] = "Generate Report";

    	$header = array_merge([
				"Name", "Position", "Honoraria", "Allowance", "Earned Amount","Deduction(s)", "Net Amount Recieve","Payee Signature" 
			]);

			Excel::create( Helper::get_month(Request::segment(3)) ." ".date('d',strtotime(Request::segment(3)))."-".date('d',strtotime(Request::segment(4))) , function($excel) use($header){
				$excel->setTitle("Exported Cashflow Data")
			    		->setCreator('HSI')
			          	->setCompany('Highly Suceed Inc.')
			          	->setDescription('Exported Cashflow data.');
			     
			    $excel->sheet('payroll', function($sheet) use($header){
			    	$sheet->freezeFirstRow();

			    	$sheet->setOrientation('landscape');

			    	  
			       	$sheet->row(1, $header);

			       	$counter = 2;

			       	$sql = 'SELECT p.*,t.name,t.basic_pay,t.payroll_category,t.id,department,
			       	(SELECT GROUP_CONCAT(s.description ,":", s.amount)FROM deductions s WHERE s.employee_id = p.employee_id and s.date_from = "'.Request::segment(3).'" and s.date_to  = "'.Request::segment(4).'"   ) AS combinedDeduction
			       	FROM  payslips p ,user t  WHERE  p.employee_id = t.id AND p.date_from = "'.Request::segment(3).'" and p.date_to = "'.Request::segment(4).'"  GROUP BY p.employee_id order by p.basic_pay';

    
    	 $reports = DB::select(DB::raw($sql));
		
								// $netpay = 0;
			       				foreach ($reports as $employee) {

			       					$cat_b =  Helper::netpay($employee->basic_pay,$employee->total_deduction);
			       					$cat_a =  Helper::netpay($employee->net_pay,$employee->total_deduction);
			       					// $netpay = $net;
			       					$data = [
			       						$employee->department,
			       						$employee->department,
			       						$employee->basic_pay,
			       						'0',
			       						$employee->payroll_category == "b" ? $cat_b:$cat_a ,
			       						$employee->combinedDeduction,
			       						$employee->payroll_category == "b" ? $cat_b:$cat_a ,

			       					];

								$sheet->row($counter++, $data);
			       				}
					
						});
			    
			})->export('xls');		

		// if ($reports->) {

		$this->data['reports'] = $reports;

		$pdf = PDF::loadView('pdf.report', $this->data)->setPaper('letter', 'landscape');


        return $pdf->stream('report.pdf');
		
	}


}