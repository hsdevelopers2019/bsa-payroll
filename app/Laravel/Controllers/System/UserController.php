<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\EmployeeType;
use App\Laravel\Models\Department;
use Illuminate\Http\Request;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\UserRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,DB;

class UserController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());


		$this->data['types'] = ['' => "Choose Event Type", 'regular' => "Regular Working Day", 'regular_holiday' => "Regular Holiday", 'special_holiday' => "Special Non-working Day",'birthday' => "Birthday Celebration",'team_building' => "Team Building/Planning"];

		$this->data['user_types'] = ['user' => "Choose Account Type",'admin' => "Administrator",'user' => "User"];
		$emp_type = array(''=>'Choose Employee Type');
		$employee_types = EmployeeType::get();

	
	

		foreach ($employee_types as $type) {
			array_push($emp_type, $type->description);
		}

		// return $emp_type;
		$this->data['employee_types'] = $emp_type;

		$this->data['heading'] = "System Account";


	}

	public function index () {
		$this->data['page_title'] = "Employee Account - Record Data";
		// $this->data['users'] = User::where('id','<>','1')->get()
		
		 $this->data['users'] = User::where('id','<>','1')->get();

		return view('system.user.index',$this->data);

	}

	public function create () {
		
		$this->data['page_title'] = "Add new account";

		$this->data['departments'] = department::get();
		return view('system.user.create',$this->data);
	}

	public function store (UserRequest $request) {
		// dd($request->all());
		try {
			$new_user = new User;

			$new_user->fill($request->except('password'));

			$new_user->type = $request->get('type');

			$new_user->password = bcrypt($request->get('password'));

			if($new_user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New account has been added.");
				return redirect()->route('system.user.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {

		$this->data['page_title'] = "System Account - Edit record";
		
		 $user = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();

       $this->data['user']= DB::Table('departments')
       				 		->join('user','user.department','=','department_description')
       						->where('user.id',$id)->first();

       	

 		$this->data['contributions']= DB::Table('deductions')
       				->where('deductions.employee_id',$id)
       				->get();


		if (!$user) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.user.index');
		}

		$this->data['departments'] = Department::get();

		return view('system.user.edit',$this->data);
	}

	public function update(Request $request) {

		try {
			$id = $request->id;
			$user = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();


			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			$user->fill($request->except('password'));
			$user->type = $request->get('type');
			if($request->has('password')){
			$user->password = bcrypt($request->get('password'));
			}


			if($user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy (Request $request) {
		// return $request->idType;
		try {
			

			$user = User::where('id',$request->idType);

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function upate_basic_pay(Request $request)
	{
		try {
			$id = $request->id;
			$user = User::where('id','<>','1')->types(['admin','user'])->where('id',$id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			$user->fill($request->except('password'));
			$user->type = $request->get('type');
			if($request->has('password')){
			$user->password = bcrypt($request->get('password'));
			}


			if($user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	



}