<?php

namespace App\Laravel\Jobs\Announcement;

use App\Laravel\Models\User;
use Illuminate\Bus\Queueable;
use App\Laravel\Models\Announcement;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Laravel\Notifications\Announcement\AnnouncementNotification;

class NotifyUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user_notified;
    protected $announcement;

    /**
     * Create a new job instance.
     *
     * @param  String $user_notified 
     * @param  Announcement  $announcement
     * @return void
     */
    public function __construct($user_notified = "all" ,Announcement  $announcement)
    {
        $this->user_notified = $user_notified;
        $this->announcement = $announcement;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch($this->user_notified){
            case 'mentee':
            case 'mentor':
                $users = User::where('type',$this->user_notified)->orderBy('last_activity',"DESC")->get();
            break;

            default:
                $users = User::where('type',['mentor','mentee'])->orderBy('last_activity',"DESC")->get();
        }

        foreach($users as $index => $user){
            $user->notify(new AnnouncementNotification($this->announcement));
        }

        
    }
}