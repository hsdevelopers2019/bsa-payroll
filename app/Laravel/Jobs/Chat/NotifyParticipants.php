<?php

namespace App\Laravel\Jobs\Chat;

use App\Laravel\Models\User;
use Illuminate\Bus\Queueable;
use App\Laravel\Models\ChatParticipant;
use App\Laravel\Models\Chat;
use App\Laravel\Models\ChatConversation;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Laravel\Notifications\Chat\NewChatNotification;
use App\Laravel\Notifications\Chat\NewMemberNotification;
use App\Laravel\Notifications\Chat\NewMessageNotification;
use App\Laravel\Notifications\Chat\NewFileMessageNotification;
use App\Laravel\Notifications\Chat\NewNameNotification;


class NotifyParticipants implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $chat,$type;
    protected $except_ids = [];
    protected $other_user;
    protected $message_id = 0;
    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @param  Chat  $chat
     * @return void
     */
    public function __construct(User $user, Chat $chat,$type = "NEW",array $except_ids,$message_id = NULL)
    {
        $this->user = $user;
        $this->chat = $chat;
        $this->type = $type;
        $this->except_ids = $except_ids;
        $this->message_id = $message_id;

        if(count($except_ids) == "2"){
            //2nd index should be the mentioned user
            $this->other_user = User::find($except_ids[1]);
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $chat = Chat::find($this->chat->id);
        $participants = ChatParticipant::where('chat_id', $this->chat->id)->whereNotIn('user_id',$this->except_ids)->get();

        foreach ($participants as $key => $participant) {
            if($participant->author){
                switch($this->type){
                    case 'UPDATE_NAME':
                        $participant->author->notify(new NewNameNotification($this->user,$this->chat));
                    break;
                    case 'NEW_MEMBER':
                        $participant->author->notify(new NewMemberNotification($this->user,$this->chat,$this->other_user));
                    break;

                    case 'NEW_JOIN':
                        $participant->author->notify(new JoinMemberNotification($this->user,$this->chat,$this->other_user));
                    break;

                    case 'NEW_MESSAGE':
                        $message = ChatConversation::find($this->message_id);
                        if($message){
                            $participant->author->notify(new NewMessageNotification($this->user,$this->chat,$message));
                        }
                    break;

                    case 'NEW_FILE_MESSAGE':
                        $message = ChatConversation::find($this->message_id);
                        if($message){
                            $participant->author->notify(new NewFileMessageNotification($this->user,$this->chat,$message));
                        }
                    break;

                    default:
                        $participant->author->notify(new NewChatNotification($this->user,$this->chat));
                }

            }
        }
    }
}