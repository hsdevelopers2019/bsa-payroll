<?php

namespace App\Laravel\Jobs\Chat;

use App\Laravel\Models\User;
use Illuminate\Bus\Queueable;
use App\Laravel\Models\ChatParticipant;
use App\Laravel\Models\Chat;
use App\Laravel\Models\ChatConversation;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Laravel\Notifications\Chat\NewChatNotification;
use App\Laravel\Notifications\Chat\NewMemberNotification;
use App\Laravel\Notifications\Chat\NewMessageNotification;
use App\Laravel\Notifications\Chat\NewFileMessageNotification;
use App\Laravel\Notifications\Chat\NewNameNotification;
use App\Laravel\Notifications\Chat\PublicNewChatNotification;

class NotifyUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $chat,$type;
    protected $except_ids = [];
    protected $other_user;
    protected $message_id = 0;
    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @param  Chat  $chat
     * @return void
     */
    public function __construct(User $user, Chat $chat,$type = "NEW",array $except_ids)
    {
        $this->user = $user;
        $this->chat = $chat;
        $this->type = $type;
        $this->except_ids = implode(",", $except_ids);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::whereRaw("id NOT IN ($this->except_ids)")->get();
        foreach($users as $index => $user ){
            $user->notify(new PublicNewChatNotification($this->user,$this->chat));
        }
    }
}