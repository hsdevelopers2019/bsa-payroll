<?php

namespace App\Laravel\Jobs\Mentorship;

use App\Laravel\Models\User;
use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Laravel\Notifications\Mentorship\MenteeSearchNotification;


class NotifyActiveMentors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $users_id;
    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @param  Chat  $chat
     * @return void
     */
    public function __construct(array $users_id)
    {
        $this->users_id = $users_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $chat = Chat::find($this->chat->id);

        foreach($this->users_id as $index => $user_id){
            $user = User::find($user_id);
            $user->notify(new MenteeSearchNotification("yes"));
        }
    }
}