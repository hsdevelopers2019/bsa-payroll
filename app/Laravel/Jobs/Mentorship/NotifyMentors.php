<?php

namespace App\Laravel\Jobs\Mentorship;

use App\Laravel\Models\User;
use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Laravel\Notifications\Mentorship\MenteeSearchNotification;


class NotifyMentors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $specialty_id;
    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @param  Chat  $chat
     * @return void
     */
    public function __construct($specialty_id = NULL)
    {
        $this->specialty_id = $specialty_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $chat = Chat::find($this->chat->id);
        $mentors = User::where('specialty_id',$this->specialty_id)->where('type','mentor')->where('is_approved','yes')->get();

        foreach ($mentors as $key => $mentor) {
            $mentor->notify(new MenteeSearchNotification());
        }
    }
}