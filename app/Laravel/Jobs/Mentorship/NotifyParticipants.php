<?php

namespace App\Laravel\Jobs\Mentorship;

use App\Laravel\Models\User;
use App\Laravel\Models\Mentorship;
use App\Laravel\Models\MentorshipParticipant;
use App\Laravel\Models\MentorshipConversation;


use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Laravel\Notifications\Mentorship\NewMentorshipNotification;
use App\Laravel\Notifications\Mentorship\NewMessageNotification;
use App\Laravel\Notifications\Mentorship\NewFileMessageNotification;


use Carbon;

class NotifyParticipants implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mentee_id,$mentor_id;
    protected $type,$mentorship,$except_ids,$message_id,$user;
    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @param  Chat  $chat
     * @return void
     */
    public function __construct(array $participants_ids,$type = "NEW_MENTORSHIP",$mentorship_id = 0,array $except_ids,$message_id = NULL)
    {
        $this->mentee_id = isset($participants_ids[0]) ? $participants_ids[0] : "0";
        $this->mentor_id = isset($participants_ids[1]) ? $participants_ids[1] : "0";
        $this->type = $type;
        $this->mentorship = Mentorship::find($mentorship_id);

        $this->except_ids = $except_ids;
        $this->message_id = $message_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $chat = Chat::find($this->chat->id);


        switch($this->type){
            case "NEW_MENTORSHIP":
                $when = Carbon::now()->addSeconds(7);
                $mentee = User::find($this->mentee_id);
                if($mentee) $mentee->notify((new NewMentorshipNotification($this->mentorship,"mentee"))->delay($when));

                $mentor = User::find($this->mentor_id);
                if($mentor) $mentor->notify((new NewMentorshipNotification($this->mentorship,"mentor"))->delay($when));
            break;

            case 'NEW_MESSAGE':
                $participants = MentorshipParticipant::where('mentorship_id', $this->mentorship->id)->whereNotIn('user_id',$this->except_ids)->get();
                foreach($participants as $index => $participant){
                    $message = MentorshipConversation::find($this->message_id);
                    if($message){
                        $this->user = $message->sender;
                        $participant->author->notify(new NewMessageNotification($this->user,$this->mentorship,$message));
                    }
                }
                
            break;

            case 'NEW_FILE_MESSAGE':
                $participants = MentorshipParticipant::where('mentorship_id', $this->mentorship->id)->whereNotIn('user_id',$this->except_ids)->get();
                foreach($participants as $index => $participant){
                    $message = MentorshipConversation::find($this->message_id);
                    if($message){
                        $this->user = $message->sender;
                        $participant->author->notify(new NewFileMessageNotification($this->user,$this->mentorship,$message));
                    }
                }
            break;
        }   
    }
}