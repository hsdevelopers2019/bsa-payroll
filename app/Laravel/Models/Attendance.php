<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
    	"employee_id","date_from","date_to","absent","status","attendance_status","overtime",
    ];
}
