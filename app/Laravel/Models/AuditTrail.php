<?php

namespace App\Laravel\Models;
use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use Str, Helper;

class AuditTrail extends Model
{

	protected $table = "audit_trail";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','remarks','process','ip','transaction_id'
    ];

    public $timestamps = true;




    public function scopeKeyword($query,$keyword){
        if($keyword){
            $key = Str::lower($keyword);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("remarks LIKE '%{$key}%'")
                          ->orWhereRaw("process LIKE '%{$key}%'");
                          
            });

        }
    }

    public function scopeDateRange($query,$from,$to){
        return $query->where(function($query) use($from){
                    if($from){
                        $_from = Helper::date_db($from); 
                        return $query->orWhereRaw("DATE(created_at) >= '{$_from}'");
                    }
                })->where(function($query) use ($to){
                    if($to){
                        $_to = Helper::date_db($to); 
                        return $query->orWhereRaw("DATE(created_at) <= '{$_to}'");
                    }
                });
    }

}
