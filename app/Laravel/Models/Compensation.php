<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon, Helper,Str;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Eloquent;

class Compensation extends Model
{
     protected $fillable = [
        'employee_id', 'honoraria', 'sss','tax','death_contribution',
        'absent','date_from','date_to',
         ];

      public function user()
    	{
        	return $this->belongsTo('App\Laravel\Models\User');
    	}
}
