<?php

namespace App\Laravel\models;

use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
    protected $fillable = [
        'employee_id','sss', 'pagibig','philhealth',
         ];

}
