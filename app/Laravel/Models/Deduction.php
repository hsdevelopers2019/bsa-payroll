<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon, Helper,Str;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Eloquent;

class Deduction extends Model
{
     protected $fillable = [
        'employee_id', 'description','amount','date_from','date_to','status',
         ];

     
}
