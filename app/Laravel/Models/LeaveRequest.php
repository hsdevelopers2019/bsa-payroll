<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{
    protected $fillable = [
    						"employee_id",
    						"name",
    						"department",
    						"date_file",
    						"position",
    						"salary",
    						"leave",
    						"salary",
    						"working_days",
    						"date_from",
    						"date_to",
    						 "where",
    						"incase",
                            "commutation",
    						"status",

    					];
}
