<?php

namespace App\Laravel\models;

use Illuminate\Database\Eloquent\Model;

class Overtime extends Model
{
    protected $fillable = ["hours","employee_id","date_from","date_to","date"];
}
