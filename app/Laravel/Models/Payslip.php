<?php

namespace App\Laravel\models;

use Illuminate\Database\Eloquent\Model;

class Payslip extends Model
{
    protected $fillable=["employee_id","date_from","date_to","gross_pay","total_deduction","overtime","net_pay","basic_pay","absent"];
}
