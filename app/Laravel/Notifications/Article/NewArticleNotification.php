<?php

namespace App\Laravel\Notifications\Article;

use App\Laravel\Models\User;
use App\Laravel\Models\Article;

use App\Laravel\Notifications\MainNotification;
use Helper;

class NewArticleNotification extends MainNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,Article $article)
    {
        $data = [
            'type' => "ARTICLE",
            'reference_id' => $article->id,
            'title' => "{$article->title}",
            'content' => "{$user->name} published new article in {$article->category->name}.",
            'thumbnail' => $article->thumbnail,
        ];

        $this->setData($data);
    }
}