<?php

namespace App\Laravel\Notifications\Chat;

use App\Laravel\Models\User;
use App\Laravel\Models\Chat;

use App\Laravel\Notifications\MainNotification;
use Helper;

class NewMemberNotification extends MainNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,Chat $chat,User $new_member)
    {
        $data = [
            'type' => "CHAT",
            'reference_id' => $chat->id,
            'title' => "{$chat->title}",
            'content' => "{$user->name} added {$new_member->name} to the group.",
            'thumbnail' => $new_member->thumbnail,
        ];

        $this->setData($data);
    }
}
