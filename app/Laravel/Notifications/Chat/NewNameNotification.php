<?php

namespace App\Laravel\Notifications\Chat;

use App\Laravel\Models\User;
use App\Laravel\Models\Chat;

use App\Laravel\Notifications\MainNotification;
use Helper;

class NewNameNotification extends MainNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,Chat $chat)
    {
        $data = [
            'type' => "CHAT",
            'reference_id' => $chat->id,
            'title' => "",
            'content' => "{$user->name} changed the group name to {$chat->title}.",
            'thumbnail' => $chat->thumbnail,
        ];

        $this->setData($data);
    }
}
