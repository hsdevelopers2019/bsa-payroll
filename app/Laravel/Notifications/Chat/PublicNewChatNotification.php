<?php

namespace App\Laravel\Notifications\Chat;

use App\Laravel\Models\User;
use App\Laravel\Models\Chat;

use App\Laravel\Notifications\FCMNotification;
use Helper;

class PublicNewChatNotification extends FCMNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,Chat $chat)
    {
        $data = [
            'type' => "CHAT",
            'reference_id' => $chat->id,
            'title' => "",
            'content' => "Mentor {$user->name} created a group chat named '{$chat->title}'",
            'thumbnail' => $chat->thumbnail,
        ];

        // $this->setData($data);
        parent::__construct($data);
    }
}
