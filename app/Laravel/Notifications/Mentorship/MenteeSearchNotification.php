<?php

namespace App\Laravel\Notifications\Mentorship;


use App\Laravel\Notifications\FCMNotification;
use Helper;

class MenteeSearchNotification extends FCMNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($is_active = "no")
    {
        if($is_active == "yes"){
            $data = [
                'type' => "SEARCH",
                'reference_id' => "",
                'title' => "",
                'content' => "Some mentee needs your help. Please stand by.",
                'thumbnail' => "",
            ];
        }else{
            $data = [
                'type' => "SEARCH",
                'reference_id' => "",
                'title' => "",
                'content' => "Some mentee needs your help. Be online now!",
                'thumbnail' => "",
            ];
        }
        

        parent::__construct($data);
    }
}
