<?php

namespace App\Laravel\Notifications\Mentorship;


use App\Laravel\Notifications\FCMNotification;
use Helper;
use App\Laravel\Models\Mentorship;


class NewMentorshipNotification extends FCMNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Mentorship $mentorship,$type = NULL)
    {
        switch ($type) {
            case "mentee":
                $data = [
                    'type' => "MENTORSHIP",
                    'reference_id' => $mentorship->id,
                    'title' => "",
                    'content' => "We found a mentor for you. Start asking your concern now!",
                    'thumbnail' => "",
                ];
                parent::__construct($data);

            break;

            case "mentor":
                $data = [
                    'type' => "MENTORSHIP",
                    'reference_id' => $mentorship->id,
                    'title' => "",
                    'content' => "We assigned a mentee for you that needs your help.",
                    'thumbnail' => "",
                ];

                parent::__construct($data);
            break;
        }

    }
}
