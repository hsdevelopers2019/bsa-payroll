<?php

namespace App\Laravel\Notifications\Mentorship;

use App\Laravel\Models\User;
use App\Laravel\Models\Mentorship;
use App\Laravel\Models\MentorshipConversation;

use App\Laravel\Notifications\FCMNotification;
use Helper;

class NewMessageNotification extends FCMNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,Mentorship $mentorship,MentorshipConversation $message)
    {
        $data = [
            'type' => "MENTORSHIP",
            'reference_id' => $mentorship->id,
            'title' => "{$user->name}",
            'content' => "{$message->content}",
            'thumbnail' => $user->thumbnail,
        ];

        parent::__construct($data);
        // $this->setData($data);
    }
}
