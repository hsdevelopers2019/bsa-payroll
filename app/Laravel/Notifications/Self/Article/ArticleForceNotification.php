<?php

namespace App\Laravel\Notifications\Self\Article;

use App\Laravel\Models\Article;
use App\Laravel\Notifications\FCMNotification;
use Helper;

class ArticleForceNotification extends FCMNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Article $article)
    {
        $data = [
            'type' => "ARTICLE",
            'reference_id' => $article->id,
            'title' => "Force notification to the owner.",
            'content' => $article->title,
            'thumbnail' => $article->thumbnail,
        ];

        parent::__construct($data);
    }
}
