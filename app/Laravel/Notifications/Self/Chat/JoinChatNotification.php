<?php

namespace App\Laravel\Notifications\Self\Chat;

use App\Laravel\Models\Chat;
use App\Laravel\Models\User;

use App\Laravel\Notifications\SelfNotification;
use Helper;

class JoinChatNotification extends SelfNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Chat $chat,User $user)
    {
        $data = [
            'type' => "CHAT",
            'reference_id' => $chat->id,
            'title' => "{$chat->title}",
            'content' => "{$user->name} added you in the group chat.",
            'thumbnail' => $chat->thumbnail,
        ];

        $this->setData($data);
    }
}
