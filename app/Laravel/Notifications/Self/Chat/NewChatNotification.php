<?php

namespace App\Laravel\Notifications\Self\Chat;

use App\Laravel\Models\Chat;
use App\Laravel\Notifications\SelfNotification;
use Helper;

class NewChatNotification extends SelfNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Chat $chat)
    {
        $data = [
            'type' => "CHAT",
            'reference_id' => $chat->id,
            'title' => "Group",
            'content' => "New group chat created.",
            'thumbnail' => $chat->thumbnail,
        ];

        $this->setData($data);
    }
}
