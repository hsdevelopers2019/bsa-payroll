<?php

namespace App\Laravel\Notifications\Self\Greetings;

use App\Laravel\Notifications\SelfNotification;
use Helper;


class WelcomeNotification extends SelfNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        $data = [
            'type' => "GREETING",
            'reference_id' => "0",
            'title' => "",
            'content' => "Welcome to Mentor Me App.",
            'thumbnail' => "",
        ];

        $this->setData($data);
    }
}
