<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class ChatNameRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'title' => "required|max:50",
        ];

        return $rules;
    }

    public function messages() {

        return [
            'max' => "Group name should not exceed to :max characters.",
            'required' => "Please indicate new name of the group.",
        ];
    }
}
