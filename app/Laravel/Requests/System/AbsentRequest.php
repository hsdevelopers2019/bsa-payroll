<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class AbsentRequest extends RequestManager{

	public function rules(){

	 // $id = $this->route('id')?:0;

		$rules = [
			'employee_id'   => "required",
			'date_from'		=> "required",
			'date_to'		=> "required",
			'absent'		=> "required",
			'attendance_status'		=> "required",
			'overtime'		=> "required",
			'department'     =>'required',
			
		];
// if($id != 0){
// 			$rules['password'] = "confirmed";
// 		}
		

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "This Field is required.",
			'employee_id.required'	=> "This Field is required.",
			
		];
	}
}