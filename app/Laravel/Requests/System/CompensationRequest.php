<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class CompensationRequest extends RequestManager{

	public function rules(){

		// $id = $this->route('id')?:0;

		$rules = [
			'employee_id'	=> "required",
			'sss'	=> "required",
			'tax'	=> "required",
			'honoraria'	=> "required",
			'absent'	=> "required",
			'loan'	=> "required",
			'date_to' => "required",
			'date_from' => "required",
			'death_contribution'	=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			// 'name.unique'	=> "Category name already used. Please double check your input.",
			'required'	=> "Field is required.",
		];
	}
}