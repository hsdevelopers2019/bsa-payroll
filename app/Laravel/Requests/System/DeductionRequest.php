<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class DeductionRequest extends RequestManager{

	public function rules(){

	 $id = $this->route('id')?:0;

		$rules = [
			'employee_id'		=> "required",
			'description'		=> "required",
			'amount'		=> "required",
			'date_from'		=> "required",
			'date_to'		=> "required",
			
		];
if($id != 0){
			$rules['password'] = "confirmed";
		}
		

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Description Field is required.",
			
		];
	}
}