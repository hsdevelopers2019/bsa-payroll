<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class DepartmentRequest extends RequestManager{

	public function rules(){

	 $id = $this->route('id')?:0;

		$rules = [
			'department_description'		=> "required|unique:departments",
		
		];
if($id != 0){
			$rules['password'] = "confirmed";
		}
		

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Description Field is required.",
			'unique:departments' => 'Department Already Exist',
			
		];
	}
}