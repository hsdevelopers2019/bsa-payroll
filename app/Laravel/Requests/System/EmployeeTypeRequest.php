<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class EmployeeTypeRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'description'		=> "required",
			
		];

		if($id != 0){
			$rules['password'] = "confirmed";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			
		];
	}
}