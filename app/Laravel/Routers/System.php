<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "System", 
	'as' => "system.", 
	// 'prefix'	=> "bsa-payroll",
	// 'middleware' => "", 

], function(){


		// ------------------------FINANCE ROUTES ------------------------------------//

		$this->group(['middleware'=>['web','finance.super_user'],'prefix' => "collection" ,'as'=>'collection.'],function(){

				$this->get('/',['as'=>"index",'uses'=> "MandatoryController@index" ]);

				$this->get('create',['as'=>"create",'uses'=> "MandatoryController@create" ]);
		});

	  // ------------------------ END OF FINANCE ROUTES-----------------------------//











		$this->group(['middleware' => ["web","system.guest"]], function(){

		$this->get('register/{_token?}',['as' => "register",'uses' => "AuthController@register"]);
		$this->post('register/{_token?}',['uses' => "AuthController@store"]);
		$this->get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);
	});

	$this->group(['middleware'=>['web'],'prefix' =>'guest','as' => 'guest.'], function(){

		$this->get('/',['as' => "index",'uses' => "GuestController@index"]);
		
		$this->get('leave',['as' => "leave",'uses' => "GuestController@create_leave"]);

		$this->post('store',['as' => "store",'uses' => "GuestController@store"]);


		});

	$this->group(['middleware'=>['web'],'prefix' =>'guest','as' => 'payslip.'], function(){

		$this->get('/payslip',['as' => "index",'uses' => "GuestController@index_payslip"]);

		$this->get('/payslip/show/{id?}/{dateFrom?}/{dateTo?}',['as' => "show",'uses' => "GuestController@show_payslip"]);
		
		});





	$this->group(['middleware' => ["web","system.auth","system.client_partner_not_allowed"]], function(){
		



		$this->get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		$this->group(['as' => "account."],function(){
			$this->get('p/{username?}',['as' => "profile",'uses' => "AccountController@profile"]);
			
			$this->group(['prefix' => "setting"],function(){
				$this->get('info',['as' => "edit-info",'uses' => "AccountController@edit_info"]);
				$this->post('info',['uses' => "AccountController@update_info"]);
				$this->get('password',['as' => "edit-password",'uses' => "AccountController@edit_password"]);
				$this->post('password',['uses' => "AccountController@update_password"]);
			});
		}); 

		$this->group(['middleware' => ["system.update_profile_first"]], function() {


				$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);

				$this->get('export',['as'=> "export",'uses'=> "DashboardController@export_excel"]);

// -------------------------------------ACCOUNT / EMPLOYEE ROUTES --------------------------------------//				
				$this->group(['middleware'=>['system.super_user'],'prefix' => "system-account", 'as' => "user."], function () {
				$this->get('/',['as' => "index", 'uses' => "UserController@index"]);
				
				$this->get('create',['as' => "create", 'uses' => "UserController@create"]);

				$this->post('create',['uses' => "UserController@store"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "UserController@edit"]);

				$this->post('update',['as'=>'update','uses' => "UserController@update"]);

				$this->post('delete',['as' => "destroy", 'uses' => "UserController@destroy"]);
			

				
			});
//-------------------------------------------------------------------------------------------------------// 



// -------------------------------------EMPLOYEE ROUTES --------------------------------------//	


$this->group(['middleware'=>['system.super_user'],'prefix' => "type", 'as' => "employee."], function () {

				$this->get('/',['as' => "index", 'uses' => "EmployeeTypeController@index"]);
				
				$this->get('create',['as' => "create", 'uses' => "EmployeeTypeController@create"]);

				$this->post('store',['as' => "store", 'uses' => "EmployeeTypeController@store"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "EmployeeTypeController@edit"]);

				$this->post('update',['as'=>'update','uses' => "EmployeeTypeController@update"]);

				$this->post('delete',['as' => "destroy", 'uses' => "EmployeeTypeController@destroy"]);

				

				
			});





//-------------------------------------------------------------------------------------------------------/



// ------------------------------------------OVERTIME ROUTES---------------------------------///

	$this->group(['middleware'=>['system.super_user'],'prefix' => "overtime", 'as' => "overtime."], function () {
				$this->get('/',['as' => "index", 'uses' => "OvertimeController@index"]);
				
				$this->get('create',['as' => "create", 'uses' => "OvertimeController@create"]);

				$this->post('create',['as' => "store", 'uses' => "OvertimeController@store"]);
				
				$this->get('result',['as' => "result", 'uses' => "OvertimeController@result"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "OvertimeController@edit"]);

				$this->post('edit/{id?}',['uses' => "OvertimeController@update"]);
				
			});




// ------------------------------------------OVERTIME ROUTES---------------------------------///




// -------------------------------------DEDUCTION ROUTES --------------------------------------//	


$this->group(['middleware'=>['system.super_user'],'prefix' => "employee", 'as' => "deduction."], function () {

				$this->get('/',['as' => "index", 'uses' => "DeductionController@index"]);
				
				$this->get('create',['as' => "create", 'uses' => "DeductionController@create"]);

				$this->post('store',['as' => "store", 'uses' => "DeductionController@store"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "DeductionController@edit"]);

				$this->post('edit/{id?}',['uses' => "UserController@update"]);

				$this->post('delete',['as' => "destroy", 'uses' => "DeductionController@destroy"]);

				$this->any('delete',['as' => "delete", 'uses' => "DeductionController@delete_deduction"]);

				$this->post('stores',['as' => "contribution", 'uses' => "DeductionController@store_contribution"]);

				$this->post('create',['as' => "absent", 'uses' => "DeductionController@absent"]);
			});





//-------------------------------------------------------------------------------------------------------/







//------------------------------------PAYROLL ROUTES------------------------------------------------------// 
				$this->group(['middleware'=>['system.super_user'],'prefix' => "payroll", 'as' => "payroll."], function () {

				$this->get('/',['as' => "index", 'uses' => "PayrollController@index"]);

				$this->get('compensation',['as' => "compensation", 'uses' => "PayrollController@index"]);

				$this->get('create/{id?}',['as' => "create", 'uses' => "PayrollController@create"]);

				$this->post('create',['uses' => "PayrollController@store"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PayrollController@edit"]);

				$this->post('update',['as' => 'update','uses' => "PayrollController@update"]);

				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PayrollController@destroy"]);

				$this->post('filter',['as' => 'filter','uses' => "PayrollController@filter_cutoff"]);
				
				$this->get('payslip/{id?}/{dateTo?}/{dateFrom?}',['as' => 'payslip','uses' => "PayrollController@payslip"]);

				$this->post('show',['as' => 'show','uses' => "PayrollController@show"]);


				$this->get('create-payroll/{id?}/{dateFrom?}/{dateTo?}',['as' => 'create','uses' => "PayrollController@create_payroll"]);
				
				$this->post('payroll-store',['as' => 'storePayroll','uses' => "PayrollController@store_payroll"]);

				$this->get('payroll-created/{id?}',['as' => 'payroll-created','uses' => "PayrollController@payroll_created"]);

				$this->get('payslip-created/{id?}/{dateFrom?}/{dateTo?}',['as' => 'payslip-created','uses' => "PayrollController@payslip_view"]);


				
			});
//-----------------------------------------------------------------------------------------------------//



// -------------------------------------------------Leave Routes -------------------------------------///

			$this->group(['prefix' => "leave", 'as' => "leave."], function () {

				$this->get('/',['as' => "index", 'uses' => "LeaveController@index"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "LeaveController@edit"]);

				$this->post('create',['as' => "store", 'uses' => "LeaveController@store"]);

				$this->post('update',['as' => "update", 'uses' => "LeaveController@update"]);

				$this->get('delete/{id?}',['as' => "delete", 'uses' => "LeaveController@destroy"]);
				
			});

			$this->group(['prefix' => "leave-approved", 'as' => "leave-approved."], function () {

				$this->get('/',['as' => "index", 'uses' => "ApprovedController@index"]);

				$this->get('show/{id?}',['as' => "show", 'uses' => "ApprovedController@show"]);
				
			});
			$this->group(['prefix' => "leave-disapproved", 'as' => "leave-disapproved."], function () {

				$this->get('/',['as' => "index", 'uses' => "DisapprovedController@index"]);

				$this->get('show/{id?}',['as' => "show", 'uses' => "DisapprovedController@show"]);
				
			});

			$this->group(['prefix' => "leave-request", 'as' => "request."], function () {

				$this->get('/',['as' => "index", 'uses' => "LeaveController@index_request"]);

				$this->get('show/{id?}',['as' => "show", 'uses' => "LeaveController@show_request"]);

				$this->post('create',['as' => "store", 'uses' => "LeaveController@store"]);

				$this->post('approval',['as' => "store-approval", 'uses' => "LeaveController@store_approval_request"]);
				
			});

			$this->group(['prefix' => "leave-credit", 'as' => "credit."], function () {

				$this->get('/',['as' => "index", 'uses' => "LeaveController@index_credit"]);

			
				
			});






// ------------------------------------------------- end Leave Routes -------------------------------------///


// -------------------------------------------------Department Routes -------------------------------------///

			$this->group(['prefix' => "department", 'as' => "department."], function () {

				$this->get('/',['as' => "index", 'uses' => "DepartmentController@index"]);

				$this->post('create',['as' => "create", 'uses' => "DepartmentController@create"]);

				$this->any('delete/{id?}',['as' => "delete", 'uses' => "DepartmentController@destroy"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "DepartmentController@edit"]);

				$this->post('update',['as' => "update", 'uses' => "DepartmentController@update"]);

				
			});
// ------------------------------------------------- end department Routes -------------------------------------///
			

// -------------------------------------------------Attendance Routes -------------------------------------///

			$this->group(['prefix' => "attendance", 'as' => "attendance."], function () {

				$this->get('/',['as' => "index", 'uses' => "AttendanceController@index"]);

				$this->any('show',['as' => "show", 'uses' => "AttendanceController@show"]);

				$this->get('show-all/{month?}/{year?}/{id?}',['as' => "show_all", 'uses' => "AttendanceController@show_all"]);

				$this->post('create',['as' => "store", 'uses' => "AttendanceController@store"]);
				

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AttendanceController@edit"]);

				$this->post('delete',['as' => "destroy", 'uses' => "AttendanceController@destroy"]);

				$this->post('search',['as' => "search", 'uses' => "AttendanceController@search"]);
			
				
			});
// ------------------------------------------------- end Attendance Routes -------------------------------------///

// -------------------------------------------------Attendance_category Routes -------------------------------------///

			$this->group(['prefix' => "attendance-category", 'as' => "attendance-category."], function () {

			$this->get('category/{category?}/{date?}',['as' => "category", 'uses' => "AttendanceCategoryController@category_a"]);

			$this->any('search/{department?}',['as' => "search", 'uses' => "AttendanceCategoryController@search"]);
			$this->get('search/{department?}',['as' => "search-get", 'uses' => "AttendanceCategoryController@search_get"]);

			$this->post('create',['as'=>'create','uses' => "AttendanceCategoryController@store"]);
				
			});
// ------------------------------------------------- end Attendance_category Routes -------------------------------------///

// -------------------------------------------------Contribution Routes -------------------------------------///

			$this->group(['prefix' => "contribution", 'as' => "contribution."], function () {
				 $this->get('/',['as' => "index", 'uses' => "ContributionController@index"]);
				$this->post('store',['as' => "store", 'uses' => "ContributionController@store"]);
				
				
			});
// ------------------------------------------------- end Contribution Routes -------------------------------------///


// ------------------------------------------------- refund Routes -------------------------------------///

			$this->group(['prefix' => "refund", 'as' => "refund."], function () {

				$this->get('/',['as' => "index", 'uses' => "ContributionController@index"]);

				$this->post('store',['as' => "store", 'uses' => "ContributionController@store"]);
				
				
			});

// ------------------------------------------------- end of refund Routes -------------------------------------///


// -----------------------------------------------Reports Routes -------------------------------------------///

			$this->group(['prefix' => "report", 'as' => "report."], function () {

				$this->any('/{dateFrom?}/{dateTo?}',['as' => "index", 'uses' => "ReportController@index"]);
				$this->get('export/{dateFrom?}/{dateTo?}',['as' => "export", 'uses' => "ReportController@export_excel"]);
				
			});
// ----------------------------------------------- End of Reports Routes -------------------------------------------///


		});
	});
});