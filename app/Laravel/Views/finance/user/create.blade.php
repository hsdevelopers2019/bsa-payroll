@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Create Employee Account Form<span class="panel-subtitle">Employee information.</span></div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}

            <div class="row">
              
                    <div class="col-md-4">
                        <div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}">
                        <label>Account Type</label>
                        {!!Form::select('type',$user_types,old('type'),['class' => "form-control select2"])!!}
                        @if($errors->first('type'))
                        <span class="help-block">{{$errors->first('type')}}</span>
                        @endif
                      </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}">
                        <label>Employee Type</label>

                        {!!Form::select('employee_type',$employee_types,old('employee_type'),['class' => "form-control select2"])!!}
                        @if($errors->first('employee_type'))
                        <span class="help-block">{{$errors->first('employee_type')}}</span>
                        @endif
                      </div>
                    </div>

                     <div class="col-md-4">
                        <div class="form-group {{$errors->first('department') ? 'has-error' : NULL}}">
                        <label>Department</label>
                          <select name="department" id="" class="form-control">
                            <option value="">--select--</option>
                            @foreach ($departments as $department)
                                  <option value="{{$department->department_description}}">{{$department->department_description}}</option>
                            @endforeach
               
                          </select>
                        @if($errors->first('department'))
                        <span class="help-block">{{$errors->first('department')}}</span>
                        @endif
                      </div>
                    </div>
                
                    <div class="col-md-12">
                      
                       <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
                          <label>Account Name</label>
                          <input type="text" placeholder="Complete name" class="form-control" name="name" value="{{old('name')}}" autocomplete="off">
                          @if($errors->first('name'))
                          <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                      
                       <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
                        <label>Email Address</label>
                        <input type="email" placeholder="email" class="form-control" name="email" value="{{old('email')}}" autocomplete="off">
                        @if($errors->first('email'))
                        <span class="help-block">{{$errors->first('email')}}</span>
                        @endif
                      </div>
                    </div>

      
                    <div class="col-md-4">
                       <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}" autocomplete="off">
                        <label>Username</label>
                        <input type="text" placeholder="Username" class="form-control" name="username" value="{{old('username')}}" autocomplete="off">
                        @if($errors->first('username'))
                        <span class="help-block">{{$errors->first('username')}}</span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-4">
                   <div class="form-group {{$errors->first('date_hired') ? 'has-error' : NULL}}" autocomplete="off">
                        <label>Date hired</label>
                        <input type="text" class="form-control datepicker" name="date_hired" value="{{old('date_hired')}}" autocomplete="off">
                        @if($errors->first('date_hired'))
                        <span class="help-block">{{$errors->first('date_hired')}}</span>
                        @endif
                      </div>
                </div>
                  
            </div>
         
            <div class="row">
              <div class="col-md-8">
                 <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}" autocomplete="off">
                  <label>Description</label>
                  <textarea name="description" id="" cols="30" rows="3" class="form-control" placeholder="Tell something about him/her.">{{old('description')}}</textarea>
                  @if($errors->first('description'))
                  <span class="help-block">{{$errors->first('description')}}</span>
                  @endif
                </div>
                

                <div class="col-md-4">
                  <div class="form-group {{$errors->first('password') ? 'has-error' : NULL}}" autocomplete="off">
                    <label>Password</label>
                    <input type="password" placeholder="Password" class="form-control" name="password" >
                    @if($errors->first('password'))
                    <span class="help-block">{{$errors->first('password')}}</span>
                    @endif
                  </div>
                </div>

                <div class="col-md-4">
                   <div class="form-group">
                    <label>Verify Password</label>
                          <input type="password" placeholder="Verify Password" class="form-control" name="password_confirmation" >
                   </div>
                </div>
                
              </div>
            </div>
           
            <div class="row xs-pt-15">
              <div class="col-xs-12 ">
                  <button type="submit" class="btn btn-space btn-success btn-lg pull-right">Create Account</button>
                 
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
   $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});

        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });
  });

</script>
@stop

