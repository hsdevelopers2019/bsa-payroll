<!DOCTYPE html>
 <html lang="zxx">

    
<!-- Mirrored from www.innovationplans.com/idesign/bemax/particles.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Aug 2019 08:47:35 GMT -->
<head>

    	<!-- Metas -->
        <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="keywords" content="HTML5 Template Bemax onepage themeforest" />
		<meta name="description" content="Bemax - Onepage Multi-Purpose HTML5 Template" />
		<meta name="author" content="" />

		<!-- Title  -->
		<title></title>

		<!-- Favicon -->
		{{-- <link rel="shortcut icon" href="img/favicon.ico" /> --}}

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700,800" rel="stylesheet">
		
		<!-- Plugins -->
		<link rel="stylesheet" href="{{ asset('frontend/assets/css/plugins.css') }}" />

        <!-- Core Style Css -->
        <link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css') }}" />

    
    </head>

    <body>

    	<!-- =====================================
    	==== Start Loading -->

    	<div class="loading">
    		<div class="text-center middle">
    			<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
    		</div>
    	</div>
        

    	<header class="header valign bg-img" style="background-color: white" data-scroll-index="0" {{-- data-overlay-dark="5"  --}}data-background="{{ asset('frontend/assets/img/bg2.jpg') }}" data-stellar-background-ratio="0.5">

            <!-- particles -->
            {{-- <div id="particles-js" style="z-index: 0;"></div> --}}

    		<div class="container text-center">
                <img src="{{ asset('frontend/assets/logo.png') }}" style="width: 14%;">
    			<div class="row">
    				<div class="full-width text-center caption mt-30">
    					<h3>BARANGAY</h3>
                        <h1 class="cd-headline clip">
                            <span class="blc">SAN ANTONIO <span style="color:#165400 ">PASIG</span></span>

                        </h1>
                        <p style="color: #000">Human Resource Information System</p>
                        <br>
                        <a href="{{ route('finance.login') }}" class="btn btn-lg" style="background-color: #d6cc00; border-color: #d6cc00    ; cursor: pointer; width:250px">
                            <span>Finance</span>
                        </a>
                        <a href="{{ route('system.dashboard') }}" class="btn btn-lg"  style="background-color: #165400; border-color: #165400; cursor: pointer;width:250px">

                            <span style="z-index: 10000;">Payroll</span>
                        </a>
    				</div>
    			</div>
    		</div>

    	
    	<!-- End Footer ====
    	======================================= -->




       
        <!-- jQuery -->
		<script src="{{ asset('frontend/assets/js/jquery-3.0.0.min.js')}}"></script>
		<script src="{{ asset('frontend/assets/js/jquery-migrate-3.0.0.min.js')}}"></script>

		<!-- popper.min -->
		<script src="{{ asset('frontend/assets/js/popper.min.js')}}"></script>

	  	<!-- bootstrap -->
		<script src="{{ asset('frontend/assets/js/bootstrap.min.js')}}"></script>

		<!-- scrollIt -->
		<script src="{{ asset('frontend/assets/js/scrollIt.min.js')}}"></script>

		<!-- animated.headline -->
		<script src="{{ asset('frontend/assets/js/animated.headline.js')}}"></script>

        <!-- particles.min js -->
        <script src="{{ asset('frontend/assets/js/particles.min.js')}}"></script>

        <!-- app js -->
        <script src="{{ asset('frontend/assets/js/app.js')}}"></script>



      	<!-- custom scripts -->
        <script src="{{ asset('frontend/assets/js/scripts.js') }}"></script>


    </body>

<!-- Mirrored from www.innovationplans.com/idesign/bemax/particles.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Aug 2019 08:48:15 GMT -->
</html>
