<!doctype html>
<html lang="en">
<head>

  <title>Pay Slip</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
*{
  font-size: 12px
}
    ul
    {
      list-style: none;
    }
    p
    {
      margin-left: 20%;
    }

  </style>


</head>
<body>

  <div class="container">
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4"  > 

{{-- <center><img src="{{ asset('assets/logo.png')}}" style="height: 150px;width: 150px" alt=""></center> --}}
     </div> 
     <hr>


<div style="margin-left: 30%">
     <table class="table table-bordered" style="width: 60%;">
      <thead>
        <tr>
           <th colspan="4">Name :  {{ strtoupper($users->name) }}</th>
        </tr>
        <tr>
           <th colspan="4">Department :  {{ strtoupper($users->department ) }}</th>
        </tr>
        <tr>
          <th colspan="4" >Date: {{ date("M j, Y", strtotime($dateFrom ))}} - {{ date("M j, Y", strtotime($date ))}}</th>
        </tr>

        <tr>
          <th colspan="2">Basic Pay</th>
          <th colspan="2">Gross Pay</th>
        </tr>
        <tr>
          <td colspan="2">  {{ number_format($users->basic_pay,2)}} Per Month</td>
          <td colspan="2">  {{ number_format($users->basic_pay / 2,2)}} Per Cut-off </td>  
        </tr>
      </thead>
      <thead>
        <tr>
          <th colspan="4" style="padding: 15px;">
            Less Deduction
          </th>
        </tr>
        <tr>

          <th colspan="2">Description</th>
          <th colspan="2">Amount</th>

        </tr>
      </thead>
      
        <tr>
        <td colspan="2">Absent</td>
        <td colspan="2">{{ number_format($absent->absent * ($users->basic_pay / 30) ,2) }}</td>
      </tr>
      @foreach ($payslip as $pay)
      
      <tr>
        <td  colspan="2" >{{ strtoupper($pay->description)}}</td>
        <td  colspan="2" >{{ number_format($pay->amount) }}</td> 
      </tr>
      
      @endforeach
      <tr>
        <td colspan="2" ><strong>Gross Pay</strong></td>
        <td colspan="2">{{ number_format($users->basic_pay /2) }}</td>
      </tr>
      <tr>
        <td colspan="2" ><strong>Total Deduction</strong></td>
        <td colspan="2" >{{ number_format($total_net->total_deduction) }}</td>
      </tr>
      <tr>
        <td colspan="2" > <strong>Overtime</strong></td>
        <td colspan="2" >{{ $total_net->overtime }}</td>
      </tr>
      <tr>
        <td colspan="2" ><strong>Net Pay</strong></td>
        <td colspan="2" >{{ number_format( $total_net->net_pay - $total_net->total_deduction ) }}</td>
      </tr>

    </table>
</div>

  </div>
</div>

</body>
</html>