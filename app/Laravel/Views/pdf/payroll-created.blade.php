<!doctype html>
<html lang="en">
<head>

  <title>Pay Slip</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
*{
  font-size: 12px
}
    ul
    {
      list-style: none;
    }
    p
    {
      margin-left: 20%;
    }

  </style>


</head>
<body>

  <div class="container">
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4"  > 

{{-- <center><img src="{{ asset('assets/logo.png')}}" style="height: 150px;width: 150px" alt=""></center> --}}
     </div> 
     <hr>


<div style="margin-left: 30%">
     <table class="table table-bordered" style="width: 60%;">
      <thead>
        <tr>
           <th colspan="4">Name : {{ $user->name }} </th>
        </tr>
        <tr>
           <th colspan="4">Department : {{ $user->department }} </th>
        </tr>
        <tr>
          <th colspan="4" >Date: {{ date('M d Y', strtotime($details->date_from)) }} - {{ date('M d Y', strtotime($details->date_to)) }} </th>
        </tr>

        <tr>
          <th colspan="2">Basic Pay</th>
          <th colspan="2">Gross Pay</th>
        </tr>
        <tr>
          <td colspan="2">{{ number_format($details->basic_pay,2) }}/Per Month</td>
          {{-- @if ($user->payroll_category == 'b')
          <td colspan="2">-</td> --}}
          {{-- @else --}}
            <td colspan="2"> {{ $user->payroll_category == 'b'? $user->basic_pay : $details->gross_pay  }}/<i>Cut-off</i> </td> 
          {{-- @endif --}}
           
        </tr>
      </thead>
      <thead>
        <tr>
          <th colspan="4" style="padding: 15px;">
            Less Deduction
          </th>
        </tr>
        <tr>

          <th colspan="2">Description</th>
          <th colspan="2">Amount</th>

        </tr>
      </thead>
      
        <tr>

        <td colspan="2">Absent</td>
        <td colspan="2">{{ number_format($details->absent,2) }}</td>
      </tr>
      @forelse ($deductions as $deduction)
       <tr>
         <td colspan="2">{{ $deduction->description }}</td>
         <td colspan="2">{{ number_format($deduction->amount,2) }}</td>
       </tr>
      @empty
      @endforelse
      <tr>
        <td colspan="2" ><strong>Gross Pay</strong></td>
        {{-- @if ($user->payroll_category == 'b') --}}
        {{-- <td colspan="2">{{ number_format($details->basic_pay) }}</td> --}}
        {{-- @else --}}
        <td colspan="2">{{ $user->payroll_category == "b" ? Helper::amount($details->basic_pay) : Helper::amount($details->gross_pay) }}</td>
        {{-- @endif --}}
        
      </tr>
      <tr>
        <td colspan="2" ><strong>Total Deduction</strong></td>
        <td colspan="2" >{{ Helper::amount($details->total_deduction) }}</td>
      </tr>
      <tr>
        <td colspan="2" > <strong>Overtime</strong></td>
        <td colspan="2" >{{ number_format($details->overtime,2) }}</td>
      </tr>
      <tr>
        <td colspan="2" ><strong>Net Pay</strong></td>
        <td colspan="2" >{{ $user->payroll_category == "b" ? Helper::amount($details->basic_pay) : Helper::amount($details->gross_pay - $details->total_deduction) }}</td>
      </tr>
      

    </table>
</div>

  </div>
</div>

</body>
</html>