<!doctype html>
<html lang="en">
<head>

  <title>Pay Slip</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
*{
  font-size: 12px
}
    ul
    {
      list-style: none;
    }
    p
    {
      margin-left: 20%;
    }
    span
    {
      font-size: 11px
    }
  </style>


</head>
<body>




     <table class="table table-bordered text text-center" width="100%">
    
      <thead >
        <tr>
        <th>No.</th>
        <th style="text-align: center;width: 15%">Name</th>
        <th style="text-align: center;">Position</th>
        <th style="text-align: center;">Honoraria</th>
        <th style="text-align: center;">Allowance</th>
        <th style="text-align: center;">Amount Earned</th>
        <th style="text-align: center;">Deductions</th>
        <th style="text-align: center;">No</th>
        <th style="text-align: center;">Net Amount Recieve</th>
        <th colspan="4" style="text-align: center;">Signature of Payee</th>
        </tr>
      </thead>
      <tbody>
        <?php

          $total_honoraria = 0;
          $total_earned = 0;
          $no = 0;
          $nos = 0;
         ?>

        @foreach ($reports as $report)
        <tr>
          <td>{{ $no += 1 }}</td>
          <td>{{ strtoupper($report->name) }}</td>
          <td>{{ strtoupper($report->department) }}</td>
          <td>{{ Helper::amount($report->basic_pay) }}</td>
          <td></td>
          <td>{{ $report->payroll_category=='b' ? Helper::netpay($report->basic_pay,$report->total_deduction ):Helper::netpay($report->net_pay,$report->total_deduction)  }}</td>
          <td>{!! $report->combinedDeduction !!}</td>
          <td>{{ $nos += 1 }}</td>
            <td>{{ $report->payroll_category == "b" ? Helper::netpay($report->basic_pay,$report->total_deduction) : Helper::netpay($report->net_pay,$report->total_deduction) }}</td>
            <td colspan="4"></td>
        </tr>
        <?php $total_honoraria += $report->basic_pay?>
        <?php 
        if($report->payroll_category == "b" )
        {
           $total_earned +=  $report->basic_pay; 
        }
        else
        {
          $total_earned +=  $report->net_pay - $report->total_deduction; 
        }
         
         ?>
        @endforeach
        <tr>
          <td colspan="3" style="text-align: right">Total</td>
          <td>{{ Helper::amount($total_honoraria) }}</td>
         
          <td></td>
          <td>{{ Helper::amount($total_earned) }}</td>

          <td></td>
          <td></td>
        
          <td>{{ Helper::amount($total_earned) }}</td>
          <td colspan="4"></td>
        </tr>
        <tr>
          <td colspan="3" style="text-align: left">
              <span>A. Certified</span><br><br>
              <span>
              Existence of Available Appropriations of the charges / Expenses indicated above
              </span><br><br><br>

              <span>Signature:____________________</span><br>
              <span >Printed Name: RACHEL MARIE RUSTIA-OLARTE</span><br>
              <span>Postion:Chairman on Approriations</span><br>
              <span>Date: {{ Carbon::now()->format('M d Y') }}</span>
          </td>
          <td colspan="3" style="text-align: left">
              <span>B. Certified</span><br><br>
              <span>
                As to validity,propriety, and legality of claims
              </span><br><br><br><br>

              <span>Signature:____________________</span><br>
              <span>Printed Name: THOMAS RAYMOND U. LISING</span><br>
              <span>Postion:Barangay Chairman</span><br>
              <span>Date: {{ Carbon::now()->format('M d Y') }}</span>
          </td>
          <td colspan="2" style="text-align: left">
            
              <span>C. Certified</span><br><br>
              <span>
              Existence of Available Appropriations of the charges / Expenses indicated above
              </span><br><br>

              <span>Signature:____________________</span><br>
              <span>Printed Name: RACHEL MARIE RUSTIA-OLARTE</span><br>
              <span>Postion:Chairman on Approriations</span><br>
              <span>Date:{{ Carbon::now()->format('M d Y') }}</span>
          </td>
          <td colspan="5" style="text-align: left;">
             <span>D. Paid By:</span><br><br>
              <span>
              {{--   --}}
              </span><br><br><br><br>

              <span>Signature:____________________</span><br>
              <span>Printed Name: ROSALIN S. SUPRA</span><br>
              <span>Postion:Brangay Treasurer</span><br>
              <span>Date: {{ Carbon::now()->format('M d Y') }}</span>

          </td>
          
        </tr>
        
      </tbody>
    </table>


  </div>
</div>

</body>
</html>