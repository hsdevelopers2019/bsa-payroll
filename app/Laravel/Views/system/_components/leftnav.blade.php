<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle"></a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <center>
                            <img src="{{ asset('assets/logo.png')}}" style="margin: 0px auto;" alt="logo" width="150" class="logo-img">
                        </center>
                        <li class="divider">Quick Menu</li>


                        {{-- accounting --}}

                        @if ( strtoupper(auth()->user()->department) == "ACCOUNTING"  ||  strtoupper(auth()->user()->type == "super_user") )
                        
                        

                        <li><a href="{{route('system.dashboard')}}"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a> </li>
                        @if(in_array($auth->type, ["super_user","admin","editor","technical","chief_editor"]))
                        {{-- <li class="divider">Employee Management</li> --}}
                        <li class="parent"><a href="#"><i class="icon mdi mdi-accounts-list"></i><span>Employee Management</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.user.create')}}">Add Employee  </a></li>
                                
                                {{-- <li><a href="{{route('system.user.index')}}">Payroll Type</a> </li> --}}
                                <li><a href="{{route('system.user.index')}}">Employee List</a></li>
                                
                            </ul>
                        </li>

                        @if(in_array($auth->type, ["super_user","admin"]))

                        


                        
                        {{-- <li class=""><a href="{{route('system.department.index')}}"><i class="icon mdi mdi-group-work"></i><span>Refund</span></a></li> --}}
                        
                        
                        @endif
                        
                        <li class="divider">Attendance Management</li>
                        <li class="parent">
                            <a href="{{route('system.attendance.index')}}"><i class="icon mdi mdi-settings-square"></i><span>Manage Attendance</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ route('system.attendance-category.category',['a']) }}">Category A</a> </li>
                                <li><a href="{{ route('system.attendance-category.category',['b']) }}">Category B</a></li>
                                {{-- <li><a href="{{route('system.overtime.create')}}">Over Time</a> </li> --}}
                                
                            </ul>
                        </li>
                        {{-- <li class="divider">Leave Management</li> --}}
                       {{--  <li class="parent"><a href="#"><i class="icon mdi  mdi-account-circle"></i><span>Leave Management</span></a></li> --}}
                            {{-- <ul class="sub-menu"> --}}
                                
                                
                               {{--  <li><a href="{{route('system.request.index')}}"><i class="icon mdi mdi-group-work"></i><span>For Approval Request</span></a></li>
                                 --}}
                                
                            {{-- </ul> --}}
                        {{-- </li> --}}
                        
                        
                        
                        <li class="divider">Paryoll Management</li>
                        
                       {{--  <li class="parent"><a href="#"><i class="icon mdi mdi-accounts-list"></i><span>Manage Payroll</span></a>
                            <ul class="sub-menu"> --}}
                             
                                <li><a href="{{route('system.attendance.show_all',[Carbon::now()->startOfMonth()->format('m'),Carbon::now()->endOfMonth()->format('Y')])}}"><i class="icon mdi mdi-group-work"></i><span>Create Payroll</span></a> </li>
                                {{-- <li><a href="{{route('system.payroll.compensation')}}">Filter Payroll</a> </li>
                                
                                <li><a href="{{route('system.deduction.index')}}">Other Deduction</a> </li> --}}
                                
                            {{-- </ul> --}}
                        {{-- </li> --}}

                        <li class="divider">Master File</li>
                        <li class=""><a href="{{route('system.department.index')}}"><i class="icon mdi mdi-group-work"></i><span>Deparment List</span></a>
                        </li>
                        <li><a href="{{route('system.employee.index')}}"><i class="icon mdi mdi-group-work"></i><span>Employee Type</span></a> </li>
                        {{-- <li><a href="{{route('system.leave.index')}}"><i class="icon mdi mdi-group-work"></i><span>Create Leave</span></a> </li> --}}
                        
                        

                        


                        @else

                        {{-- admintype  == USER --}}
                        <li><a href="{{ route('system.payslip.index') }}"><i class="icon mdi mdi-home"></i><span>Payslip</span></a> </li>
                        <li><a href="{{route('system.guest.leave')}}"><i class="icon mdi mdi-home"></i><span>Leave Form</span></a> </li>
                        <li><a href="{{route('system.guest.index')}}"><i class="icon mdi mdi-home"></i><span>Leave Request Status</span></a> </li>
                        {{--end admintype  == USER --}}
                        @endif

                        
                        

                        
                        


                        {{-- end of accoutting  --}}




                        @else

                        <li><a href="{{ route('system.payslip.index') }}"><i class="icon mdi mdi-home"></i><span>Payslip</span></a> </li>
                        <li><a href="{{route('system.guest.leave')}}"><i class="icon mdi mdi-home"></i><span>Leave Form</span></a> </li>
                        <li><a href="{{route('system.guest.index')}}"><i class="icon mdi mdi-home"></i><span>Leave Request Status</span></a> </li>

                        {{-- Finance --}}
{{-- 
                    <li><a href="{{route('system.dashboard')}}"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a> </li>

                    <li class="parent"><a href="#"><i class="icon mdi mdi-accounts-list"></i><span>Mandatories Collection</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ route('system.collection.index') }}">Collections</a></li>
                                <li><a href="#">Add Collections</a> </li>
                               
                                <li><a href="{{ route('system.collection.create') }}">Create Collections</a></li>
                                
                            </ul>
                        </li> --}}


                        {{--end of finance  --}}
                        @endif   
                        
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>





