<!DOCTYPE html>
<html lang="en">
  
<head>
    @include('system._components.metas')
    @include('system._components.styles')
  </head>
  <body>
    <div class="be-wrapper be-color-header be-color-header-success">
      @include('system._components.topnav')
      @include('system._components.leftnav')
      <div class="be-content">
        @yield('content')
      </div>
      {{-- @include('system._components.rightnav') --}}
    </div>

    @yield('page-modals')
    @include('system._components.scripts')
  </body>
<div id="create_payroll" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
     
 
      <div class="modal-body">
        
      
        
        <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider"><button type="button" class="close" data-dismiss="modal">&times;</button>GENERATE PAYROLL</div>
        
        <div class="panel-body">
          {{-- <form method="POST" action="{{ route('system.deduction.store') }}" > --}}
            {!!csrf_field()!!}
             
            <div class="form-group">
              <label for="amount">MONTH</label>
              <select name="date" id="date" class="form-control">
                  <option value="jan">January</option>
                  <option value="feb">Febuary</option>
              </select>
            </div>
            <div class="form-group">
              <label for="amount">Category</label>
              <select name="date" id="date" class="form-control">
                  <option value="a">CATEGORY A</option>
                  <option value="b">CATEGORY B</option>
              </select>
            </div>
          
            <div class="form-group" hidden="">
              <input type="text" name="employee_id" id="employee_id" >
              <input type="text" name="date_from" id="date_from" >
              <input type="text" name="date_to" id="date_to" >
            </div>
          

        
        </div>
      </div>

    
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-url="" id="proceed">Proceed</a>
        {{-- <button class="btn btn-success">Proceed</button> --}}
      </div>
      
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){

    $(document).on('change','#date',function(){
      
      $('#proceed').attr({"href" : $(this).val()/a});
    })
  })
</script>

</html>