@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
   
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-success">
        <div class="panel-heading">Category   - Record Data
          <div class="tools dropdown">
            <a href="#"  type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
              <li><a href="#search" data-toggle="modal">Filter Department/Position</a></li>
              {{-- <li><a href="#">Import Calendar</a></li> --}}
              {{-- <li class="divider"></li> --}}
              {{-- <li><a href="#">Export calendar</a></li> --}}
            </ul>
          </div>
        </div>
        <div class="panel-body">

          <div class="col-md-12">
          <table class="table table-hover table-wrapper">
            <thead>
              <th>Fullname</th>
              {{-- <th>Pay-run</th> --}}

              
              <th>1st Cut-off</th>
          
              <th>2nd Cut-off</th>
              
              <th class="actions"></th>
            </thead>
            <tbody>
            @forelse($employees as $user)
            <tr id="{{ $user->id }}">
              <td class="cell-detail">
              <span>{{ $user->name }}</span>  
              <span class="cell-detail-description">{{ $user->department }}</span>  
              </td>
        
             {{--  <td class="cell-detail">
              <span class="cell-detail-description">{{$user->date_from ==NULL ? 'a':'b' }}</span>  
              <span class="cell-detail-description">{{date('M d, y',strtotime($user->date_to)) }}</span>  
              </td> --}}
                
              <td>{!!  strtolower($user->attendance_status) =='a'  || strtolower($user->attendance_status =='aa') || strtolower($user->attendance_status == 'created' ) ? '<span class="label label-success" id="first">Created</span>'  : '<span class="label label-danger">Not Created</span>' !!} </td>

              <td>{!!  strtolower($user->date_from) == Carbon::now()->firstOfMonth()->addDays('15')->format('Y-m-d')  || strtolower($user->attendance_status) =='aa'   ? '<span class="label label-success">Created</span>'  : '<span class="label label-danger">Not Created</span>' !!} </td>
                  
              
              @if($user->attendance_status == 'aa')
              <td>--</td>
              @else
              <td>
                <a href="#modal_attendance" data-from data-role="manage" data-id="{{ $user->id }}" data-dep="{{ $user->department }}" data-toggle="modal">manage</a>
              </td>
              @endif
              
          
            </tr>
            @empty
            @endforelse
            
            </tbody>
          </table>
          
          </div>
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {{-- {!! $files->render()!!} --}}
          </div>
        </div>
      </div>

    </div>


  </div>
</div>
@stop

@section('page-modals')
<div id="modal_attendance" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Manage Attendance</h1>
      </div>
<form action="{{ route('system.attendance-category.create') }}" method="POST"> 
  {{ csrf_field() }}
      <div class="modal-body">
        
          <div class="form-group">
           
            @if (Request::segment(3) == 'b')
            
            <input type="text" class="form-control" name="date_from" value="{{ Carbon::now()->format('M') }}" style="display: none">
            @else  
            <label for="payrun">Pay run: </label>
            <select name="payrun" id="payrun" class="form-control">
              <option value="first">FIRST CUTOFF</option>
              
              <option value="second">SECOND CUTOFF</option>

            </select>
            @endif


          </div>
        
          <div class="form-group" >

            <label for="absent">Number of Absent</label>
            <input type="number" placeholder="Enter number of Absent"  class="form-control" name="absent" id="" value="0">
          </div>  
          <div class="form-group" >
            <label for="absent">Over Time</label>
            <input type="number" placeholder="Enter number of hours"  class="form-control" name="overtime" id="" value="0">
          </div>  
          <div class="form-group" style="display: none">
            <input type="text" name="date_from" id="date_from" value="{{ Carbon::now()->startOFMonth() }}" >
            <input type="text" name="date_to" id="date_to" value="{{ Carbon::now()->firstOfMonth()->addDays(14)->format('Y-m-d') }}">
            <input type="text" name="employee_id" id="employee_id" value="">
            <input type="text" name="status" id="status" value="a">
            <input type="text" name="attendance_status" id="attendance_status" value="a">
            <input type="text" name="department" id="department" value="">
          </div>
          
        


        <hr>

    
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        {{-- <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Create</a> --}}
        <button type="submit" class="btn btn-success">create</button>
      </div>
       </form>
    </div>
  </div>
</div>

{{-- //////////////////////////////////////////////////// --}}

<div id="search" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Manage Attendance</h1>
      </div>
<form action="{{ route('system.attendance-category.search') }}" method="post"> 
  {{ csrf_field() }}
      <div class="modal-body">
        
      
        <hr>

        <div class="form-group">
          <label for="department">Filter by Department/Position</label>
          <select name="department" id="department" class="form-control select2" required="">
            <option value="">Select Department/Position</option>
            @foreach ($departments as $department)
              <option value="{{ $department->department_description }}">{{ $department->department_description }}</option>
            @endforeach
            
          </select>
        </div>

    
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>

        <button type="submit" class="btn btn-success">Filter</button>
      </div>
       </form>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){





    $('#payrun').on('change',function(){


      if($(this).val() == 'first')
      {
        $('#date_from').val('{{ Carbon::now()->firstOfMonth()->format('Y-m-d')  }}')
        $('#date_to').val('{{ Carbon::now()->firstOfMonth()->addDays(14)->format('Y-m-d')  }}')
        $('#attendance_status').val('a');
       
        
      }
      else
      {
        $('#date_from').val('{{ Carbon::now()->firstOfMonth()->addDays(15)->format('Y-m-d')  }}')
        $('#date_to').val('{{ Carbon::now()->endOfMonth()->format("Y-m-d")}}')
        $('#attendance_status').val('aa');
      }
    });

    $(document).on('click','a[data-role=manage]',function(){
      $('#employee_id').val($(this).data('id'));
      $('#department').val($(this).data('dep'));
    });

    $('.datepicker').datetimepicker({autoclose: true})
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $(".focus").on("click",function(){
      $(this).select();
    })

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

  });
</script>
@stop

