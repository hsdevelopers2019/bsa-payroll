@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Attendance<span class="panel-subtitle">Attendance information.</span></div>
        <div class="panel-body">

        <div class="row">
          <div class="col-md-12">
           

          <div class="row">
            

            <div class="col-md-12">
             <div class="form-group">
              <a href="{{ route('system.attendance.show_all') }}" class="pull-right">Attendance List</a>
              <h3>Attendance Range</h3>
            </div> 
            </div>
            
            
          <hr>

          <form action="{{ route('system.attendance.store') }}" method="POST">

            {!! csrf_field() !!}
           
            
          {{--   
               <div class="col-md-12">
                <div class="form-group {{$errors->first('employee_id') ? 'has-error' : NULL}}">

                  <label for="date">Select Employee</label>
                      <select name="employee_id" id="employee_id" class="form-control select2" >
                        <option value="{{ old('employee_id') }}" >{{ old('employee_id')  ?? "---Select ---"}}</option>
                        @foreach ($users as $user)
                          <option value="{{ $user->id}}"> {{ $user->name }}</option>
                        @endforeach
                      </select>
                   @if($errors->first('date_from'))
                        <span class="help-block">{{$errors->first('employee_id')}}</span>
                    @endif
                </div>
              </div> --}}

              <div class="col-md-4">
                <div class="form-group {{$errors->first('employee_id') ? 'has-error' : NULL}}">

                  <label for="date">Payroll Category</label>
                      <select name="employee_id" id="employee_id" class="form-control select2" >
                        <option value="">--Select Payroll Category</option>
                          <option value="">Category A</option>
                          <option value="">Category B</option>
                      </select>
                   @if($errors->first('date_from'))
                        <span class="help-block">{{$errors->first('employee_id')}}</span>
                    @endif
                </div>
              </div>

             {{--  <div class="col-md-3">
                <div class="form-group {{$errors->first('date_from') ? 'has-error' : NULL}}">
                  <label for="date">From</label>
                  <input type="text" name="date_from" id="date_from" class="form-control datepicker" autocomplete="off" placeholder="Select Date" value="{{old('date_from')}}">
                   @if($errors->first('date_from'))
                        <span class="help-block">{{$errors->first('date_from')}}</span>
                    @endif
                </div>
              </div> --}}
               {{-- <div class="col-md-3">
               <div class="form-group {{$errors->first('date_to') ? 'has-error' : NULL}}">
                  <label for="date">To</label>
                  <input type="text" name="date_to" id="date_to" class="form-control datepicker" autocomplete="off" placeholder="Select Date" value="{{old('date_to')}}">
                   @if($errors->first('date_to'))
                        <span class="help-block">{{$errors->first('date_to')}}</span>
                    @endif
                </div>
              </div> --}}
                {{-- <div class="col-md-3">
                  <div class="form-group {{$errors->first('absent') ? 'has-error' : NULL}}">
                    <label for="date">Number of Absent</label>
                      <input type="number" min="0" name="absent" id="absent" class="form-control" autocomplete="off" placeholder="Enter # of Absent" value="{{old('absent',0)}}">
                        @if($errors->first('absent'))
                          <span class="help-block">{{$errors->first('absent')}}</span>
                      @endif
                  </div>
                </div> --}}
             {{--  <div class="col-md-2">
                <div class="form-group">
                  <label for="">.</label>
                      <input type="submit" value="Go" class="btn btn-primary form-control" >
                </div>
              </div> --}}
            </div>

          </form> 


           
      
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  

 


 
</script>

@stop

@section('page-modals')
<div id="create" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Create Leave</h1>
      </div>

      <div class="modal-body">
        
        

        <hr>

        <div class="row">
          <div class="col-md-9">
            <div class="form-group">
              <input type="text" name="leave_category" id="leave_category" class="form-control" placeholder="Enter Category">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="submit" value="Create" class="btn btn-primary form-control ">
            </div>
          </div>
        </div>
      </div>

     
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});
        
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });



    $(document).on('click','a[data-role=delete]',function(){

              $('#id').val($(this).data('id'))
    })
  });

</script>
@stop


