@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading">Attendance           <div class="tools dropdown">
          <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
          <ul role="menu" class="dropdown-menu pull-right">
            <li><a href="{{  route('system.attendance.index') }} ">Create Attendance</a></li>
          </ul>
        </div>
          <span class="panel-subtitle">Attendance information.</span>

        </div>
        <div class="panel-body">
          
          <form action="POST" method="">
         {{--   <div class="col-md-10">
           
            <select name="employee_id" id="employee_id" class="form-control select2">
              <option value="">Search Employee</option>
              @foreach ($employees as $employee)
                <option value="{{ $employee->id }}">{{ $employee->name }}</option>
              @endforeach
            </select>
            <input type="text" class="form-control" name="month" value="{{ Request::segment(3) }}" style="display: none" >
            <input type="text" class="form-control" name="year" value="{{ Request::segment(4) }}" style="display: none" >
            <input type="text" class="form-control" name="year" value="{{ Request::segment(4) }}" style="display: none" >
          </div> --}}
          {{-- <div class="col-md-2">
              <input type="submit" value="search" class="btn  btn-lg btn-success">
          </div> --}}
         </form>
          
          <div class="col-md-12">
            <center><h1>Category {{ strtoupper($employee->payroll_category) }}</h1></center>
          <div class="row">
          
  
              <div class="col-md-6" >
                <div class="form-group">
                  <h3>First Cut-off List</h3>
                  <div id="result"></div>
                  <table   class="table table-bordered table-wrapper text-center" id="emp_table">
                    <thead>
                      <th style="text-align: center;">Name</th>
                      
                      {{-- <th style="text-align: center;width: 20%">Other Deduction(s)</th> --}}
                      <th style="text-align: center;">Payroll Status</th>
                      <th style="text-align: center;">Action</th>
                    </thead>
                    <tbody>


                    <tr>
                      <td>{{ $employee->name }}</td>
                     {{--  <td>{!! $employee->status ? '<span class="label label-success">'.$employee->status .'</span>' :'<span class="label label-danger">Pending</span>' !!}</td> --}}
                        @if ($employee->status == 'a' || $employee->status == NUll)
                          <td><span class="label label-warning">PENDING</span></td>
                          @else
                            <td><span class="label label-success ">{{ strtoupper($employee->status) }}</span></td>
                        @endif
                      <td>
                        @if ($employee->status)
                        <div class="btn-group btn-hspace">
                          <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                          <ul role="menu" class="dropdown-menu">

                            @if ($employee->status == 'created')
                            <li><a  href="{{ route('system.payroll.payslip-created',[$employee->employee_id,$employee->date_from,$employee->date_to]) }}">View</a></li>
                            @else
                            <li><a  href="{{ route('system.payroll.create',[$employee->employee_id,$employee->date_from,$employee->date_to]) }}">Edit | Create Payroll</a></li>


                            <li><a  data-role="delete" data-id="{{ $employee->id }}"  data-from="{{ $employee->date_from }}" data-to="{{ $employee->date_to }}" href="#confirm-delete" data-toggle="modal" title="Remove Record">Remove Record</a></li>
                            @endif


                          </ul>
                        </div>
                          @else

                          <span class="label label-danger">attendace not yet created</span>

                        @endif
                       
                      </td>
                    </tr>
                             
                  
                             

                        
            
                    </tbody>
                    
                  </table>  
                </div>

                

                <div class="form-group">
                  {{-- {{ $attendance->links() }} --}}
                </div>
              </div>
             
            
           

            </div>
          </div>
   
        </div>
      </div>
    </div>
  </div>
</div>


  
@stop

@section('page-modals')

<div id="confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Create Leave</h1>
      </div>

      <div class="modal-body">
        

        <hr>

        <div class="row">
          <div class="col-md-9">
            <div class="form-group">

              <input type="text" name="leave_category" id="leave_category" class="form-control" placeholder="Enter Category">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="submit" value="Create" id="filtesr" class="btn btn-primary form-control ">
            </div>
          </div>
        </div>
      </div>

     
    </div>
  </div>
</div>

<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>
            
      <div class="modal-footer">
        <form action=" {{ route('system.attendance.destroy') }} " method="POST">
          {!! csrf_field() !!}
          <input type="text" name="idd" id="idd" hidden=""  >
          <input type="text" name="date_from" id="from" hidden="" >
          <input type="text" name="date_to" id="to" hidden="">
          <input type="text" name="employee_id" id="emp_id" hidden="" >
        
          
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</button>
        </form>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){


    $(document).on('click','a[data-role=add_contribution]',function(){
      // alert($(this).data('from'))
      $('#employee_id').val($(this).data('id'));
      
      $('#date_from').val($(this).data('from'));
      $('#date_to').val($(this).data('to'));
    })
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});
        
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });

$(document).on('click','a[data-role=delete]',function(){
  var a = $(this).data('id');

  $('#idd').val(a)
  $('#from').val($(this).data('aa'))
  $('#to').val($(this).data('to'))
  $('#emp_id').val($(this).data('empid'))

})
            

  });

</script>
@stop
