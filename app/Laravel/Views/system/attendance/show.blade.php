@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading">Payroll<div class="tools dropdown">
          <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
          <ul role="menu" class="dropdown-menu pull-right">
            <li><a href="#filter_report" data-toggle="modal">Generate Report</a></li>
{{--             <li><a href="#filter_report_excel" data-toggle="modal">Generate Reports Excel</a></li>
 --}}          </ul>
        </div>
          <span class="panel-subtitle">Payroll information.</span>

        </div>
        <div class="panel-body">
          
          <form method="POST" action="{{ route('system.attendance.search') }}">
            {{ csrf_field() }}
          <div class="col-md-10">
            {{-- {!!Form::select('type',$user_types,old('type'),['class' => "form-control select2"])!!} --}}
            <select name="employee_id" id="employee_id" class="form-control select2" required="">
              <option value="">Search Employee</option>
              @foreach ($employees as $employee)
                <option value="{{ $employee->id }}">{{ $employee->name }}</option>
              @endforeach
            </select>
            <input type="text" class="form-control" name="month" value="{{ Request::segment(3) }}" style="display: none" >
            <input type="text" class="form-control" name="year" value="{{ Request::segment(4) }}" style="display: none" >
            <input type="text" class="form-control" name="year" value="{{ Request::segment(4) }}" style="display: none" >
          </div>
          <div class="col-md-2">
              <input type="submit" value="search" class="btn  btn-lg btn-success">
          </div>
         </form>
         

         
          <div class="col-md-12">
          
          <div class="row">
            <center><h1>PAYROLL STATUS - {{ strtoupper(date('M Y',strtotime(Request::segment(3).Request::segment(3) )))  }}</h1></center>
            <br>
  
              <div class="col-md-4" >
                <div class="form-group">
                  {{-- <h3>First Cut-off List</h3> --}}
                  <div id="result"></div>
                  <table   class="table table-bordered table-wrapper text-center" id="emp_table">
                    <thead>
                      <tr>
                      <th colspan="3"><center> Category A (01-15) </center>
                      </th>
                      </tr>
                      <th style="text-align: center;">Name</th>
                      
                      {{-- <th style="text-align: center;width: 20%">Other Deduction(s)</th> --}}
                      <th style="text-align: center;">Payroll Status</th>
                      <th style="text-align: center;">Action</th>
                    </thead>
                    <tbody>


                    @forelse ($attendance as $at)
                      <tr>
                        <td class="cell-detail">
                        <span class="cell-detail">{{ $at->name }}</span> 
                        <span class="cell-detail-description">{{ $at->department }}</span> 
                        </td>
                       
                             
                               
                              {{-- @endforelse --}}
                             

                        
            
                        
                        @if ($at->status == 'a' || $at->status == NUll)
                          <td><span class="label label-warning">PENDING</span></td>
                          @else
                            <td><span class="label label-success ">{{ strtoupper($at->status) }}</span></td>
                        @endif
                        @if($at->attendance_status == NULL)
                        <td><span class="label label-danger">Attendance Not Set</span></td>
                        @else
                          <td>
                          <div class="btn-group btn-hspace">
                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <ul role="menu" class="dropdown-menu">

                              @if ($at->status == 'created')
                                 <li><a  href="{{ route('system.payroll.payslip-created',[$at->employee_id,$at->date_from,$at->date_to]) }}">View</a></li>
                                 @else
                                 <li><a  href="{{ route('system.payroll.create',[$at->employee_id,$at->date_from,$at->date_to]) }}">Edit | Create Payroll</a></li>
                                 
                             
                              <li><a  data-role="delete" data-id="{{ $at->id }}"  data-from="{{ $at->date_from }}" data-to="{{ $at->date_to }}" href="#confirm-delete" data-toggle="modal" title="Remove Record">Remove Record</a></li>
                              @endif
                             
                          
                            </ul>
                          </div>
                        </td>
                        @endif
                      
                      </tr>
                      </tbody>
                      @empty
                        <tr>
                          <td colspan="5" class="text-center"><i>No record found yet.</i></td>
                        </tr>
                     
                    @endforelse
                  </table>  
                </div>

                

                <div class="form-group">
                  {{-- {{ $attendance->links() }} --}}
                </div>
              </div>

              <div class="col-md-4" >
                <div class="form-group">
                  {{-- <h3>2nd Cut-off List</h3> --}}
                  <div id="result"></div>
                  <table   class="table table-bordered table-wrapper text-center" id="emp_table">
                    <thead>
                      <tr>
                      <th colspan="3">  <center>Category B (16-30)</center>
                      </tr>
                      <th style="text-align: center;">Name</th>
                      
                      {{-- <th style="text-align: center;width: 20%">Other Deduction(s)</th> --}}
                      <th style="text-align: center;">Payroll Status</th>
                      <th style="text-align: center;">Action</th>
                    </thead>
                    <tbody>


                    @forelse ($attendance2 as $at)
                      <tr>
                        <td class="cell-detail">
                        <span class="cell-detail">{{ $at->name }}</span> 
                        <span class="cell-detail-description">{{ $at->department }}</span> 
                        </td>
                       
                             
                               
                              {{-- @endforelse --}}
                             

                        
            
                        
                        @if ($at->status == 'a' || $at->status == NUll)
                          <td><span class="label label-warning">PENDING</span></td>
                          @else
                            <td><span class="label label-success ">{{ strtoupper($at->status) }}</span></td>
                        @endif
                        @if($at->attendance_status == NULL)
                        <td><span class="label label-danger">Attendance Not Set</span></td>
                        @else
                          <td>
                          <div class="btn-group btn-hspace">
                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <ul role="menu" class="dropdown-menu">

                              @if ($at->status == 'created')
                                 <li><a  href="{{ route('system.payroll.payslip-created',[$at->employee_id,$at->date_from,$at->date_to]) }}">View</a></li>
                                 @else
                                 <li><a  href="{{ route('system.payroll.create',[$at->employee_id,$at->date_from,$at->date_to]) }}">Edit | Create Payroll</a></li>
                              <li><a  data-role="delete" data-id="{{ $at->id }}"  data-from="{{ $at->date_from }}" data-to="{{ $at->date_to }}" href="#confirm-delete" data-toggle="modal" title="Remove Record">Remove Record</a></li>
                              @endif
                             
                          
                            </ul>
                          </div>
                        </td>
                        @endif
                      
                      </tr>
                      </tbody>
                      @empty
                        <tr>
                          <td colspan="5" class="text-center"><i>No record found yet.</i></td>
                        </tr>
                     
                    @endforelse
                  </table>  
                </div>

                

              </div>

          
              <div class="col-md-4">
                  {{-- <center><h4 >{{ date('M Y',strtotime(Request::segment(3).Request::segment(3) )) }} - Category B</h4></center> --}}
                <div class="form-group">
                  
                  <div id="result"></div>
                  <table   class="table table-bordered table-wrapper text-center" id="emp_table">
                    <thead>
                       <tr>
                      <th colspan="3">  <center>Barangay Officials / Consultant</center>
                      </tr>
                      <th style="text-align: center;">Name</th>
                      
                      {{-- <th style="text-align: center;width: 20%">Other Deduction(s)</th> --}}
                      <th style="text-align: center;">Payroll Status</th>
                      <th style="text-align: center;">Action</th>
                    </thead>
                    <tbody>


                    @forelse ($attendance3 as $at)
                      <tr>
                        <td class="cell-detail">
                        <span class="cell-detail">{{ $at->name }}</span> 
                        <span class="cell-detail-description">{{ $at->department }}</span> 
                        </td>
                       
                             
                               
                              {{-- @endforelse --}}
                             

                        
            
                        
                        @if ($at->status == 'a' || $at->status == NUll)
                          <td><span class="label label-warning">PENDING</span></td>
                          @else
                            <td><span class="label label-success ">{{ strtoupper($at->status) }}</span></td>
                        @endif
                        @if($at->attendance_status == NULL)
                        <td><span class="label label-danger">Attendance Not Set</span></td>
                        @else
                          <td>
                          <div class="btn-group btn-hspace">
                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <ul role="menu" class="dropdown-menu">

                              @if ($at->status == 'created')
                                 <li><a  href="{{ route('system.payroll.payslip-created',[$at->employee_id,$at->date_from,$at->date_to]) }}">View</a></li>
                                 @else
                                 <li><a  href="{{ route('system.payroll.create',[$at->employee_id,$at->date_from,$at->date_to]) }}">Edit | Create Payroll</a></li>
                              <li><a  data-role="delete" data-id="{{ $at->id }}"  data-from="{{ $at->date_from }}" data-to="{{ $at->date_to }}" href="#confirm-delete" data-toggle="modal" title="Remove Record">Remove Record</a></li>
                              @endif
                             
                          
                            </ul>
                          </div>
                        </td>
                        @endif
                      
                      </tr>
                      </tbody>
                      @empty
                        <tr>
                          <td colspan="5" class="text-center"><i>No record found yet.</i> </td>
                        </tr>
                     
                    @endforelse
                  </table>  
                </div>

                

                <div class="form-group">
                  {{-- {{ $attendance->links() }} --}}
                </div>
              </div>

            </div>
          </div>
   
        </div>
      </div>
    </div>
  </div>
</div>


  
@stop

@section('page-modals')

<div id="confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Create Leave</h1>
      </div>

      <div class="modal-body">
        

        <hr>

        <div class="row">
          <div class="col-md-9">
            <div class="form-group">

              <input type="text" name="leave_category" id="leave_category" class="form-control" placeholder="Enter Category">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="submit" value="Create" id="filtesr" class="btn btn-primary form-control ">
            </div>
          </div>
        </div>
      </div>

     
    </div>
  </div>
</div>
{{-- filter  reports modal--}}
<div id="filter_report_excel" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Generate Report</h1>
      </div>
 <form action="{{  route('system.report.export') }}" method="Post">
  {{ csrf_field() }}
      <div class="modal-body">
        

        <hr>


          <div class="col-md-12">

         
            <div class="form-group">
              <label for="date">Date From</label>
              {{-- <input type="month" id="month" name="month" min="2018-03" class="form-control"  required=""> --}}
              <input type="text" class="form-control datepicker" name="date_from" placeholder="Choose date from"  autocomplete="off">
            </div>
            <div class="form-group">
              <label for="date">Date To</label>
              {{-- <input type="month" id="month" name="month" min="2018-03" class="form-control"  required=""> --}}
                <input type="text" class="form-control datepicker" name="date_to"  placeholder="Choose date to"  autocomplete="off">
            </div>
            <div class="form-group">
              <label for="date">Department/Position</label>
              <select name="department" id="department" required="" class="form-control" >
                <option value="">Choose Department</option>
                   <option value="b">Officer</option>
                @foreach ($departments as $department)
                  <option value="{{ $department->department_description }}">{{ $department->department_description }}</option>
                @endforeach
              </select>
            </div>

     
          </div>
 

     
    </div>

    <div class="modal-footer">
      
      
          
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Generate</button>
       
     
  </div>
</form>
</div>
</div>
</div>

{{-- end of filter reports modal --}}
{{-- filter  reports modal--}}
<div id="filter_report" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Generate Report</h1>
      </div>
 <form action="{{  route('system.report.index') }}" method="Post">
  {{ csrf_field() }}
      <div class="modal-body">
        

        <hr>
<table class="table table-wrapper">
  <th>Month</th>
  <th>Payrun</th>

  <th class="actions">Action</th>
  @forelse($payrolls as $payroll)
  <tr>
    <td>{{ Helper::get_month($payroll->date_from) }}</td>
    <td>{{ Helper::payrun($payroll->date_from,$payroll->date_to) }}</td>
  
    <td class="actions"><a class="label label-info" href="{{ route('system.report.index',[$payroll->date_from,$payroll->date_to])}}">Pdf</a>&nbsp;<a class="label label-success" href="{{ route('system.report.export',[$payroll->date_from,$payroll->date_to])}}">Excel</a></td>
  </tr>
  @empty
  @endforelse
</table>

          {{-- <div class="col-md-12">

         
            <div class="form-group">
              <label for="date">Date From</label>
              
              <input type="text" class="form-control datepicker" name="date_from" placeholder="Choose date from"  autocomplete="off">
            </div>
            <div class="form-group">
              <label for="date">Date To</label>
              
                <input type="text" class="form-control datepicker" name="date_to"  placeholder="Choose date to"  autocomplete="off">
            </div>
            <div class="form-group">
              <label for="date">Department/Position</label>
              <select name="department" id="department" required="" class="form-control" >
                <option value="">Choose Department</option>
                   <option value="b">Officer</option>
                @foreach ($departments as $department)
                  <option value="{{ $department->department_description }}">{{ $department->department_description }}</option>
                @endforeach
              </select>
            </div>

     
          </div> --}}
 

     
    </div>

    {{-- <div class="modal-footer">
      
      
          
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Generate</button>
       
     
  </div> --}}
</form>
</div>
</div>

{{-- end of filter reports modal --}}

<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>
            
      <div class="modal-footer">
        <form action=" {{ route('system.attendance.destroy') }} " method="POST">
          {!! csrf_field() !!}
          <input type="text" name="idd" id="idd" hidden=""  >
          <input type="text" name="date_from" id="from" hidden="" >
          <input type="text" name="date_to" id="to" hidden="">
          <input type="text" name="employee_id" id="emp_id" hidden="" >
        
          
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</button>
        </form>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){


    $(document).on('click','a[data-role=add_contribution]',function(){
      // alert($(this).data('from'))
      $('#employee_id').val($(this).data('id'));
      
      $('#date_from').val($(this).data('from'));
      $('#date_to').val($(this).data('to'));
    })
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});
        
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });

$(document).on('click','a[data-role=delete]',function(){
  var a = $(this).data('id');

  $('#idd').val(a)
  $('#from').val($(this).data('aa'))
  $('#to').val($(this).data('to'))
  $('#emp_id').val($(this).data('empid'))

})
            

  });

</script>
@stop
