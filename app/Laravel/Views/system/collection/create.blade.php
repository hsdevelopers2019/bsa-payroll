@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Create Record Form<span class="panel-subtitle">File information.</span></div>
        <div class="panel-body">
          <form method="POST" action="" enctype="multipart/form-data">
            {!!csrf_field()!!}
            
            <div class="form-group col-md-6{{$errors->first('new_sin') ? 'has-error' : NULL}}">
              <label>New SIN</label>
              <input type="text" placeholder="Enter new sin" class="form-control" name="new_sin" value="{{old('new_sin')}}">
              @if($errors->first('new_sin'))
              <span class="help-block">{{$errors->first('new_sin')}}</span>
              @endif
            </div>

            <div class="form-group col-md-6 {{$errors->first('old_sin') ? 'has-error' : NULL}}">
              <label>Old SIN</label>
              <input type="text" placeholder="Enter Old sin" class="form-control" name="old_sin" value="{{old('old_sin')}}">
              @if($errors->first('old_sin'))
              <span class="help-block">{{$errors->first('old_sin')}}</span>
              @endif
            </div>

          

           
            
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Create Record</button>
                  <a href="{{route('system.collection.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


