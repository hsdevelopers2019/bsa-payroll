@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-default">
        <div class="panel-heading panel-heading-divider">Edit Department<span class="panel-subtitle">Modify employee information.</span></div>
        <div class="panel-body">
          <form method="POST" action= "{{ route('system.department.update') }}">
            {!!csrf_field()!!}

            <div class="row">
              
                  
            </div>
         
            <div class="row">
             
               
                <div class="col-md-6">
                  <div class="form-group {{$errors->first('department') ? 'has-error' : NULL}}" autocomplete="off">
                    <label>Department</label>
                    <input type="text" value="{{ $department->id }}" hidden="" name="id">
                    <input type="text" placeholder="Enter Department name" class="form-control" name="department" autocomplete="off" value="{{ $department->department_description }}" >
                    @if($errors->first('department'))
                    <span class="help-block">{{$errors->first('department')}}</span>
                    @endif
                  </div>
                </div>

               
          
            </div>
           
            <div class="row xs-pt-15">
              <div class="col-xs-6 ">
                  <button type="submit" class="btn btn-space btn-success btn-lg pull-right">Update</button>
                  <a href="{{ route('system.department.index') }}" class="btn btn-space btn-lg btn-default pull-right">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">

</script>
@stop

