@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-4">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Department<span class="panel-subtitle">Department information.</span></div>
        <div class="panel-body">

        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <a href="#create" data-toggle="modal" class="btn btn-primary pull-right">Create Department</a>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <h3>Department List</h3>
                  <table class="table table-striped">
                    <th>Department name</th> 
                    <th width="10%"></th> 

                    @foreach ($departments as $department)
                      <tr>
                      <td>{{ $department->department_description }}</td>
                      <td>  
                        <div class="btn-group btn-hspace">
                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <ul role="menu" class="dropdown-menu">

                                {{--  <li><a href="#delete" data-toggle="modal" >Delete</a></li> --}}
                      
                                 <li><a  href="{{ route('system.department.edit',[$department->id]) }}">Edit</a></li>
                                  <li class="divider"></li>
                            </ul>
                          </div>
                      </td>
                    </tr>
                    @endforeach
                   
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="create" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Create Department</h1>
      </div>

      <div class="modal-body">
        
        
<form action="{{ route('system.department.create') }}" method="POST">
  {!! csrf_field() !!}

        <hr>

        <div class="row">
          <div class="col-md-9">
            <div class="form-group">
              <input type="text" name="department_description" id="leave_category" class="form-control" placeholder="Enter Department name">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="submit" value="Create" class="btn btn-primary form-control ">
            </div>
          </div>
        </div>

  </form>
      </div>

     
    </div>
  </div>
</div>

{{-- /////////////////////////////////////////////////////// --}}

<div id="delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm</h1>
      </div>

      <div class="modal-body">
        
         <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to delete the record?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="{{-- {{route('system.department.delete',[$department->id]) }} --}}" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>

      </div>

     
    </div>
  </div>
</div>

@stop




