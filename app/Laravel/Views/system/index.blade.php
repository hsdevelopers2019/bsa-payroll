@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    @include('system._components.notifications')


  @if(in_array($auth->type, ["super_user","admin"]))
          
    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-account-add"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Number of Employee</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">{{ $user_count }}</span>
          </div>
        </div>
      </div>
    </div>
  


    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-account-circle"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Payroll</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div>
    {{-- <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-account-o"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Under Maintenance</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div> --}}
    {{-- <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-attachment"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Under Maintenance</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div> --}}
    {{-- <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-accounts-list-alt"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Pending Approval</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div> --}}
    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-assignment-alert"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Pending Leave Request</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">{{ $leave_request }}</span>
          </div>
        </div>
      </div>
    </div>
  </div>



      
    @else



    {{-- ///////////////////////////// USERS /////////////////////////////////////////////// --}}
         <div class="user-display">
          <div class="user-display-bg"><img src="{{asset('assets/img/user-profile-display.png')}}" alt="Profile Background"></div>
          <div class="user-display-bottom">
            <div class="user-display-avatar"><img src="{{$user->avatar}}" alt="{{$user->name}}"></div>
            <div class="user-display-info">
              <div class="name">{{$user->name}}</div>
              <div class="nick"><span class="mdi mdi-account"></span> {{$user->username}}</div>
            </div>
          </div>
        </div>
          <div class="user-info-list panel panel-default">
          <div class="panel-heading panel-heading-divider">About Me
            <span class="pull-right"><a href="{{route('system.account.edit-info')}}"><i class="mdi mdi-edit"></i></a></span>
            <span class="panel-subtitle">
              @if($user->description)
              {{$user->description}}
              @else
              <i>No description provided.</i>
              @endif
            </span>
          </div>
          <div class="panel-body">
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="icon"><span class="mdi mdi-email"></span></td>
                  <td class="item">Email Address<span class="icon s7-portfolio"></span></td>
                  <td><a href="mailto:{{$user->email}}">{{$user->email}}</a></td>
                </tr>
                <tr>
                  <td class="icon"><span class="mdi mdi-lock"></span></td>
                  <td class="item">Password<span class="icon s7-portfolio"></span></td>
                  <td>******** <span class="pull-right"><a href="{{route('system.account.edit-password')}}">[Edit Password]</a></span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div> 
    @endif

</div>
@stop




@section('page-scripts')

{{-- <script src="{{asset('assets/lib/countup/countUp.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.pie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/curvedLines.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.tooltip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/chartjs/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/raphael/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/morrisjs/morris.min.js')}}" type="text/javascript"></script> --}}

@stop