@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Leave Category<span class="panel-subtitle">Leave information.</span></div>
        <div class="panel-body">

        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <a href="#create" data-toggle="modal" class="btn btn-primary pull-right">Create Leave Category</a>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <h3>Leave Category List</h3>
                  <table class="table table-striped">
                    <th>CATEGORY NAME</th> 
                    <th>ACTION</th> 
                    <tr>
                      <td>Vacation</td>
                      <td><a href="#">Edit</a> | <a href="">Delete</a></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="create" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Create Leave</h1>
      </div>

      <div class="modal-body">
        
        

        <hr>

        <div class="row">
          <div class="col-md-9">
            <div class="form-group">
              <input type="text" name="leave_category" id="leave_category" class="form-control" placeholder="Enter Category">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="submit" value="Create" class="btn btn-primary form-control ">
            </div>
          </div>
        </div>
      </div>

     
    </div>
  </div>
</div>
@stop


