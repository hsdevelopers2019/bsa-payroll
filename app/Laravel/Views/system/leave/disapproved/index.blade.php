@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
     <div class="panel-heading">Disapproved Leave Request
         <div class="tools dropdown">
            <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
              <li><a href="{{ route('system.leave-approved.index') }}">View Approved Request</a></li>
              <li><a href="{{ route('system.leave-disapproved.index') }}">View Disapproved Request</a></li>
            </ul>
          </div>
          <hr>
        </div>
        
        <div class="panel-body">

        <div class="row">
          <div class="col-md-12">
            

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <h3>Approval Request List</h3>
                  <table class="table table-striped">
                    <th>EMPLOYEE NAME</th> 
                    <th>DESCRIPTION</th> 
                    <th>DAY(S)</th> 
                    <th>DATE RANGE</th> 
                    <th>STATUS</th> 
                    <th>--</th> 
                    @forelse($leaves as $leave)
                    <tr>
                      <td>{{ $leave->name}}</td>
                      <td>{{ $leave->leave}}</td>
                      <td>{{ $leave->working_days}}</td>
                      <td>{{ date("M j", strtotime( $leave->date_from ))." - ". date("M j, Y", strtotime( $leave->date_to )) }}</td>
                     
                        @if ( $leave->status == 'approve')
                          <td><span class="text text-success">APPROVED</span>  </td>
                          @elseif($leave->status == 'disapprove')
                          <td><span class="text text-danger">DISAPPROVED</span></td>
                          @else
                          <td><span class="text text-danger">{{ $leave->status}}</span></td>
                        @endif
                       

                   
                      <td class="action">
                        <div class="btn-group btn-hspace">
                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                 <li><a href="{{ route('system.leave-approved.show',[$leave->id]) }}">review</a></li>
                            </ul>
                          </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="create" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Leave </h1>
      </div>

      <div class="modal-body">
        
        

        <hr>

        <div class="row">
          <div class="col-md-9">
            <div class="form-group">
              <input type="text" name="leave_category" id="leave_category" class="form-control" placeholder="Enter Category">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="submit" value="Create" class="btn btn-primary form-control ">
            </div>
          </div>
        </div>
      </div>

     
    </div>
  </div>
</div>
@stop


