@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Application Leave Form<span class="panel-subtitle">Leave Request information.</span></div>
        <div class="panel-body">
<style>
  span{font-weight: bold};
  i{ font-size: 20px;font-size: 20px !important  }
</style>
        <div class="row">
          
          <table class="table table-bordered">
            @foreach ($users as $user)
              {{-- expr --}}
         
            <tr>
              <td width="20%">
                <span><u>Department / Division</u></span><br>
                <i>{{ $user->department }}</i>
              </td>

              <td colspan="2">
                <span><u>Fullname</u></span><br>
                <i >{{ $user->name }}</i>
              </td>
            </tr>
{{-- ////////////////////////////////////////////////////////////////// --}}

            <tr>
              <td>
                <span><u>Date of Filling</u></span><br>
                <i>{{ date("M j, Y", strtotime($user->date_file)) }}</i>
              </td>
              <td>
                <span><u>Positon</u></span><br>
                <i>{{  $user->department  }}</i>
              </td>
              <td>
                <span><u>Monthly Salary</u></span><br>
               &#x20B1; <i>{{ number_format($user->basic_pay,2)}}</i>
              </td>
            </tr>

            <tr>
              <td colspan="3" style="text-align: center;"><span>DETAILS OF APPLICATION</span></td>
            </tr>

            <tr>
           
              <td colspan="2">
                <span>Type of Leave</span><br>
                <i>{{ $user->leave }}</i>
              </td>
          

             
              
              <td>
               <span> Number of Working days</span><br>
               <span>Applied for:</span> <i> {{ $user->working_days }} </i><br>
               <span>From:</span> <i>  {{ date("M j, Y", strtotime($user->date_from))  }} </i><br>
               <span>To:</span> <i> {{ date("M j, Y", strtotime($user->date_to)) }}</i><br>
              </td>
            </tr>

            <tr>
              @if ($user->where !=null)
              <td colspan="2">
                <span>Where Leave Will Be Spent </span><br>
                <span>(1) In Case of Vacation Leave</span><br>
                <i>{{ $user->where }}</i>
              </td>
              @else
              <td colspan="2">
                <span>Incase of Sick leave</span><br>
                <i>{{ $user->incase }}</i>
              </td>
              @endif
              
              

              <td>
                <span>Commutation :</span>
                <i>{{ $user->commutation }}</i><br>
                <div class="text-center">
                  <center>
                <p class="mt-3" style="padding-top:5px;margin-top:50px;border-top: 1px solid gray; width: 180px;">
                Signature of Applicant
                 </p>
                 </center>
                 </div>
              </td>

              <tr>
                <td colspan="3" style="text-align: center;font-weight: bold;">
                  DETAILS OF ACTION ON APPLICATION
                </td>
              </tr>

              <tr>
                <td colspan="3">

                  
              <span>Recomendation</span>

              <br>
              <form action="{{ route('system.request.store-approval') }}" method="POST">
                {{ csrf_field() }}

                <input type="text" name="id" value="{{ $user->id }}" hidden="">
               

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <input type="radio" name="recomendation" id="recomendation" value="approve" {{ $user->status =="approve" ? "checked" : "" }}>
                      <label for="recomendation">Approve</label>
                    </div>
                    <div class="form-group">
                      <input type="radio" name="recomendation" id="recomendation" value="disapprove" {{ $user->status =="disapprove" ? "checked" : "" }}>
                      <label for="recomendation">Disapprove</label>
                    </div>
                  </div>
                

                  <div class="col-md-12">
                    <button type="sumbit" class="btn btn-lg btn-success">Submit</button>
                  </div>
                </div>


              </form>











                </td>
              </tr>
            </tr>


             @endforeach
          </table>
            

        </div>


        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="create" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Leave Request</h1>
      </div>

      <div class="modal-body">
        
        

        <hr>

        <div class="row">
          <div class="col-md-9">
            <div class="form-group">
              <input type="text" name="leave_category" id="leave_category" class="form-control" placeholder="Enter Category">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="submit" value="Create" class="btn btn-primary form-control ">
            </div>
          </div>
        </div>
      </div>

     
    </div>
  </div>
</div>
@stop


