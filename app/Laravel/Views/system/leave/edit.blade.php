@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-5">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Edit Leave Category<span class="panel-subtitle">Leave information.</span></div>
        <div class="panel-body">

          <div class="row">
            <div class="col-md-8">
              
            <form action="{{ route('system.leave.update') }}" method="POST">
              {!! csrf_field() !!}
              <div class="form-group">
                <label for="">Category</label>
                <input type="text" name="id" value="{{ $category->id }}" hidden="">
                <input type="text" name="description" id="description" class="form-control" value="{{ $category->description }}" >
              </div>
           
              <div class="row">
                
             </div>
           </div>

              <div class="col-md-2">
              <div class="form-group">
                <label for="a" style="color: #fff">.</label>
               <button type="submit" class="btn btn-success btn-lg">Update</button>
              </div>
               </form>
              <div class="row">
                
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
@stop

@section('page-modals')
<form action="{{route('system.employee.destroy')}}" method="POST">
  {{ csrf_field() }}
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You want to delete This Data?</p>
        <input type="text" name="idType" id="idType" hidden="" >

        
        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary btn-raised btn-loading" >Delete</button>
         {{-- <a href="{{route('system.employee.destroy',[$type->id])}}" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="" id="btn-confirm-delete">Delete Record</a > --}}
      </div>
    </div>
  </div>
</div>
</form>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(function(){

$(document).on('click','a[data-role=delete]',function(){

  $('#idType').val($(this).data('id'))
  
  
})

})
</script>
@stop

