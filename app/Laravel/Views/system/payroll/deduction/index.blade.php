@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-5">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Deduction <span class="panel-subtitle">Deduction List.</span></div>
        <div class="panel-body">
          <div class="row">
          
                      
                <form action="{{ route('system.deduction.store')}}" method="POST">
               {!!csrf_field()!!}
                  <div class="col-md-12">
                     <div class="form-group">
                    <label for="employee_id">Select Employee </label>
                   <select name="employee_id" id="employee_id" class="form-control select2">
                    <option value="">--Select --</option>
                    @foreach ($users as $user)
                      <option value="{{ $user->id }}"> {{ $user->name }}</option>
                    @endforeach
                     
                   </select>
                      
                    {{-- <input type="text" name="employee_id" id="employee_id" placeholder="Enter employee number" class="form-control"> --}}
                  </div>
                </div>
               
                  <div class="col-md-12">
                     <div class="form-group">
                     <label for="deduction_description">Deduction Description</label>
                    <input type="text" name="description" id="description" placeholder="Enter Deduction Description" class="form-control" autocomplete="off">
                    
                  </div>
                 </div> 
                 <div class="col-md-6">
                     <div class="form-group">
                     <label for="payrun">From</label>
                     <input type="text" name="date_from" id="date_from" class="form-control datepicker" autocomplete="off">
                  </div>
                 </div> 

                  <div class="col-md-6">
                     <div class="form-group">
                     <label for="payrun">To</label>
                     <input type="text" name="date_to" id="date_to" class="form-control datepicker" autocomplete="off">
                  </div>
                 </div>


                 <div class="col-md-12">
                     <div class="form-group">
                     <label for="amount">Amount</label>
                    <input type="number" name="amount" id="amount" class="form-control" placeholder="0.00">
                  </div>
                 </div>

                  <div class="col-md-12">
                     <div class="form-group">
                    <button type="submint" class="btn btn-success pull-right btn-lg">Submit</button>
                  </div>
                 </div>
                </form>
              


            
            
          </div>
          
        </div>
      </div>
    </div>
  </div>
 {{-- //////////////////////////////////////////////////////// --}}





  {{-- //////////////////////////////////////////////////////// --}}



</div>
@stop

@section('page-modals')
<form action="{{route('system.deduction.create')}}" method="POST">
  {{ csrf_field() }}
<div id="create_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Deduction</h1>
      </div>

      <div class="modal-body">
        <form action="" method="POST">
       <div class="form-group">
         <label for="deduction">Deduction</label>
         <input type="text" name="employee_number" id="" placeholder="enter deduction type" class="form-control">
       </div>
       </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary btn-raised btn-loading" >Add</button>
         {{-- <a href="{{route('system.employee.destroy',[$type->id])}}" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="" id="btn-confirm-delete">Delete Record</a > --}}
      </div>
    </div>
  </div>
</div>
</form>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(function(){

$(document).on('click','a[data-role=delete]',function(){

  $('#idType').val($(this).data('id'))
  
  
})

  $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});

        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });
  });


})
</script>


@stop

