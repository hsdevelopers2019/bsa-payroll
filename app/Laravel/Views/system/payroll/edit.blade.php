@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Employee Compenstation Form<span class="panel-subtitle">Modify employee Compensation.</span></div>
        <div class="panel-body">

          <div class="row">
            <div class="col-md-6">
             <span style="font-size:30px">{{ strtoupper($user->name)  }}</span>
             <hr>
            </div>
          </div>
          <form method="POST" action="{{route('system.payroll.update')}}">
            {!!csrf_field()!!}
             


         <input type="text" name="employee_id" hidden="" value="{{ $user->id }}">
            <div class="form-group {{$errors->first('honoraria') ? 'has-error' : NULL}}">
              <label>Total Honoraria</label>
              <input type="number" placeholder="Enter amount" class="form-control" name="honoraria" value="{{old('honoraria',$compensation->honoraria)}}">
              @if($errors->first('honoraria'))
              <span class="help-block">{{$errors->first('honoraria')}}</span>
              @endif
            </div>

            <div class="divider">LESS DEDUCTION :</div>
            <hr>

            <div class="row">
              

              <div class="col-md-3">
              <div class="form-group {{$errors->first('tax') ? 'has-error' : NULL}}">
              <label>With Holding Tax</label>
              <input type="number" placeholder="Tax" class="form-control" name="tax" value="{{old('tax',$compensation->tax)}}">
              @if($errors->first('tax'))
              <span class="help-block">{{$errors->first('tax')}}</span>
              @endif
            </div>
            </div>


            <div class="col-md-3">
              <div class="form-group {{$errors->first('absent') ? 'has-error' : NULL}}">
              <label>Absent</label>
              <input type="number" placeholder="absent" class="form-control" name="absent" value="{{old('absent',$compensation->absent)}}">
              @if($errors->first('username'))
              <span class="help-block">{{$errors->first('absent')}}</span>
              @endif
            </div>
            </div>

            <div class="col-md-3">
              <div class="form-group {{$errors->first('loan') ? 'has-error' : NULL}}">
              <label>Share / Loan</label>
              <input type="number" placeholder="loan" class="form-control" name="loan" value="{{old('absent','0.00')}}">
              @if($errors->first('loan'))
              <span class="help-block">{{$errors->first('loan')}}</span>
              @endif
            </div>
            </div>


            <div class="col-md-3">
              <div class="form-group {{$errors->first('sss') ? 'has-error' : NULL}}">
              <label>SSS</label>
              <input type="number" placeholder="sss" class="form-control" name="sss" value="{{old('sss',$compensation->sss)}}">
              @if($errors->first('sss'))
              <span class="help-block">{{$errors->first('sss')}}</span>
              @endif
            </div>
            </div>


            <div class="col-md-3">
              <div class="form-group {{$errors->first('death_contribution') ? 'has-error' : NULL}}">
              <label>Death Contribution</label>
              <input type="number" placeholder="Death Contribution" class="form-control" name="death_contribution" value="{{old('death',$compensation->death_contribution)}}">
              @if($errors->first('death'))
              <span class="help-block">{{$errors->first('death_contribution')}}</span>
              @endif
            </div>

            <div class="divider">CUT-OFF:</div><br>
           

          
          
            </div>


            </div>
             <div class="row">

              
             <div class="col-md-3">
              <div class="form-group {{$errors->first('date_from') ? 'has-error' : NULL}}">
                <label>FROM</label>
                    <input type="date"  class="form-control" name="date_from" value="{{old('date_to',$compensation->date_from)}}">
              @if($errors->first('date_from'))
              <span class="help-block">{{$errors->first('tax')}}</span>
              @endif
            </div>
            </div>
            
             <div class="col-md-3">
              <div class="form-group {{$errors->first('date_to') ? 'has-error' : NULL}}">
                <label>TO</label>
                    <input type="date"  class="form-control" name="date_to" value="{{old('date_to',$compensation->date_to)}}">
              @if($errors->first('date_to'))
              <span class="help-block">{{$errors->first('date_to')}}</span>
              @endif
            </div>
            </div>

          </div>
            
             
          
          
            <div class="row xs-pt-15">
              <div class="col-xs-12">
                  <button type="submit" class="btn btn-space btn-lg btn-success pull-right">Submit Compensation</button>
                  <a href="{{route('system.user.index')}}" class="btn btn-space btn-lg btn-default pull-right">Cancel</a>
              </div>
            </div>

          </form>
        </div>

        


      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('input.datepicker').focus(function(){
        $(this).datetimepicker({autoclose: true,pickTime : false});
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });
  });
</script>
@stop

