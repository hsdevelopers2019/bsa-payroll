@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-primary">
        <div class="panel-heading">Record Data
          <div class="tools dropdown">
            <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
             
           
            </ul>
          </div>
        </div>
        <div class="panel-body">
          
         <hr>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-striped">
                  <thead>
                    <th>Account name</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                   @foreach ($users as $user)
                     <tr>
                        <td class="user-avatar"> <img src="{{$avatars->avatar}}" alt="{{$user->name}}">{{$user->name}}</td>
                       <td>{{ date("M j, Y", strtotime($user->date_from))}}</td>
                      <td>{{ date("M j, Y", strtotime($user->date_to))}}</td>
                      <td><a href="{{ route('system.payroll.payslip',[$user->id."/".$user->date_to]) }}">view payslip</a></td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </div>
            </div>

   
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {{-- {!!$users->render()!!} --}}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){

    


    $('#dateTo').on('change',function(){
      // var dateFrom = $('#dateFrom').val();
      // alert(dateFrom +' '+ $(this).val())
    }) 

  });
</script>
@stop

