@extends('system._layouts.main')
<link rel="stylesheet" href="{{ asset('assets/lib/datatables/css/DataTables.bootstrap.min.css')}}">
@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default   panel-border-color panel-border-color-primary">
        <div class="panel-heading">
          Create Payroll
        </div>
        <div class="panel-body">

          {{-- <div class="col-md-12"> --}}
            <hr>

        
        <form action="{{ route('system.attendance.show') }}" method="POST">
          {{ csrf_field() }}
          <h4>Select Cut-off</h4>
            
               <div class="form-group">
                  <label for="date_from">From</label>
                  <input type="text" name="date_from" id="date_from" class="form-control datepicker" autocomplete="off" placeholder="Select date from">
                </div>
            
          
              <div class="form-group">
                <label for="date_to">To</label>
                <input type="text" name="date_to" id="date_to" class="form-control datepicker" autocomplete="off" placeholder="Select date to">
              </div>
            
             
              <div class="form-group ">
                <label for="filter" style="color:#fff">.</label><br>
                <input type="submit" value="Apply Filter"  id="filter" class="btn btn-lg btn-success ">
              </div>
            

           </form>  
             
    
          {{-- </div> --}}




      </div>
    
    </div>

  </div>
</div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('input.datepicker').focus(function(){

      $(this).datetimepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        minView: "month",
        language: "fr"});

      $(this).datetimepicker('show');
      $(this).on('changeDate', function(ev){
            // do stuff
          }).on('hide', function(){
            $(this).datetimepicker('remove');
          });
        });
  });

</script>
@stop
