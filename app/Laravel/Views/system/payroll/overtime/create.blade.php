@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-success">
        <div class="panel-heading">Overtime
          <div class="tools dropdown">
            <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">


            </ul>
          </div>
        </div>
        <div class="panel-body">
        <hr>
          <div class="col-md-12">
           <div class="form-group">
             <a href="{{ route('system.overtime.index') }}" class="pull-right">Overtime List</a>
           </div>
           <form action="" method="POST">
            {!! csrf_field() !!}
             <div class="form-group {{$errors->first('employee_id') ? 'has-error' : NULL}}">
               <label for="employee_id">Select Employee</label>
              <select name="employee_id" id="employee_id" class="form-control select2">
                <option value="">---Select---</option>
                @foreach ($users as $user)
                  <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
                
              </select>
               @if($errors->first('employee_id'))
                  <span class="help-block">{{$errors->first('employee_id')}}</span>
                @endif
             </div>

             <div class="form-group {{$errors->first('hours') ? 'has-error' : NULL}}">
               <label for="hours">Number of Hour</label>
               <input type="number" name="hours" id="hours" class="form-control">
                @if($errors->first('hours'))
                  <span class="help-block">{{$errors->first('hours')}}</span>
                @endif
             </div>
               <div class="form-group {{$errors->first('date') ? 'has-error' : NULL}}">
              <label for="date_from">Date</label>
               <input type="text" name="date" id="date" class="form-control datepicker" autocomplete="off">
                @if($errors->first('date'))
                  <span class="help-block">{{$errors->first('date')}}</span>
                @endif
             </div>
             <h4>Payrun</h4>
             <hr>

             <div class="form-group {{$errors->first('date_from') ? 'has-error' : NULL}}">
              <label for="date_from">From</label>
               <input type="text" name="date_from" id="date_from" class="form-control datepicker" autocomplete="off">
                @if($errors->first('date_from'))
                  <span class="help-block">{{$errors->first('date_from')}}</span>
                @endif
             </div>
             <div class="form-group {{$errors->first('date_to') ? 'has-error' : NULL}}">
              <label for="date_to">to</label>
               <input type="text" name="date_to" id="date_to" class="form-control datepicker" autocomplete="off">
               @if($errors->first('date_to'))
                  <span class="help-block">{{$errors->first('date_to')}}</span>
                @endif
             </div>
              <div class="form-group">
              <input type="submit" value="Proceed" class="btn btn-lg btn-success ">
             </div>

           
           </form>
              <br>
         
           </div>

         </div>

         <div class="col-md-12">
            
         </div>



     </div>
     <div class="panel-footer">
      <div class="pagination-wrapper text-center">
       HRIS
      </div>
    </div>
  </div>

</div>
</div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});
        
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });



    $(document).on('click','a[data-role=delete]',function(){

              $('#id').val($(this).data('id'))
    })
  });

</script>
@stop
