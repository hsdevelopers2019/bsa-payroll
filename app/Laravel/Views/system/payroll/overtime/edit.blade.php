@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-4">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Edit Overtime<span class="panel-subtitle">Edit Overtime information.</span></div>
        <div class="panel-body">
          <div class="col-md-12">
            
      
            <form action="" method="POST">
              {{ csrf_field() }}
              <div class="form-group">
                  <label for="date_from">From</label>
                  <input type="text" value="{{ old('from',$users->date_from) }}" name="date_from" id="date_from" class="form-control datepicker" autocomplete="off">
                </div>
      
            
            
     
              <div class="form-group">
                <label for="date_to">To</label>
                <input type="text" value="{{ old('from',$users->date_to) }}" name="date_to" id="date_to" class="form-control datepicker" autocomplete="off">
              </div>
            <div class="form-group">
              <input type="text" name="id" id="" class="form-control" value="{{ old('id',$users->id) }}" style="display: none">
              <label for="hour">Number of Hour/s</label>
              <input type="text" name="hours" id="" class="form-control" value="{{ $users->hours }}">
            </div>
            <div class="form-group">
              <input type="submit" value="update" class="btn btn-success form-control">
            </div>
              </form>
          </div>
   
        </div>

  </div>


  
@stop



@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script>
   $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});
        
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });

</script>
@stop
