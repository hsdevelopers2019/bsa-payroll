@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-4">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Filter Overtime<span class="panel-subtitle">Search Overtime by Date Range.</span></div>
        <div class="panel-body">
          <div class="col-md-12">

               <div class="form-group">
                  <label for="date_from">From</label>
                  <input type="text" name="" id="date_from" class="form-control datepicker" autocomplete="off">
                </div>
      
            
            
     
              <div class="form-group">
                <label for="date_to">To</label>
                <input type="text" name="date_to" id="date_to" class="form-control datepicker" autocomplete="off">
              </div>
      
    
              <div class="form-group">
                <label for="" style="color: #fff">--</label><br>
                <input type="button" value="Apply Filter"  id="filter" class="btn  btn-lg btn-primary ">

              </div>
    
           
            
        </div>
      </div>
    </div>
  </div>

   <div class="col-md-6">
      
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Overtime Table<span class="panel-subtitle">Overtime List information.</span></div>
        <div class="panel-body">
          <div class="col-md-12">
           
              <div class="col-md-12" >
                <div class="form-group">
                  {{-- <h3>Employee List</h3> --}}
                  <div id="result"></div>
                </div>
              </div>
        </div>
      </div>
    </div>
  </div>
</div>


  
@stop

@section('page-modals')
<div id="create" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Create Leave</h1>
      </div>

      <div class="modal-body">
        
        

        <hr>

        <div class="row">
          <div class="col-md-9">
            <div class="form-group">
              <input type="text" name="leave_category" id="leave_category" class="form-control" placeholder="Enter Category">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="button" value="Create" id="filter" class="btn btn-primary form-control ">
            </div>
          </div>
        </div>
      </div>

     
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});
        
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });


              

    $('#filter').on('click',function(){


          var date_from = $('#date_from').val();
          var date_to = $('#date_to').val();
            $.ajax({
              method:"get",
              url:"{{ route('system.overtime.result') }}",
              data:{date_from:date_from,date_to:date_to},
              // headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              dataType:'json',
              success:function(data)
                {

                  
                    var table = "<table class='table table-striped'><th>Employee name </th><th>Date From</th><th>Date To</th><th>Hour(s)</th><th class='actions'></th>"
                    

                    for (var i = 0; i < data.length; i++) {
                                 

                                 table += "<tr>"
                                 table += "<td>"+data[i].name+"</td>",
                                 table += "<td>"+data[i].date_from+"</td>",
                                 table += "<td>"+ data[i].date_to +"</td>"
                                 table += "<td>"+ data[i].hours +"</td>"
                                 table += "<td><a  href='{{ route("system.overtime.edit") }}/"+data[i].id+"' </a>Edit</td>"
                              
                                 
                    }
                                 table += "</tr>"
                                 table += "</table>"
                     $("#result").html(table);
                  
              }
            })
    })
  });

</script>
@stop
