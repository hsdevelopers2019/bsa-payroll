@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-primary">
        <div class="panel-heading">Payroll from {{ date("M j, Y", strtotime($date_from))}} <strong>-</strong> {{ date("M j, Y", strtotime($date_to))}}
        
          </div>
        
        <div class="panel-body">

         <hr>
        
          <div class="col-md-12">
            <div class="form-group">


              
            </div>

           <br>
        
        <div class="form-group">
            <a  href="#contribution" data-toggle="modal" data-role="add_deduction" data-id="{{ $user->id }}" data-from="{{ $date_from }}" data-to="{{ $date_to }}" class="btn btn-success pull-right ">Add Deduction</a>
            </div>
           <div class="form-group">
            
             <table class="table table-bordered text-center" style="margin-top: 10px">
                <thead>
                  <th colspan="3" class="text-center" >DEDUCTIONS</th>
                </thead>
               <thead>
                 <th style="text-align: center;">Description</th>
                <th style="text-align: center;">Pay run</th>
                 <th style="text-align: center;">Amount</th>
               
                
               </thead>
               
               <?php $total_deduction =0; ?>
               @foreach ($payroll as $pay)
                 <tr>

                   <td>{{ strtoupper($pay->description) }}</td>
                    <td>{{ date("M j, Y", strtotime($date_from)) ."-". date("M j, Y", strtotime($date_to))}} </td>
                   <td>{{ number_format($pay->amount,2) }}</td>
                    <?php $total_deduction += $pay->amount ?> 
                 </tr>

               @endforeach

               <tr>
                  <td>Absent</td>
                  <td>{{ date("M j, Y", strtotime($date_from)) ."-". date("M j, Y", strtotime($date_to))}} </td>
                    

                    @if($absent == 0 || $absent == NULL)
                      <?php
                      $absents = 0;
                      ?>
                    @else
                    <?php
                      $absents = ($user->basic_pay / 30)* $absent  
                    ?>
                    @endif
                    
                   <td>{{  number_format( $absents ,2) }}</td>
                   {{-- Grand Total --}}
                    <?php  $grand_total_deduction = $total_deduction + $absents   ?>
                 </tr>
               <tr>
                 <td colspan="2" style="text-align: right;font-weight: bold">Total Deductions</td>
                 <td>{{ number_format($grand_total_deduction,2)}}</td>
               </tr>
             </table>
           </div>
         </div>

         <div class="col-md-12">
          <div class="form-group">
            <table class="table table-striped text-center">
                <form action="{{ route('system.payroll.storePayroll') }}" method="POST">
                  {!! csrf_field() !!}
              <thead>
                <th colspan="6">BASIC PAY: {{ number_format($user->basic_pay) }}</th>
              </thead>
              <thead>
                <th class="text-center">Gross Pay</th>
                <th class="text-center">Total Deductions</th>
                <th class="text-center">Overtime</th>
                <th class="text-center">Absent</th>
                <th class="text-center">Net Pay</th>
                <th class="text-center">PaySlip</th>
                  </thead>
                <tr>
                  <td>{{  number_format($user->basic_pay/2,2) }}</td>
                  <td>{{  number_format($grand_total_deduction,2) }}</td>
                  
                    @if ($overtime == NULL)
                    <td>0.00</td>
                    <td>00</td>
                    <input type="text" hidden="" name="overtime" value="0">
                        <input type="text" hidden="" name="net_pay" value="{{ $user->basic_pay /2 }}">
                     <td>{{ number_format(($user->basic_pay/2)  - $grand_total_deduction,2)   }}</td>
                     <?php 
                         $total_overtime =  0; 
                

                        ?>
                      @else
                      <td>{{  $overtime .'Hours' }}</td> 
                       <td>{{ $absent}} Day/s</td>
                       <?php 
                         $total_overtime =  ((($user->basic_pay / 30) / 8) * $overtime ); 
                         $net_pay = ($user->basic_pay/2) +  $total_overtime  - $grand_total_deduction;

                        ?>
                        <input type="text" hidden="" name="overtime" value="{{ $total_overtime }}">
                        <input type="text" hidden="" name="net_pay" value="{{ $net_pay }}">
                      <td>{{ number_format($net_pay,2)   }}</td>

                    @endif

                    @if ($payslip > 0)
                     <td> <a href="{{ route('system.payroll.payslip',[$user->id,$date_to,$date_from]) }}">Print</a></td>
                     @else
                  
                    <td>
               
                      <div class="form-group" >
                        
                        <input type="text" hidden="" name="employee_id" value="{{  $employee_id  }}">
                        <input type="text" hidden="" name="date_from" value="{{  $date_from  }}">
                        <input type="text" hidden="" name="date_to" value="{{  $date_to  }}">
                        <input type="text" hidden="" name="gross_pay" value="{{  $user->basic_pay/2  }}">
                        <input type="text" hidden="" name="total_deduction" value="{{ $grand_total_deduction }}">
                        <input type="text" hidden="" name="absent" value="{{ $absents }}">
                        <input type="text" hidden="" name="basic_pay" value="{{ $user->basic_pay }}">

                        <input type="text" hidden="" name="overtime" value="{{ $total_overtime }}">
                        
                        <button type="submit" id="create" class="btn btn-lg btn-success " >Create</button>
                      </div>
                  </form>
                  </td>
                    @endif
                </tr>
            
            </table>
            </div>
         
        
    
         </div>

                


     </div>



     <div class="panel-footer">
      <div class="pagination-wrapper text-center">
        {{-- {!!$users->render()!!} --}}
      </div>
    </div>

</div>
  </div>

</div>
</div>
</div>
@stop

@section('page-modals')

{{--  --}}

<div id="contribution" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
  
      <div class="modal-body">
        
<div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider"><button type="button" class="close" data-dismiss="modal">&times;</button>DEDUCTION(S)<span class="panel-subtitle">Add/edit Record Form</span></div>
        
        <div class="panel-body">
          <form method="POST" action="{{ route('system.deduction.store') }}" >
            {!!csrf_field()!!}
             
            <div class="form-group">
              <label for="amount">DESCRIPTION</label>
              <input type="text" name="description" id="descrption" class="form-control" autocomplete="off" placeholder="Enter Description">
            </div>
            <div class="form-group">
              <label for="amount">AMOUNT</label>
              <input type="number" name="amount" id="amount" class="form-control" autocomplete="off">
            </div>
          
            <div class="form-group" hidden="">
              <input type="text" name="employee_id" id="employee_id" >
              <input type="text" name="date_from" id="date_from" >
              <input type="text" name="date_to" id="date_to" >
            </div>
          

        
        </div>
      </div>

      <div class="modal-footer">
      
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success">Update Record</button>

      </div>
      </form>
      </div>
 
     
    </div>
  </div>
</div>


{{--  --}}

<div id="confirm-create" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>


<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){

    $('#create').on('click',function(){

        if(confirm('Are you sure want to procceed?') == false)
        {
           return false;
        }
     
    })

$(document).on('click','a[data-role=add_deduction]',function(){
  $('#employee_id').val($(this).data('id'));
  $('#date_from').val($(this).data('from'));
  $('#date_to').val($(this).data('to'));

})

    $('#dateTo').on('change',function(){
      // var dateFrom = $('#dateFrom').val();
      // alert(dateFrom +' '+ $(this).val())
    }) 

  });
</script>
@stop

