@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
   
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-success">
        <div class="panel-heading">Reports
          <div class="tools dropdown">
            <a href="#"  type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
              <li><a href="#" data-toggle="modal">Print Report</a></li>
              {{-- <li><a href="#">Import Calendar</a></li> --}}
              {{-- <li class="divider"></li> --}}
              {{-- <li><a href="#">Export calendar</a></li> --}}
            </ul>
          </div>
        </div>
        <div class="panel-body">
        <table class="table table-bordered table-wrapper">
            <thead>
              <th>Fullname</th>
              {{-- <th>Pay-run</th> --}}

              
              <th>Honoraria</th>
          
              <th>Allowance</th>
              <th>Amount Earned</th>
              <th>Deductions</th>
              
              <th>Net Amount Recieved</th>
              
            </thead>
            <tbody>
            @forelse($reports as $report)
            <tr id="{{ $report->id }}">
              
            <td class="cell-detail">
              <span>{{ $report->name }}</span>
              <span class="cell-detail-description">{{ $report->department }}</span>
            </td>

            <td>{{ Helper::amount($report->basic_pay)}}</td>
            <td>-</td>
            <td>{{ Helper::amount($report->net_pay - $report->total_deduction) }}</td>
            <td>{!! $report->combinedDeduction !!}</td>
            <td>{{ Helper::amount($report->net_pay - $report->total_deduction) }}</td>
           

          
            </tr>
            @empty
            @endforelse
            
            </tbody>
          </table>
        
        </div>
@stop

@section('page-modals')


{{-- //////////////////////////////////////////////////// --}}

<div id="search" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Manage Attendance</h1>
      </div>
<form action="{{ route('system.attendance-category.search') }}" method="post"> 
  {{ csrf_field() }}
      <div class="modal-body">
        
      
        <hr>

        <div class="form-group">
          <label for="department">Filter by Department/Position</label>
          <select name="department" id="department" class="form-control select2" required="">
            <option value="">Select Department/Position</option>
            @foreach ($departments as $department)
              <option value="{{ $department->department_description }}">{{ $department->department_description }}</option>
            @endforeach
            
          </select>
        </div>

    
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>

        <button type="submit" class="btn btn-success">Filter</button>
      </div>
       </form>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){





    $('#payrun').on('change',function(){


      if($(this).val() == 'first')
      {
        $('#date_from').val('{{ Carbon::now()->firstOfMonth()->format('Y-m-d')  }}')
        $('#date_to').val('{{ Carbon::now()->firstOfMonth()->addDays(14)->format('Y-m-d')  }}')
        $('#attendance_status').val('a');
        
      }
      else
      {
        $('#date_from').val('{{ Carbon::now()->firstOfMonth()->addDays(15)->format('Y-m-d')  }}')
        $('#date_to').val('{{ Carbon::now()->endOfMonth()->format("Y-m-d")}}')
        $('#attendance_status').val('aa');
      }
    });

    $(document).on('click','a[data-role=manage]',function(){
      $('#employee_id').val($(this).data('id'));
    });

    $('.datepicker').datetimepicker({autoclose: true})
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $(".focus").on("click",function(){
      $(this).select();
    })

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

  });
</script>
@stop

