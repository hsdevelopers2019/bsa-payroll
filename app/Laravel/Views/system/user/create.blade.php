@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-5">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Create Employee Account Form<span class="panel-subtitle">Employee information.</span></div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}

            <div class="row">
              
                    <div class="col-md-12" style="display: none" >
                        <div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}">
                        <label>Account Type</label>
                        {!!Form::select('type',$user_types,old('type'),['class' => "form-control select2"])!!}
                        @if($errors->first('type'))
                        <span class="help-block">{{$errors->first('type')}}</span>
                        @endif
                      </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group {{$errors->first('employee_type') ? 'has-error' : NULL}}">
                        <label>Employee Type</label>

                        {!!Form::select('employee_type',$employee_types,old('employee_type'),['class' => "form-control select2"])!!}
                        @if($errors->first('employee_type'))
                        <span class="help-block">{{$errors->first('employee_type')}}</span>
                        @endif
                      </div>
                    </div>

                     <div class="col-md-12">
                        <div class="form-group {{$errors->first('department') ? 'has-error' : NULL}}">
                        <label>Postion</label>
                          <select name="department" id="" class="form-control">
                            <option value="">--select--</option>
                            @foreach ($departments as $department)
                                  <option value="{{$department->department_description}}">{{$department->department_description}}</option>
                            @endforeach
               
                          </select>
                        @if($errors->first('department'))
                        <span class="help-block">{{$errors->first('department')}}</span>
                        @endif
                      </div>
                    </div>
                
                    <div class="col-md-12">
                      
                       <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
                          <label>Account Name</label>
                          <input type="text" placeholder="Complete name" class="form-control" name="name" value="{{old('name')}}" autocomplete="off">
                          @if($errors->first('name'))
                          <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                      
                       <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
                        <label>Email Address</label>
                        <input type="email" placeholder="email" class="form-control" name="email" value="{{old('email')}}" autocomplete="off">
                        @if($errors->first('email'))
                        <span class="help-block">{{$errors->first('email')}}</span>
                        @endif
                      </div>
                    </div>

      
                    <div class="col-md-12" style="display: none">
                       <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}" autocomplete="off">
                        <label>Username</label>
                        <input type="text" placeholder="Username" class="form-control" name="username" value="{{old('username')}}" autocomplete="off">
                        @if($errors->first('username'))
                        <span class="help-block">{{$errors->first('username')}}</span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-12">
                   <div class="form-group {{$errors->first('date_hired') ? 'has-error' : NULL}}" autocomplete="off">
                        <label>Date hired</label>
                        <input type="text" class="form-control datepicker" name="date_hired" value="{{old('date_hired')}}" autocomplete="off">
                        @if($errors->first('date_hired'))
                        <span class="help-block">{{$errors->first('date_hired')}}</span>
                        @endif
                      </div>
            </div>
             <div class="col-md-12">
                       <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}" autocomplete="off">
                        <label>Payroll Category</label>
                        <select name="payroll_category" id="" class="form-control">
                          <option value="a">Category A</option>
                          <option value="b">Category B</option>
                        </select>
                        @if($errors->first('username'))
                        <span class="help-block">{{$errors->first('username')}}</span>
                        @endif
                      </div>
                </div>
         
            
             <div class="col-md-12">
                  <div class="form-group {{$errors->first('basic_pay') ? 'has-error' : NULL}}" autocomplete="off">
                    <label>Basic Pay</label>
                    <input type="number" placeholder="Enter Basic pay" class="form-control" name="basic_pay" >
                    @if($errors->first('basic_pay'))
                    <span class="help-block">{{$errors->first('basic_pay')}}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-12">
                  <div style="display: none" class="form-group {{$errors->first('password') ? 'has-error' : NULL}}" autocomplete="off">
                    <label>Password</label>
                    <input type="password" placeholder="Password" class="form-control" name="password" value="123456" >
                    @if($errors->first('password'))
                    <span class="help-block">{{$errors->first('password')}}</span>
                    @endif
                  </div>
                </div>

                <div class="col-md-12">
                   <div class="form-group" style="display: none">
                    <label>Verify Password</label>
                          <input type="password" style="display: none" placeholder="Verify Password" value="123456"  class="form-control" name="password_confirmation" >
                   </div>
                </div>
                <div class="col-md-12">
                <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}" autocomplete="off">
                  <label>Description</label>
                  <textarea name="description" id="" cols="30" rows="3" class="form-control" placeholder="Tell something about him/her.">{{old('description')}}</textarea>
                  @if($errors->first('description'))
                  <span class="help-block">{{$errors->first('description')}}</span>
                  @endif
          
              </div>
              </div>
                
              
                 
              
        
           
            <div class="row xs-pt-15">
              <div class="col-xs-12 ">
                  <button type="submit" class="btn btn-space btn-success btn-lg pull-right">Create Account</button>
                 
              </div>
            </div>

          </form>
{{-- 

          <style>
* {
  box-sizing: border-box;
}
/*body {
  background:url(../background2.png);
}*/
#regForm {
  background-color: #ffffff;
  margin: 0px auto;
  font-family: Raleway;
  /*padding: 40px;*/
  width: 40%;
  min-width: 300px;
  border-radius:10px;
}
h1 {
  text-align: center;  
}
input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}
input.invalid {
  background-color: #ffdddd;
}
.tab {
  display: none;
}
button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}
button:hover {
  opacity: 0.8;
}
#prevBtn {
  background-color: #bbbbbb;
}
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}
.step.active {
  opacity: 1;
}
.step.finish {
  background-color: #4CAF50;
}
</style>
<body>



  <div class="col-md-12">
    <form id="regForm" action="action.php" method="post">

  <div class="tab">Personal onformation:

    <p><input placeholder="First name..." oninput="this.className = ''" name="name"></p>
    <p><input placeholder="Username..." oninput="this.className = ''" name="username"></p>
    <p><input placeholder="E-mail..." oninput="this.className = ''" name="email"></p>

  </div>
  <div class="tab">Type / Department:
    <p>{!!Form::select('type',$user_types,old('type'),['class' => "form-control select2"])!!}</p>
    <p>{!!Form::select('employee_type',$employee_types,old('employee_type'),['class' => "form-control select2"])!!}</p>
    <p>
      <select name="department" id="" class="form-control">
                            <option value="">--select--</option>
                            @foreach ($departments as $department)
                                  <option value="{{$department->department_description}}">{{$department->department_description}}</option>
                             @endforeach
        </select>                          
    </p>
  </div>
  <div class="tab">Employee Details:
    <p><input placeholder="date hired" oninput="this.className = ''" name="date_hired"></p>
    <p><input placeholder="department" oninput="this.className = ''" name="department"></p>
    <p><input placeholder="Basic Pay" oninput="this.className = ''" name="basic_pay"></p>
  </div>
  <div class="tab">Password:
    <p><input placeholder="Username..." oninput="this.className = ''" name="uname"></p>
    <p><input placeholder="Password..." oninput="this.className = ''" name="pword" type="password"></p>
  </div>
  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
 <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
  </div>
</form>
  </div>



<script>
var currentTab = 0;
showTab(currentTab);

function showTab(n) {
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  fixStepIndicator(n)
}

function nextPrev(n) {
  var x = document.getElementsByClassName("tab");
  if (n == 1 && !validateForm()) return false;
  x[currentTab].style.display = "none";
  currentTab = currentTab + n;
  if (currentTab >= x.length) {
    document.getElementById("regForm").submit();
    return false;
  }
  showTab(currentTab);
}

function validateForm() {
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  for (i = 0; i < y.length; i++) {
    if (y[i].value == "") {
      y[i].className += " invalid";
      valid = false;
    }
  }
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid;
}
function fixStepIndicator(n) {
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  x[n].className += " active";
}
</script> --}}

        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
   $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});

        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });
  });

</script>
@stop

