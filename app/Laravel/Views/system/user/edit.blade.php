@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Employee Data File Form<span class="panel-subtitle">Modify employee information.</span></div>
        <div class="panel-body">
         
           
         
    <ul class="nav nav-tabs">
      <li >
        <a  href="#1" data-toggle="tab">Personal Info</a></li>
      <li><a href="#2" data-toggle="tab">User Account Details</a></li>
      <li class="active"><a href="#3" data-toggle="tab">Basic Pay / Deduction</a></li>
      <li><a href="#4" data-toggle="tab">Contribution</a></li>
    </ul>
  
      <div class="tab-content ">
        {{-- Persnal informatoin tab --}}
        <div class="tab-pane" id="1">
            <form method="POST" action="{{ route('system.user.update') }}">
            {!!csrf_field()!!}
            <input type="text" name="id" hidden="" value="{{ $user->id}}">
            <div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}" hidden="">
              <label>Account Type</label>
              {!!Form::select('type',$user_types,old('type',$user->type),['class' => "form-control select2"])!!}
              @if($errors->first('type'))
              <span class="help-block">{{$errors->first('type')}}</span>
              @endif
            </div>
         
            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Account Name</label>
              <input type="text" placeholder="Complete name" class="form-control" name="name" value="{{old('name',$user->name)}}" autocomplete="off">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
              <label>Email Address</label>
              <input type="text" placeholder="email" class="form-control" name="email" value="{{old('email',$user->email)}}" autocomplete="off">
              @if($errors->first('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
              @endif
            </div>

      
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Account</button>
                  <a href="{{route('system.user.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
        {{-- End Persnal informatoin tab --}}

        {{-- Persnal Company Details tab --}}
        <div class="tab-pane" id="2">
          
          
             <form method="POST" action="{{ route('system.user.update') }}">
               {!!csrf_field()!!}

            <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}">
              <label>Employee ID</label>
             <input type="text" name="" disabled="" value="{{ $user->id}}" class="form-control">
             <input type="text" name="id" style="display: none" value="{{ $user->id}}" class="form-control">

            <input type="text"  hidden="" value="{{ $user->id}}" >
              @if($errors->first('username'))
              <span class="help-block">{{$errors->first('username')}}</span>
              @endif
            </div>


            <div class="form-group {{$errors->first('department_description') ? 'has-error' : NULL}}">
              <label>Department</label>

              <select name="department" id="department" class="form-control select2">
              
                <option value="{{ $user->department_description }}">{{ $user->department_description }}</option>
            @foreach ($departments as $department)
                  <option value="{{$department->department_description }}">{{ $department->department_description }}</option>
                @endforeach 
              </select>
             


              @if($errors->first('department'))
              <span class="help-block">{{$errors->first('department')}}</span>
              @endif
            </div>
            
            <div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}" hidden="">
              <label>Account Type</label>
              {!!Form::select('type',$user_types,old('type',$user->type),['class' => "form-control select2"])!!}
              @if($errors->first('type'))
              <span class="help-block">{{$errors->first('type')}}</span>
              @endif
            </div>
         

      <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}">
              <label>Username</label>
              <input type="text" placeholder="Username" class="form-control" name="username" value="{{old('username',$user->username)}}">
              @if($errors->first('username'))
              <span class="help-block">{{$errors->first('username')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
              <label>Description</label>
              <textarea name="description" id="" cols="30" rows="3" class="form-control" placeholder="Tell something about him/her.">{{old('description',$user->description)}}</textarea>
              @if($errors->first('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('password') ? 'has-error' : NULL}}">
              <code>Note: If you want to <strong>reset account password</strong>. Fill up the password below.</code>
              <label>New Password</label>
              <input type="password" placeholder="New Password" class="form-control" name="password" >
              @if($errors->first('password'))
              <span class="help-block">{{$errors->first('password')}}</span>
              @endif
            </div>
            <div class="form-group">
              <label>Verify New Password</label>
              <input type="password" placeholder="Verify New Password" class="form-control" name="password_confirmation" >
            </div>

      
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Account</button>
                  <a href="{{route('system.user.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>




        </div>
        {{-- end Persnal  Company Details tab  --}}

        {{-- Persnal  Financial tab  --}}
        <div class="tab-pane active" id="3">
          <div class="row">
            <div class="col-md-12">



              <form action="{{ route('system.user.update') }}" method="POST">
                    {!!csrf_field()!!}


                  
                  {{-- .........................................hidden ...........................................--}}
                    <div hidden="">
                  <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}">
              <label>Employee ID</label>
             <input type="text" name="" disabled="" value="{{ $user->id}}" class="form-control">
             <input type="text" name="id" style="display: none" value="{{ $user->id}}" class="form-control">

            <input type="text"  hidden="" value="{{ $user->id}}" >
              @if($errors->first('username'))
              <span class="help-block">{{$errors->first('username')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('department_description') ? 'has-error' : NULL}}">
              <label>Department</label>

              <select name="department" id="department" class="form-control select2">

                <option value="{{ $user->department }}">{{ $user->department_description }}</option>
                @foreach ($departments as $department)
                  <option value="{{$department->department_description }}">{{ $department->department_description }}</option>
                @endforeach
              </select>
             
              @if($errors->first('department'))
              <span class="help-block">{{$errors->first('department')}}</span>
              @endif
            </div>
            
            <div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}" hidden="">
              <label>Account Type</label>
              {!!Form::select('type',$user_types,old('type',$user->type),['class' => "form-control select2"])!!}
              @if($errors->first('type'))
              <span class="help-block">{{$errors->first('type')}}</span>
              @endif
            </div>
         

      <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}">
              <label>Username</label>
              <input type="text" placeholder="Username" class="form-control" name="username" value="{{old('username',$user->username)}}">
              @if($errors->first('username'))
              <span class="help-block">{{$errors->first('username')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
              <label>Description</label>
              <textarea name="description" id="" cols="30" rows="3" class="form-control" placeholder="Tell something about him/her.">{{old('description',$user->description)}}</textarea>
              @if($errors->first('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('password') ? 'has-error' : NULL}}">
              <code>Note: If you want to <strong>reset account password</strong>. Fill up the password below.</code>
              <label>New Password</label>
              <input type="password" placeholder="New Password" class="form-control" name="password" >
              @if($errors->first('password'))
              <span class="help-block">{{$errors->first('password')}}</span>
              @endif
            </div>
            <div class="form-group">
              <label>Verify New Password</label>
              <input type="password" placeholder="Verify New Password" class="form-control" name="password_confirmation" >
            </div>

      
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Account</button>
                  <a href="{{route('system.user.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>
</div>
                  {{--............... edn hidden ...................................................--}}
               
                    <a  href="#" data-toggle="collapse" class="pull-right" data-target="#demo" style="font-weight: bold;font-size: 18">VIew Basic Pay</a>
                    <div id="demo" class="collapse">
                        <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="id" id="" value="{{ $user->id }}" hidden="">
                            <label for="basic_pay">Amount</label>
                            <input type="number" name="basic_pay" id=""class="form-control" value="{{ $user->basic_pay }}">
                            
                        </div>
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group">
                    
                        <button type="submit" class="btn btn-space btn-lg btn-success">Update</button>
                        </div>
                      </div>
                    </div>
                    </div>
              </form>



<div class="col-md-4">
  <h3> <strong>Deductions</strong> </h3>
  {{-- <form action="{{route('system.payroll.show')}}" method="POST">
    {!! csrf_field() !!}
  
<label for="date">FILTER BY DATE</label>
<input type="text" name="employee_id" hidden="" value="{{ $user->id}}">
<p><input type="text" name="date" id="datepicker" class="datepicker" autocomplete="off"><button type="submit" >filter</button></p> 
</form> --}}
</div>
  
  <br>

      <div class="row">
        <div class="col-md-12">
          <table class="table table-striped">
            <tr>
              <th colspan="5">HISTORY</th>
            </tr>
            
            <th>-</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Pay run</th>
            <th>Total</th>
          <?php $total = 0; ?>
            @foreach ($contributions as $row)
             <tr>
              <td><a href="#delete" data-toggle="modal" data-role="delete" data-id="{{ $row->id }}" class="text text-danger" >&times;</a></td>
               <td>{{ $row->description }}</td>
               <td>{{ $row->amount }}</td>
               <td>{{ date("M j, Y", strtotime($row->date_to))}} </td>
               <td>{{ $row->amount }}</td>
             </tr>
               
               <?php $total += $row->amount ?>
        
            @endforeach

                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: right;font-weight: bold">Total Deduction :</td>
                    <td>{{ number_format($total) }}</td>
                  </tr>
             

          </table>
        </div>
      </div>



            </div>
          </div>
        </div>
        {{-- end Persnal  Company Details tab  --}}




        {{-- Contribution tab--}}
        <div class="tab-pane" id="4">
          <div class="row">
            <div class="col-md-12">

              
<br>
  <form action="{{ route('system.deduction.contribution') }}" method="POST">
                    {!!csrf_field()!!}
                    <input type="text" name="employee_id" id="employee_id" hidden="" value="{{ $user->id }}" >

                      {{-- expr --}}
                


                    <div class="row">
                      
                        <div class="col-md-6">
                          <div class="form-group">
                          <label for="philhealth">From</label>
                            <input type="text" name="date_from" id="payrun" class="form-control datepicker" value="{{ old('date_from') }}" autocomplete="off">
                        </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                          <label for="philhealth">to</label>
                            <input type="text" name="date_to" id="payrun" class="form-control datepicker" value="{{ old('date_to') }}" autocomplete="off">
                        </div>
                        </div>
                        
                            
                   
                    
                      <div class="col-md-12">
                          <div class="form-group">
                            <input type="text" name="type" id="" value="{{ $user->type }}" hidden="">
                            <label for="description">Contribution</label>
                            <select name="description" id="" class="form-control">
                              <option value="">--SELECT---</option>
                              <option value="sss">S.S.S</option>
                              <option value="pagibig">PAG-IBIG</option>
                              <option value="philhealth">PHILHEALTH </option>
                            </select>
                          </div>
                        </div>
              
                
                   
        
                      <div class="col-md-12">
                            <label for="philhealth">AMOUNT</label>
                            <input type="number" name="amount" id="amount" class="form-control" value="{{ old('amount') }}">
                          </div>
                        </div>


                        <div class="row xs-pt-15">
                              <div class="col-xs-6">
                                  <button type="submit" class="btn btn-space btn-lg btn-success">Update</button>
                              </div>
                        </div>
                    </div>


           
              </form>
              <br>
            </div>
          </div>
        </div>
        {{-- end contribution tab --}}
      </div>

    
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')



<div id="delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm</h1>
      </div>

      <div class="modal-body">
        <form action="{{ route('system.deduction.delete')}}" method="POST">
          {!! csrf_field() !!}
         <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to delete the record?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>
              <input type="text" name="id" id="id" value="" hidden="">
      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button  type="submit" class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</button>
      </div>

      </div>

     </form>
    </div>
  </div>
</div>

@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});
        
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });



    $(document).on('click','a[data-role=delete]',function(){

              $('#id').val($(this).data('id'))
    })
  });

</script>
@stop