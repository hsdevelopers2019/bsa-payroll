@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-default">
        <div class="panel-heading panel-heading-divider">Edit Employee Type Form<span class="panel-subtitle">Modify employee information.</span></div>
        <div class="panel-body">


          <form method="POST" action="{{route('system.employee.update')}}">
            {!!csrf_field()!!}

            <div class="row">
              
                  
            </div>
         
            <div class="row">
             
               

                <div class="col-md-6">
                  <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}" autocomplete="off">
                    <input type="text" name="id" value="{{ $type->id }}" hidden="">
                    <label>Employee Type</label>
                    <input type="text" placeholder="Enter employee Type" class="form-control" name="description" autocomplete="off" value="{{ $type->description }}" >
                    @if($errors->first('description'))
                    <span class="help-block">{{$errors->first('description')}}</span>
                    @endif
                  </div>
                </div>

               

          
            </div>
           
            <div class="row xs-pt-15">
              <div class="col-xs-6 ">
                  <button type="submit" class="btn btn-space btn-success btn-lg pull-right">Update</button>
                  <a href="{{route('system.employee.index')}}" class="btn btn-space btn-lg btn-default pull-right">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">

</script>
@stop

