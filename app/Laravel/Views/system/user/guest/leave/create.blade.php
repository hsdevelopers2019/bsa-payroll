  @extends('system._layouts.main')

  @section('content')
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        @include('system._components.notifications')
        <div class="panel panel-default panel-border-color panel-border-color-success">
          <div class="panel-heading panel-heading-divider">Leave Form<span class="panel-subtitle">Leave employee Information.</span></div>
          <div class="panel-body">
            <style>
             .text-color-green {
              color: #227b43;
            }
            .bg-color-green {
              background-color: #227b43;
            }
            .w-5 {
              width: 5%;
            }
            .font-semibold {
              font-weight: 500;
            }
            .spacing-1 {
              letter-spacing: 1px;
            }
            .text-xxxsmall {
              font-size: 1.3rem !important;
            }
            .text-xxsmall {
              font-size: 1.3rem !important;
            }
          </style>
          <section style="padding-bottom: 40px; padding-top: 40px;">

            <div class="row">
              <div class="col-lg-12 text-center">
                <label class="font-semibold text-color-gray text-xsmall spacing-2 ">APPLICATION FOR LEAVE</label><hr style="height: 2%;" class="w-5 bg-color-green">
              </div>



                              

                 <form action="{{ route('system.guest.store') }}" method="POST">
                {!! csrf_field() !!}
              <div class="col-lg-12 mt-3">
                <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
                <input type="text" value="{{ auth()->user()->id }}" name="employee_id" hidden="">
                <label for="firstname" class="text-xxxsmall font-semibold text-uppercase spacing-1">Name</label>
                <input type="text" class="form-control text-xxsmall " name="name" id="name" placeholder="Insert your firstname" name="" autocomplete="off" value="{{ auth()->user()->name }}">
                @if($errors->first('name'))
                  <span class="help-block">{{$errors->first('name')}}</span>
                @endif
              </div>
              </div>
           
             
              <div class="col-lg-6 mt-3">
                <div class="form-group {{$errors->first('department') ? 'has-error' : NULL}}">
                <label for="department" class="text-xxxsmall font-semibold text-uppercase spacing-1">Department / Division</label>
                <input type="text" class="form-control text-xxsmall " name="department" id="department" placeholder="Insert your department" name="" autocomplete="off" value="{{ auth()->user()->department }}">
                @if($errors->first('department'))
                  <span class="help-block">{{$errors->first('department')}}</span>
                @endif
              </div>
              </div>


              <div class="col-lg-6 mt-3">
                <div class="form-group {{$errors->first('date_file') ? 'has-error' : NULL}}">
                <label for="date" class="text-xxxsmall font-semibold text-uppercase spacing-1">Date of Filing</label>
                <input type="text" class="form-control text-xxsmall datepicker " name="date_file" id="date" name="" autocomplete="off" placeholder="Enter Date" value="{{ old('date_file')}}">
                 @if($errors->first('date_file'))
                  <span class="help-block">{{$errors->first('date_file')}}</span>
                @endif
              </div>
              </div>
              <div class="col-lg-6 mt-3">
                <div class="form-group {{$errors->first('position') ? 'has-error' : NULL}}">
                <label for="position" class="text-xxxsmall font-semibold text-uppercase spacing-1">Position</label>
                <input type="text" class="form-control text-xxsmall " id="position" name="position" placeholder="Insert your position" name="" value="{{ auth()->user()->department }}">
                 @if($errors->first('position'))
                  <span class="help-block">{{$errors->first('position')}}</span>
                @endif
              </div>
              </div>
              <div class="col-lg-6 mt-3">
                <div class="form-group {{$errors->first('salary') ? 'has-error' : NULL}}">
                <label for="salary" class="text-xxxsmall font-semibold text-uppercase spacing-1">Monthly Salary</label>
                <input type="text" class="form-control text-xxsmall " id="salary" name="salary" placeholder="Insert your monthly salary" name="" value="{{ auth()->user()->basic_pay }}">
                @if($errors->first('salary'))
                  <span class="help-block">{{$errors->first('salary')}}</span>
                @endif
              </div>
              </div>
              <div class="col-md-12"><hr></div>
              
              <div class="col-lg-12 text-center mt-5 ">

                <label class="font-semibold text-color-gray text-xsmall spacing-2 ">DETAILS OF APPLICATION</label><hr style="height: 2%;" class="w-5 bg-color-green">
              </div>


              <div class="col-lg-3 mt-3">
                <div class="form-group {{$errors->first('leave') ? 'has-error' : NULL}}">
                <label for="leave" class="text-xxxsmall font-semibold text-uppercase spacing-1">Type of leave</label>
                <select id="leave" name="leave" class="form-control text-xxsmall ">
                  <option value="{{ old('leave')}}"> {{ old('leave') ??  "Choose your type of leave"  }}</option>
                  @foreach ($leave_categories as $category)
                    <option value="{{ $category->description }}"> {{ $category->description }} </option>
                  @endforeach
                 
                </select>
                 @if($errors->first('leave'))
                  <span class="help-block">{{$errors->first('leave')}}</span>
                @endif
              </div>
              </div>



              <div class="col-lg-3 mt-3">
                 <div class="form-group {{$errors->first('working_days') ? 'has-error' : NULL}}">
                <label for="working-days" class="text-xxxsmall font-semibold text-uppercase spacing-1">Number of working days</label>
                <input type="number" class="form-control text-xxsmall " id="working-days" name="working_days" placeholder="Applied for" autocomplete="off" value="{{ old('working_days') }}">
                @if($errors->first('working_days'))
                  <span class="help-block">{{$errors->first('working_days')}}</span>
                @endif
              </div>          
              </div>
                

              <div class="col-lg-2 mt-3">
                <div class="form-group {{$errors->first('date_from') ? 'has-error' : NULL}}">
                <label for="From" class="text-xxxsmall font-semibold text-uppercase spacing-1">From</label>
                <input type="text" class="form-control text-xxsmall datepicker" name="date_from" id="From" placeholder="" autocomplete="off" value="{{ old('date_from') }}" >     
                 @if($errors->first('date_from'))
                  <span class="help-block">{{$errors->first('date_from')}}</span>
                @endif
              </div>     
              </div>


              <div class="col-lg-3 mt-3">
                <div class="form-group {{$errors->first('date_to') ? 'has-error' : NULL}}">
                <label for="to" class="text-xxxsmall font-semibold text-uppercase spacing-1">To</label>
                <input type="text" class="form-control text-xxsmall datepicker" name="date_to" id="to" placeholder="" autocomplete="off" value="{{ old('date_to') }}" >     @if($errors->first('date_to'))
                  <span class="help-block">{{$errors->first('date_to')}}</span>
                @endif
              </div>       
              </div>

              <div class="col-md-12"><hr></div>


           
                
              <div id="vacation" style="display: none" class="col-lg-3 mt-3">
                <div class="form-group {{$errors->first('where') ? 'has-error' : NULL}}">
                <label  class="text-xxxsmall font-semibold text-uppercase spacing-1">Where leave will be spent</label>

                <label  class="text-xxxsmall font-semibold spacing-1">Incase of vacation leave</label>

                <div class="custom-control custom-radio">
                  <input type="radio" id="customRadio1" name="where" value="within the Philippines" class="custom-control-input" {{ old('where') ==  "within the Philippines" ? "checked" : ""}}>
                  <label class="custom-control-label text-xxxsmall pt-1" for="customRadio1">Within the Philippines</label>
                </div>


                <div  class="custom-control custom-radio">
                  <input type="radio" id="customRadio2" name="where" value="others" class="custom-control-input " {{ old('where') ==  "others" ? "checked" : ""}}>
                  <label class="custom-control-label text-xxxsmall pt-1" for="customRadio2">Others</label>
                </div>
                    @if($errors->first('where'))
                  <span class="help-block">{{$errors->first('where')}}</span>
                @endif
              </div> 
              </div>

              
             


              <div id="sick" style="display: none"  class="col-lg-4 mt-5">

                <div class="form-group {{$errors->first('incase') ? 'has-error' : NULL}}">
                <label  class="text-xxxsmall font-semibold spacing-1">In case of sick leave</label>
                <div class="custom-control custom-radio">
                  <input type="radio" id="customRadio3" name="incase" value="in hospital" class="custom-control-input"{{ old('incase') == "in hospital" ? "checked" : "" }}>
                  <label class="custom-control-label text-xxxsmall pt-1" for="customRadio3">In Hospital</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" id="customRadio4" name="incase" value="Out Patient" class="custom-control-input " {{ old('incase') ==  "Out Patient" ? "checked" : ""}}>
                  <label class="custom-control-label text-xxxsmall pt-1" for="customRadio4">Out Patient</label>
                </div>
                 @if($errors->first('incase'))
                  <span class="help-block">{{$errors->first('incase')}}</span>
                @endif
              </div> 
              </div>


              <div class="col-lg-5 mt-5">
                <div class="form-group {{$errors->first('commutation') ? 'has-error' : NULL}}">
                <label  class="text-xxxsmall font-semibold spacing-1">Commutation</label>
                <div class="custom-control custom-radio">
                  <input type="radio" id="customRadio5" name="commutation" value="Request" class="custom-control-input"{{ old('commutation') == "Request" ? "checked": "" }}>
                  <label class="custom-control-label text-xxxsmall pt-1"  for="customRadio5">Request</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" id="customRadio6" name="commutation" value="Not Request" class="custom-control-input " {{ old('commutation') == "Not Request" ? "checked" : "" }}>
                  <label class="custom-control-label text-xxxsmall pt-1" for="customRadio6">Not Request</label>
                </div>
                @if($errors->first('commutation'))
                  <span class="help-block">{{$errors->first('commutation')}}</span>
                @endif
              </div> 
              </div>

              <div class="col-md-12">
                <button type="submit" class="btn btn-lg btn-success pull-right">Submit</button>
              </div>
              </form>
            </div>



          </div>
        </section>


      </div>

      
    </div>
  </div>
</div>
</div>
@stop



@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
$(function(){

$(document).on('click','a[data-role=delete]',function(){

  $('#idType').val($(this).data('id'))
  
  
})

  $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});

        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });
  });


  $('#leave').on('change',function(){
    
    if($(this).val() == 'VACATION')
    {
      $('#vacation').css('display','block');
       $('#sick').css('display','none');
    }
    else
    {
      $('#sick').css('display','block');
       $('#vacation').css('display','none');
    }
  })

})
</script>

@stop

