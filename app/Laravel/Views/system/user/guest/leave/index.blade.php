  @extends('system._layouts.main')

  @section('content')
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        @include('system._components.notifications')
        <div class="panel panel-default panel-border-color panel-border-color-success">
          <div class="panel-heading panel-heading-divider">Leave Form<span class="panel-subtitle">Leave employee Information.</span></div>
          <div class="panel-body">
      
          
      <div class="row">
        <div class="col-md-12">
           <table class="table table-hover table-wrapper" id="emp_table">
            <thead>
              <tr>
                <th style="width:25%;">Description</th>
                <th>Number of Day(s)</th>
                <th>Date range</th>
                <th >Status</th>
              </tr>
            </thead>
            <tbody>
              @forelse($leave_request as  $leave)
              <tr>
                <td>{{ $leave->leave }}</td>
                <td>{{ $leave->date_from }}</td>
                <td>{{ $leave->date_to }}</td>
               
                 @if ( $leave->status == 'approve')
                          <td><span class="text text-success">APPROVED</span>  </td>
                          @elseif($leave->status == 'disapprove')
                          <td><span class="text text-danger">DISAPPROVED</span></td>
                          @else
                          <td><span class="text text-danger">{{ $leave->status}}</span></td>
                        @endif
              
              </tr>
              @empty
              <td colspan="5" class="text-center"><i>No record found yet.</i> <a href="{{route('system.user.create')}}"><strong>Click here</strong></a> to create one.</td>
              @endforelse
         
            </tbody>
          </table>
        </div>
      </div>

          </div>

      
    </div>
  </div>
</div>
</div>
@stop



@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
$(function(){

$(document).on('click','a[data-role=delete]',function(){

  $('#idType').val($(this).data('id'))
  
  
})

  $(function(){
    $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});

        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });
  });


})
</script>

@stop

