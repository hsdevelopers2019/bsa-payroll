@extends('system._layouts.main')

@section('content')

<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Employee List<span class="panel-subtitle">Modify employee information.</span></div>
        <div class="panel-body">
         
           
           <table class="table" id="emp_table">
            <thead>
              <tr>
                <th style="width:25%;">Account Name</th>
                <th>Email</th>
                <th>Date Hired</th>
                <th>Department</th>
                <th class="actions">Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse($users as $index => $user)
              <tr>

                <td class="user-avatar"> <img src="{{$user->avatar}}" alt="{{$user->name}}">{{$user->name}}</td>
                <td class="cell-detail"> 
                  <span><a href="mailto:{{$user->email}}">{{$user->email}}</a></span>
                  <span class="cell-detail-description">{{"@".$user->username}}</span>
                </td>
                <td class="cell-detail">{{ date("M j, Y", strtotime($user->date_hired)) }}</td>
                <td class="cell-detail">{{$user->department}}</td>


                @if(request()->segment(2) == "compensation")
                <td><a href="">Add Compenstation</a></td>
                @else
                <td class="actions">
                  {{-- <a href="{{route('system.user.edit',[$user->id])}}" class="icon btn btn-sm" title="Edit Record"><i class="mdi mdi-edit text-primary"></i></a>
                  <a href="#" class="icon btn btn-sm action-delete" data-url="{{route('system.user.destroy',[$user->id])}}" data-toggle="modal" data-target="#confirm-delete" title="Remove Record"><i class="mdi mdi-delete text-danger"></i></a>
                </td> --}}

                <div class="btn-group btn-hspace">
                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <ul role="menu" class="dropdown-menu">

                                 <li><a href="#confirm-delete" data-role="delete" data-toggle="modal"  data-id="{{ $user->id }}" title="Remove Record">Delete</a></li>
                                 <li><a href="{{route('system.user.edit',[$user->id])}}">Edit</a></li>
                              <li></li>
                            </ul>
                          </div>
                @endif
              </tr>
              @empty
              <td colspan="5" class="text-center"><i>No record found yet.</i> <a href="{{route('system.user.create')}}"><strong>Click here</strong></a> to create one.</td>
              @endforelse
         
            </tbody>
          </table>

  
    
        </div>
      </div>
    </div>
  </div>
</div>




@stop
@section('page-modals')
<form action="{{route('system.user.destroy')}}" method="POST">
  {{ csrf_field() }}
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You want to delete This Data?</p>
        <input type="text" name="idType" id="idType" hidden="" >

        
        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary btn-raised btn-loading" >Delete</button>

      </div>
    </div>
  </div>
</div>
</form>
@stop


@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
{{-- <script src="{{asset('assets/lib/datatables/js/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>--}}


<script src="{{asset('assets/lib/datatables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>

{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/datatables/css/datatables.bootstrap.min.css') }}"/> --}}

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js"></script>

<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
      $('#emp_table').DataTable({
          "paging": true
      });


      $(document).on('click','a[data-role=delete]',function(){

       $('#idType').val($(this).data('id'))
  
  
})

  });


</script>
@stop

{{-- /////////////////////////////////////////////////////////////////////////////////// --}}


