<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableLeaveRequestAddField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('leave_requests', function($table){
           $table->string('status')->nullable()->default('pending');
           
            
        });
    

         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::table('leave_requests', function($table){
            $table->dropColumn(['status']);
        });
    }
}
