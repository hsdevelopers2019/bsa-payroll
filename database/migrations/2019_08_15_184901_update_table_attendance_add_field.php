<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableAttendanceAddField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendances', function($table){
           $table->string('status')->nullable();
           $table->string('attendance_status')->nullable();
           $table->integer('overtime')->nullable();
          
         

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::table('attendances', function($table){
            // $table->dropColumn(['status']);
            // $table->dropColumn(['attendance_status']);
            // $table->dropColumn(['overtime']);
           

        });
    }
}
