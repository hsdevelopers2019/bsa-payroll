<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablePayslipsAddFiled extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payslips', function($table){
           $table->float('basic_pay')->nullable();
           $table->float('absent')->nullable();
           // $table->float('overtime')->nullable();
           
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('payslips', function($table){
            $table->dropColumn(['basic_pay']);
            $table->dropColumn(['absent']);
            // $table->dropColumn(['overtime']);
        });
    }
}
