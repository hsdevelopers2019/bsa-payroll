<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AdminAccountSeeder::class);
         // $this->call(UserAccountSeeder::class);
         $this->call(DepartmentSeeder::class);
         $this->call(EmployeeTypeSeeder::class);
         // $this->call(UserFinanceSeeder::class);
    }
}
