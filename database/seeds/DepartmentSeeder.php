<?php

use Illuminate\Database\Seeder;
use App\Laravel\models\Department;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department = new Department;
       $input = ['department_description' => "Punong Barangay"];

            $department->fill($input);
            $department->save();

        // $department->department_description = "Punong Barangay";
        // $department->department_description = "Kagawad";
        // $department->department_description = "Barangay Treasurer";
        // $department->department_description = "Sk Chairman";
        // $department->department_description = "Public Relation Assistant";
        // $department->save();
    }
}
