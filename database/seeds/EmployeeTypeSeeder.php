<?php

use Illuminate\Database\Seeder;
use App\Laravel\models\EmployeeType;

class EmployeeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department = new EmployeeType;

        $department->description = "Applicant";
        $department->description = "Regular";
        $department->description = "Volunteer";
        
        $department->save();
    }
}
