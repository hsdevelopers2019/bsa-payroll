<?php

use Illuminate\Database\Seeder;
use App\Laravel\models\User;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($user = 0;$user < 10;$user++)
        {
        	User::create([
        		'name' => $faker->name,
                'username' =>$faker->userName,
        		'email' =>$faker->email,
        		'password' => bcrypt('user'),
                'type' =>'user',
                'department' =>'IT',
        		'basic_pay' =>'25000',
        		

        	]);
        }
    }
}
