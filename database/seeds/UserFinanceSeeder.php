<?php
use App\Laravel\Models\UserFinance;
use Illuminate\Database\Seeder;

class UserFinanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $first_user =new UserFinance;
       
            $input = ['username' => "admin",'password' => bcrypt('admin'),'email'=>'admin@admin.com','name' => "Master Admin","type" => "super_user","address"=>"pasig","contact_number"=>"09179479397"];
            $first_user->fill($input);
            $first_user->save();
    }
}
